# tootcat boxes runbook!

This as a ~book~ document about how to run things, and also a kind of FAQ. The ordering of
sections below is _NOT_ significant. Please feel free to jump straight to whatever section
is most relevant to your needs.

## how to bring up the staging environment

> NOTE: The staging environment _IS NOT_ expected to be running at all times. This is primarily a
> cost savings measure.

Run the `up-staging` target, which _should_ prompt for typing `"yes"`:

```bash
make up-staging
```

This can be run with a git reference to bring up staging with a branch, for example:

```bash
make up-staging STAGING_TARGET=my-very-good-branch
```

This process will make many resources in Hetzner Cloud specific to the staging setup,
and then run `dep` tasks to ensure everything is provisioned and the database is
restored. The staging environment will then be available at <https://staging.toot.cat>.

## how to bring down the staging environment

Run the `down-staging` target:

```bash
make down-staging
```

This process will delete the resources in Hetzner Cloud specific to the staging setup.

## how to deploy a release

A release may be deployed via `dep`. The default deployment target is the [`re-live`
branch of tootcat/glitch
repository](https://gitlab.com/tootcat/glitch/-/tree/re-live?ref_type=heads).

In this example, the default target is being deployed to the staging web hosts:

```bash
dep deploy 'role.web=ok & stage=staging'
```

To deploy a different target, pass the `target` option:

```bash
dep deploy 'role.web=ok & stage=staging' --option target=my-lovely-branch
```

## how to access a host via ssh

The `dep` tool provides an `ssh` command that may be run with a specific host name. As
with all `dep` commands, there is a built in help system:

```bash
dep ssh --help
```

For example, accessing the load balancer host would be done like so:

```bash
dep ssh lb.toot.cat
```

## how to run locally in vagrant

Ensure the virtual machine is _up_:

```bash
vagrant up
```

after which the `dep` tasks may take over:

```bash
dep provision stage=local
```

```bash
dep deploy stage=local
```

and then things should be running, although of limited utility. These steps are great for
ensuring the `provision` and `deploy` tasks are working correctly :tada:.

<!--
vim:tw=90
-->
