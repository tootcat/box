<?php declare(strict_types=1);
namespace Deployer;

function has_role(string $role): bool {
    return 'ok' == (get('labels')['role.' . $role] ?? 'no');
}
