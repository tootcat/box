<?php declare(strict_types=1);
namespace Deployer;

function install(
    string $src,
    string $dest,
    string $owner,
    string $group,
    string $mode,
) {
    $parsedSrc = parse($src);
    $parsedDest = parse($dest);

    $contents = file_get_contents($parsedSrc);
    if (false === $contents) {
        throw error("failed to get source from {$parsedSrc}");
    }

    if (str_ends_with($parsedSrc, '.tmpl')) {
        $contents = parse($contents);
    }

    if (preg_match('/\\.ya?ml$/', $parsedDest)) {
        info("formatting YAML for {$parsedDest}");

        $contents = yaml_emit(yaml_parse($contents));
    }

    install_contents($contents, $dest, $owner, $group, $mode);

    info(
        "installed {$src} to {$dest} (owner={$owner} group={$group} mode={$mode})",
    );
}

function install_contents(
    string $contents,
    string $dest,
    string $owner,
    string $group,
    string $mode,
) {
    $tmpSrc = tempnam('/tmp', 'dep-install-');

    $putRes = file_put_contents($tmpSrc, $contents);
    if (false === $putRes) {
        throw error("failed to write to {$tmpSrc}");
    }

    $tmpDest = run('mktemp /tmp/dep-install.XXXXXXXX');

    upload($tmpSrc, $tmpDest);

    run("install -v -m {$mode} -o {$owner} -g {$group} {$tmpDest} {$dest}");

    run("rm -f {$tmpDest}");

    unlink($tmpSrc);
}
