<?php declare(strict_types=1);
namespace Deployer;

set('store_apt_env', ['DEBIAN_FRONTEND' => 'noninteractive']);

function store_client_init(string $user, string $group, string $ssh_dir) {
    run('apt-get update -y', env: get('store_apt_env'));
    run('apt-get install -y rsync', env: get('store_apt_env'));

    run(
        implode(' ', [
            'install -v -d',
            '-m 0770',
            '-o',
            $user,
            '-g',
            $group,
            $ssh_dir,
        ]),
    );

    install_contents(
        get('store_client_ssh_id'),
        "{$ssh_dir}/id_store",
        $user,
        $group,
        '0600',
    );

    install_contents(
        get('store_client_ssh_id_pub'),
        "{$ssh_dir}/id_store.pub",
        $user,
        $group,
        '0644',
    );

    install_contents(
        parse(
            implode("\n", [
                'STORE_DEST_HOST={{store_dest_host}}',
                'STORE_DEST_PREFIX={{store_dest_prefix}}',
                '',
            ]),
        ),
        '{{store_client_env}}',
        'root',
        'root',
        '0644',
    );
}

function store_stash(
    string $source,
    string $dest,
    ?int $timeout = null,
    ?bool $no_throw = false,
) {
    $source = parse($source);
    $dest = parse($dest);
    $remotePrefix = str_ends_with($dest, '/') ? $dest : dirname($dest);

    run(
        implode(' ', [
            'ssh ${SSH_OPTS}',
            '"{{store_dest_host}}"',
            'mkdir -p "${REMOTE_PREFIX}"',
            $no_throw ? '|| true' : '',
        ]),
        env: [
            'REMOTE_PREFIX' => $remotePrefix,
            'SSH_OPTS' => implode(' ', [
                '-i /root/.ssh/id_store',
                '-o StrictHostKeyChecking=no',
                '-o UserKnownHostsFile=/dev/null',
            ]),
        ],
        timeout: $timeout,
    );

    run(
        implode(' ', [
            'rsync -avzP -e',
            '"ssh ${SSH_OPTS}"',
            $source,
            '"{{store_dest_host}}:${REMOTE_DEST}"',
            $no_throw ? '|| true' : '',
        ]),
        env: [
            'REMOTE_DEST' => $dest,
            'SSH_OPTS' => implode(' ', [
                '-i /root/.ssh/id_store',
                '-o StrictHostKeyChecking=no',
                '-o UserKnownHostsFile=/dev/null',
            ]),
        ],
        timeout: $timeout,
    );
}

function store_unstash(
    string $source,
    string $dest,
    ?int $timeout = null,
    ?bool $no_throw = false,
) {
    $source = parse($source);
    $dest = parse($dest);
    $localPrefix = str_ends_with($dest, '/') ? $dest : dirname($dest);

    run(
        implode(' ', [
            'mkdir -p "${LOCAL_PREFIX}"',
            $no_throw ? '|| true' : '',
        ]),
        env: [
            'LOCAL_PREFIX' => $localPrefix,
            'SSH_OPTS' => implode(' ', [
                '-i /root/.ssh/id_store',
                '-o StrictHostKeyChecking=no',
                '-o UserKnownHostsFile=/dev/null',
            ]),
        ],
        timeout: $timeout,
    );

    run(
        implode(' ', [
            'rsync -avzP -e',
            '"ssh ${SSH_OPTS}"',
            '"{{store_dest_host}}:${REMOTE_SOURCE}"',
            '"${LOCAL_DEST}"',
            $no_throw ? '|| true' : '',
        ]),
        env: [
            'REMOTE_SOURCE' => $source,
            'LOCAL_DEST' => $dest,
            'SSH_OPTS' => implode(' ', [
                '-i /root/.ssh/id_store',
                '-o StrictHostKeyChecking=no',
                '-o UserKnownHostsFile=/dev/null',
            ]),
        ],
        timeout: $timeout,
    );
}
