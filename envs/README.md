# envs

The files in this directory are in ["dotenv format"](https://12factor.net/config), which
is a lightly-defined protocol/format understood by various language libraries and tools
including [`vlucas/phpdotenv`](https://github.com/vlucas/phpdotenv).

Files ending with `.enc.env` are not meant to be edited directly, but instead must be
edited via [sops](https://getsops.io/), which has its own ways of opening an editor like
via `$EDITOR`, e.g.:

```bash
sops ./local.enc.env
```
