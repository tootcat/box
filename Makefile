SHELL := /bin/bash
TOP := $(shell git rev-parse --show-toplevel)
DEP := $(TOP)/vendor/bin/dep
TARGET ?= re-live
STAGING_TARGET ?= re-live
TOFU_APPLY_ARGS ?=

##@ Development

.PHONY: all
all: fmt lint test ## Run the default development bits

.PHONY: fmt
fmt: ## Format (almost) everything with prettier
	$(TOP)/node_modules/.bin/prettier -w .

.PHONY: lint
lint: ## Lint the PHP with phplint
	$(TOP)/vendor/bin/phplint

.PHONY: test
test: ## Run PHP tests with pest
	$(TOP)/vendor/bin/pest --coverage

##@ Operations

.PHONY: forward-traefik
forward-traefik: ## Forward Traefik dashboard to localhost
	@printf '\n====> http://127.0.0.1:8080/dashboard/\n\n' && \
		(ssh -F tf.sshconfig -N -L 8080:127.0.0.1:8080 lb.toot.cat || true)

.PHONY: forward-prometheus
forward-prometheus: ## Forward Prometheus dashboard to localhost
	@printf '\n====> http://127.0.0.1:9090/\n\n' && \
		(ssh -F tf.sshconfig -N -L 9090:127.0.0.1:9090 maintenance.toot.cat || true)

.PHONY: deploy-production
deploy-production: ## Deploy TARGET to production web servers
	$(DEP) deploy:mastodon:rolling 'stage=production & role.web=ok' -o 'target=$(TARGET)'

.PHONY: deploy-staging
deploy-staging: ## Deploy STAGING_TARGET to staging web servers
	$(DEP) deploy:mastodon:rolling 'stage=staging & role.web=ok' -o 'target=$(STAGING_TARGET)' --hastily

.PHONY: rollback-production
rollback-production: ## Rollback production web servers to previous release
	$(DEP) rollback 'stage=production & role.web=ok'

.PHONY: rollback-staging
rollback-staging: ## rollback staging web servers to previous release
	$(DEP) rollback 'stage=staging & role.web=ok' --hastily

.PHONY: up-staging
up-staging: .pre-deploy-staging deploy-staging .post-deploy-staging ## Bring up the staging environment

.PHONY: down-staging
down-staging: .tofu-destroy-staging .lb-config-staging ## Take down the staging environment

.PHONY: smoke-staging
smoke-staging: ## Smoke test the staging environment
	$(DEP) deploy:mastodon:smoke localhost -o 'smoke_test_base_url=https://staging.toot.cat'

.PHONY: migrate-staging
migrate-staging: ## Run migrations in staging
	$(DEP) rails:db:migrate web01.staging.toot.cat -v

.PHONY: migrate-production
migrate-production: ## Run migrations in production
	$(DEP) rails:db:migrate web01.production.toot.cat -v

.PHONY: .tofu-destroy-staging
.tofu-destroy-staging:
	tofu -chdir=./tf/staging apply -destroy $(TOFU_APPLY_ARGS)

.PHONY: .pre-deploy-staging
.pre-deploy-staging: .tofu-apply-staging .dep-tf-sshconfig .wait-ssh-staging .provision-staging

.PHONY: .post-deploy-staging
.post-deploy-staging: .restore-staging migrate-staging .lb-config-staging

.PHONY: .tofu-apply-staging
.tofu-apply-staging:
	tofu -chdir=./tf/staging/ apply $(TOFU_APPLY_ARGS)

.PHONY: .dep-tf-sshconfig
.dep-tf-sshconfig:
	$(DEP) tf:sshconfig localhost

.PHONY: .wait-ssh-staging
.wait-ssh-staging:
	$(DEP) $(DEP_PROVISION_WAIT_SSH_ARGS) provision:wait_ssh 'stage=staging'

.PHONY: .provision-staging
.provision-staging:
	$(DEP) $(DEP_PROVISION_ARGS) provision 'stage=staging'

.PHONY: .restore-staging
.restore-staging:
	$(DEP) deploy:mastodon:stop stage=staging -v && \
		$(DEP) provision:postgresql:restore stage=staging -v && \
		printf '(hanging out a bit while mastodon wakes up)\n' >&2 && \
		sleep 30 && \
		$(DEP) deploy:mastodon:tootctl web01.staging.toot.cat -o tootctl_command='feeds build' -v && \
		$(DEP) deploy:mastodon:reload 'stage=staging & role.web=ok' -v

.PHONY: .lb-config-staging
.lb-config-staging:
	$(DEP) provision:traefik:sync_configs lb.toot.cat -v

##@ Meta

# NOTE: this bit lovingly borrowed from
# https://github.com/operator-framework/operator-sdk/blob/master/Makefile
.DEFAULT_GOAL := help
.PHONY: help
help: ## Show this help screen.
	@echo 'Usage: make <OPTIONS> ... <TARGETS>'
	@echo ''
	@echo 'Available targets are:'
	@echo ''
	@awk 'BEGIN { FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n" } \
	/^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-25s\033[0m %s\n", $$1, $$2 } \
	/^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } \
	' $(MAKEFILE_LIST)
