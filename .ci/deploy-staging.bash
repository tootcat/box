#!/usr/bin/env bash
set -o errexit
set -o pipefail

main() {
  local top
  top="$(git rev-parse --show-toplevel)"

  source "${top}/.ci/functions.bash"
  assert-ci
  assert-nonroot

  cd "${top}"
  eval "$(grep -v ^PATH_add .envrc.recommended)"

  export SSH_ASKPASS="${PWD}/.ci/gitlab-ssh-askpass"
  eval "$(ssh-agent)"
  DISPLAY=1 ssh-add ~/.ssh/id_ed25519
  touch ~/.ssh/known_hosts
  chmod -v 0600 ~/.ssh/known_hosts
  tee -a ~/.ssh/known_hosts <assets/maintenance.toot.cat.public-keys

  make up-staging TOFU_APPLY_ARGS=-auto-approve DEP_PROVISION_WAIT_SSH_ARGS=-vvv
  make smoke-staging

  if [[ "${STAGING_DOWN_ON_COMPLETE:-no}" == 'true' ]]; then
    make down-staging TOFU_APPLY_ARGS=-auto-approve
  else
    printf 'NOTE: *not* taking down staging due to STAGING_DOWN_ON_COMPLETE=%s\n' \
      "${STAGING_DOWN_ON_COMPLETE}"
  fi
}

main "${@}"
