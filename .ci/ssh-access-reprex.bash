#!/usr/bin/env bash
set -o errexit
set -o pipefail

main() {
  eval "$(grep -v ^PATH_add .envrc.recommended)"
  export SSH_ASKPASS="${PWD}/.ci/gitlab-ssh-askpass"

  printenv SSH_AUTH_SOCK || echo 'No SSH_AUTH_SOCK?'
  eval "$(ssh-agent)"
  printenv SSH_AUTH_SOCK || echo 'No SSH_AUTH_SOCK?'

  DISPLAY=1 ssh-add ~/.ssh/id_ed25519

  touch ~/.ssh/known_hosts
  chmod -v 0600 ~/.ssh/known_hosts
  tee -a ~/.ssh/known_hosts <assets/maintenance.toot.cat.public-keys
  ls -la ~/.ssh/

  ./vendor/bin/dep tf:sshconfig localhost

  while true; do
    printf '\n\n### ------------- ###\n\n'

    ssh \
      -F tf.sshconfig \
      -o 'StrictHostKeyChecking=yes' \
      -o 'ConnectTimeout=5' \
      -vvv maintenance.toot.cat uptime || \
      printf 'oh no: %s\n' "${?}"

    sleep 30
  done
}

main "${@}"
