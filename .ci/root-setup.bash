#!/usr/bin/env bash
set -o errexit
set -o pipefail

main() {
  source '.ci/functions.bash'
  assert-ci
  assert-root

  export DEBIAN_FRONTEND=noninteractive

  local packages=(
    composer
    curl
    git
    openssh-client
    php-curl
    php-xdebug
    php-xml
    php-yaml
    sudo
  )

  apt-get update -yq
  apt-get install -yq "${packages[@]}"

  if ! sops --version &>/dev/null; then
    curl -fsSL -o /tmp/sops \
      'https://github.com/getsops/sops/releases/download/v3.9.0/sops-v3.9.0.linux.amd64'
    install -v -m 0755 -o root /tmp/sops /usr/local/bin/sops
    rm -f /tmp/sops
  fi

  printf 'ubuntu ALL=(ALL) NOPASSWD: ALL\n' | tee /etc/sudoers.d/ubuntu
  chmod -v 0600 /etc/sudoers.d/ubuntu
  chown -R ubuntu:ubuntu "${CI_BUILDS_DIR}"
}

main "${@}"
