#!/usr/bin/env bash
set -o errexit
set -o pipefail

main() {
  local top
  top="$(git rev-parse --show-toplevel)"

  source "${top}/.ci/functions.bash"
  assert-ci
  assert-nonroot

  local stage="${1}"

  if [[ -z "${stage}" ]]; then
    printf 'ERROR: missing <stage> positional argument\n' >&2
  fi

  cd "${top}"
  eval "$(grep -v ^PATH_add .envrc.recommended)"
  tofu -chdir="./tf/${stage}" init
}

main "${@}"
