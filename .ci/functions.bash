assert-ci() {
  if [[ "${CI}" != 'true' ]]; then
    printf 'ERROR: not in a CI environment; byeee\n' >&2
    exit 1
  fi
}

assert-root() {
  if [[ "$(id -u)" != '0' ]]; then
    printf 'ERROR: not running as root; byeee\n' >&2
    exit 2
  fi
}

assert-nonroot() {
  if [[ "$(id -u)" == '0' ]]; then
    printf 'ERROR: running as root; byeee\n' >&2
    exit 2
  fi
}
