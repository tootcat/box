#!/usr/bin/env bash
set -o errexit
set -o pipefail

main() {
  local top
  top="$(git rev-parse --show-toplevel)"

  source "${top}/.ci/functions.bash"
  assert-ci
  assert-nonroot

  if ! printenv GITLAB_AGE_PRIVATE_KEY >/dev/null; then
    printf 'ERROR: missing required env var GITLAB_AGE_PRIVATE_KEY\n' >&2
    exit 86
  fi

  mkdir -p ~/.config/sops/age/
  printenv GITLAB_AGE_PRIVATE_KEY | tee ~/.config/sops/age/keys.txt &>/dev/null
  chmod -v 0700 ~/.config/sops/age/
  chmod -v 0600 ~/.config/sops/age/keys.txt

  set -o allexport
  source envs/sops.env
  eval "$(sops -d envs/all.enc.env)"
  set +o allexport

  mkdir -p ~/.ssh/
  printenv GITLAB_SSH_PRIVATE_KEY | base64 --decode | tee ~/.ssh/id_ed25519 &>/dev/null
  printenv GITLAB_SSH_PUBLIC_KEY | base64 --decode | tee ~/.ssh/id_ed25519.pub &>/dev/null
  chmod -v 0700 ~/.ssh/
  chmod -v 0600 ~/.ssh/id_ed25519 ~/.ssh/id_ed25519.pub

  ./dev-setup-ubuntu.php
  ./vendor/bin/dep tf:sshconfig localhost
}

main "${@}"
