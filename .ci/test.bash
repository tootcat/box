#!/usr/bin/env bash
set -o errexit
set -o pipefail

main() {
  local top
  top="$(git rev-parse --show-toplevel)"

  source "${top}/.ci/functions.bash"
  assert-ci
  assert-nonroot

  eval "$(grep -v ^PATH_add .envrc.recommended)"
  make all
  git diff --exit-code
  git diff --staged --exit-code
}

main "${@}"
