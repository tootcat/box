#!/usr/bin/env bash
set -o errexit
set -o pipefail

main() {
  local top
  top="$(git rev-parse --show-toplevel)"

  source "${top}/.ci/functions.bash"
  assert-ci
  assert-nonroot

  cd "${top}"
  eval "$(grep -v ^PATH_add .envrc.recommended)"

  export SSH_ASKPASS="${PWD}/.ci/gitlab-ssh-askpass"
  eval "$(ssh-agent)"
  DISPLAY=1 ssh-add ~/.ssh/id_ed25519
  touch ~/.ssh/known_hosts
  chmod -v 0600 ~/.ssh/known_hosts
  tee -a ~/.ssh/known_hosts <assets/maintenance.toot.cat.public-keys

  dep deploy:mastodon:rolling \
    'stage=production & role.web=ok' \
    -o target="${TARGET:-re-live}"
}

main "${@}"
