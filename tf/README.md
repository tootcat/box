# tf

The sub-directories in this directory and their respective purposes are:

- [`modules`](./modules) contains OpenTofu "modules" which are the primary abstraction of
  reuse. At the time of this writing, the only module is `tout` (as in "everything"),
  which is used by both the `./staging` and `./production` directories.
- [`production`](./production) contains the OpenTofu resource definitions for the
  "production" (`toot.cat`) environment. At the time of this writing, this environment
  is _not_ set up.
- [`staging`](./staging) contains the OpenTofu resource definitions for the "staging"
  (`staging.toot.cat`) environment, and is _mostly_ intended to be ephemeral.
