variable "db_protect" { type = bool }
variable "db_server_type" { type = string }
variable "db_volume_size" { type = number }
variable "environment" { type = string }
variable "location" { type = string }
variable "network_zone" { type = string }
variable "web_count" { type = number }
variable "web_domain" { type = string }
variable "web_server_type" { type = string }

variable "topology" {
  type = object({
    cidr = string

    private = object({
      production = object({ cidr = string, id = string })
      staging    = object({ cidr = string, id = string })
      shared     = object({ cidr = string, id = string })
    }),

    public = object({ cidr = string, id = string })
  })
}

locals {
  private_subnet_cidr = var.topology.private[var.environment].cidr

  db_ip     = cidrhost(local.private_subnet_cidr, 3)
  web_names = [for n in range(var.web_count) : format("web%02d", n + 1)]
  web_ips   = [for n in range(var.web_count) : cidrhost(local.private_subnet_cidr, n + 11)]
}

data "hcloud_ssh_keys" "tootcat" {
  with_selector = "project=tootcat"
}

data "hcloud_network" "main" {
  name = "tootcat"
}

resource "hcloud_placement_group" "main" {
  name = "${var.environment}.${var.web_domain}"
  type = "spread"

  labels = {
    env = var.environment
  }
}

resource "hcloud_server" "web" {
  for_each = toset(local.web_names)

  name               = "${each.key}.${var.environment}.${var.web_domain}"
  image              = "ubuntu-22.04"
  server_type        = var.web_server_type
  location           = var.location
  placement_group_id = hcloud_placement_group.main.id
  ssh_keys           = data.hcloud_ssh_keys.tootcat.ssh_keys.*.name

  user_data = templatefile(
    "${path.module}/nat-client-cloud-init.yaml.tmpl",
    { gateway = cidrhost(data.hcloud_network.main.ip_range, 1) },
  )

  public_net {
    ipv4_enabled = false
    ipv6_enabled = false
  }

  network {
    alias_ips  = []
    ip         = local.web_ips[index(local.web_names, each.key)]
    network_id = data.hcloud_network.main.id
  }

  labels = {
    env               = var.environment
    "role.nat_client" = "ok"
    "role.web"        = "ok"
  }

  lifecycle {
    ignore_changes = [ssh_keys]
  }
}

resource "hcloud_server_network" "web" {
  for_each = toset(local.web_names)

  server_id = hcloud_server.web[each.key].id
  subnet_id = var.topology.private[var.environment].id
}

resource "hcloud_volume" "db" {
  name              = "tootcat-db-${var.environment}"
  size              = var.db_volume_size
  format            = "ext4"
  server_id         = hcloud_server.db.id
  automount         = true
  delete_protection = var.db_protect

  labels = {
    env       = var.environment
    "role.db" = "ok"
  }
}

resource "hcloud_server" "db" {
  name               = "db.${var.environment}.${var.web_domain}"
  image              = "ubuntu-22.04"
  server_type        = var.db_server_type
  location           = var.location
  placement_group_id = hcloud_placement_group.main.id
  delete_protection  = var.db_protect
  rebuild_protection = var.db_protect
  ssh_keys           = data.hcloud_ssh_keys.tootcat.ssh_keys.*.name

  user_data = templatefile(
    "${path.module}/nat-client-cloud-init.yaml.tmpl",
    { gateway = cidrhost(data.hcloud_network.main.ip_range, 1) },
  )

  public_net {
    ipv4_enabled = false
    ipv6_enabled = false
  }

  network {
    alias_ips  = []
    ip         = local.db_ip
    network_id = data.hcloud_network.main.id
  }

  labels = {
    env                  = var.environment
    "role.db"            = "ok"
    "role.elasticsearch" = "ok"
    "role.nat_client"    = "ok"
    "role.opendkim"      = "ok"
    "role.postfix"       = "ok"
    "role.valkey"        = "ok"
  }

  lifecycle {
    ignore_changes = [ssh_keys]
  }
}

resource "hcloud_server_network" "db" {
  server_id = hcloud_server.db.id
  subnet_id = var.topology.private[var.environment].id
}

resource "local_file" "sshconfig" {
  filename             = "${path.root}/ssh/${var.environment}.sshconfig"
  file_permission      = "0644"
  directory_permission = "0755"
  content = join("\n", concat([
    <<-EOF
    # vim:filetype=sshconfig

    Host ${hcloud_server.db.name}
      Hostname ${local.db_ip}
      User root
      UserKnownHostsFile /dev/null
      StrictHostKeyChecking no
      ProxyJump maintenance.${var.web_domain}
    EOF
    ], [
    for name in local.web_names :
    <<-EOF
    Host ${hcloud_server.web[name].name}
      Hostname ${local.web_ips[index(local.web_names, name)]}
      User root
      UserKnownHostsFile /dev/null
      StrictHostKeyChecking no
      ProxyJump maintenance.${var.web_domain}
    EOF
    ],
  ))
}
