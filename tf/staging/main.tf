variable "db_protect" { type = bool }
variable "db_server_type" { type = string }
variable "db_volume_size" { type = number }
variable "environment" { type = string }
variable "location" { type = string }
variable "network_zone" { type = string }
variable "web_count" { type = number }
variable "web_domain" { type = string }
variable "web_server_type" { type = string }

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/55496989/terraform/state/staging"
    lock_address   = "https://gitlab.com/api/v4/projects/55496989/terraform/state/staging/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/55496989/terraform/state/staging/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = "5"
  }

  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.47.0"
    }

    local = { version = "2.5.1" }
  }
}

provider "hcloud" {}

module "tout" {
  source = "../modules/tout"

  db_protect      = var.db_protect
  db_server_type  = var.db_server_type
  db_volume_size  = var.db_volume_size
  environment     = var.environment
  location        = var.location
  network_zone    = var.network_zone
  topology        = jsondecode(file("${path.root}/../net/topology.json"))
  web_count       = var.web_count
  web_domain      = var.web_domain
  web_server_type = var.web_server_type
}
