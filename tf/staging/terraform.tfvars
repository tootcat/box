db_protect      = false
db_server_type  = "cpx11"
db_volume_size  = 10
environment     = "staging"
location        = "ash"
network_zone    = "us-east"
web_count       = 2
web_domain      = "toot.cat"
web_server_type = "cpx11"
