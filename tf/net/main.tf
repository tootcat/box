variable "lb_protect" { type = bool }
variable "lb_server_type" { type = string }
variable "lilstore_volume_size" { type = number }
variable "location" { type = string }
variable "mail_domain" { type = string }
variable "maintenance_server_type" { type = string }
variable "nat_protect" { type = bool }
variable "nat_server_type" { type = string }
variable "network" { type = string }
variable "network_zone" { type = string }
variable "web_domain" { type = string }

locals {
  production_ip   = hcloud_floating_ip.lb_ipv4.ip_address
  production_ipv6 = hcloud_server.lb.ipv6_address
  staging_ip      = hcloud_floating_ip.lb_ipv4.ip_address

  topology = {
    cidr = var.network

    public = {
      cidr = hcloud_network_subnet.public.ip_range
      id   = hcloud_network_subnet.public.id

      lb               = hcloud_floating_ip.lb_ipv4.ip_address
      lb_ipv4          = hcloud_floating_ip.lb_ipv4.ip_address
      lb_ipv6          = hcloud_floating_ip.lb_ipv6.ip_address
      maintenance      = hcloud_server.maintenance.ipv4_address
      maintenance_ipv4 = hcloud_server.maintenance.ipv4_address
      maintenance_ipv6 = hcloud_server.maintenance.ipv6_address
      nat              = hcloud_floating_ip.nat_ipv4.ip_address
      nat_ipv4         = hcloud_floating_ip.nat_ipv4.ip_address
      nat_ipv6         = hcloud_floating_ip.nat_ipv6.ip_address
    }

    private = {
      production = {
        cidr = hcloud_network_subnet.private["production"].ip_range
        id   = hcloud_network_subnet.private["production"].id
      }

      shared = {
        cidr = hcloud_network_subnet.private["shared"].ip_range
        id   = hcloud_network_subnet.private["shared"].id

        lb          = hcloud_server_network.lb.ip
        maintenance = hcloud_server_network.maintenance.ip
        nat         = hcloud_server_network.nat.ip
      }

      # NOTE: 'meta' is a copy of 'shared' due to how the top-level deploy.php wants to
      # look up the cidr based on the 'stage' variable :upside_down_face: whoops!
      meta = {
        cidr = hcloud_network_subnet.private["shared"].ip_range
        id   = hcloud_network_subnet.private["shared"].id

        lb          = hcloud_server_network.lb.ip
        maintenance = hcloud_server_network.maintenance.ip
        nat         = hcloud_server_network.nat.ip
      }

      staging = {
        cidr = hcloud_network_subnet.private["staging"].ip_range
        id   = hcloud_network_subnet.private["staging"].id
      }
    }
  }
}

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/55496989/terraform/state/net"
    lock_address   = "https://gitlab.com/api/v4/projects/55496989/terraform/state/net/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/55496989/terraform/state/net/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = "5"
  }

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.39.2"
    }

    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.47.0"
    }

    local = { version = "2.5.1" }
  }
}

data "hcloud_ssh_keys" "tootcat" {
  with_selector = "project=tootcat"
}

data "digitalocean_domain" "main" {
  name = var.web_domain
}

data "digitalocean_domain" "mail" {
  name = var.mail_domain
}

resource "hcloud_network" "main" {
  name              = "tootcat"
  ip_range          = var.network
  delete_protection = true
}

resource "hcloud_network_subnet" "public" {
  network_id   = hcloud_network.main.id
  type         = "cloud"
  network_zone = var.network_zone
  ip_range     = cidrsubnet(hcloud_network.main.ip_range, 2, 0)
}

resource "hcloud_network_subnet" "private" {
  for_each = toset(["production", "shared", "staging"])

  network_id   = hcloud_network.main.id
  type         = "cloud"
  network_zone = var.network_zone
  ip_range = lookup({
    production = cidrsubnet(hcloud_network.main.ip_range, 2, 2)
    shared     = cidrsubnet(hcloud_network.main.ip_range, 2, 1)
    staging    = cidrsubnet(hcloud_network.main.ip_range, 2, 3)
  }, each.key)
}

resource "hcloud_firewall" "maintenance" {
  name = "tootcat-maintenance"

  rule {
    description = "Allow public ICMP (ping)"
    direction   = "in"
    protocol    = "icmp"
    source_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }

  rule {
    description = "Allow public SSH"
    direction   = "in"
    protocol    = "tcp"
    port        = "22"
    source_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }
}

resource "hcloud_firewall" "lb" {
  name = "tootcat-lb"

  rule {
    description = "Allow public ICMP (ping)"
    direction   = "in"
    protocol    = "icmp"
    source_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }

  rule {
    description = "Allow public HTTP"
    direction   = "in"
    protocol    = "tcp"
    port        = "80"
    source_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }

  rule {
    description = "Allow public HTTPS"
    direction   = "in"
    protocol    = "tcp"
    port        = "443"
    source_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }
}

resource "hcloud_firewall" "private" {
  name = "tootcat-private"

  rule {
    description = "Allow all ephemeral port TCP"
    direction   = "in"
    protocol    = "tcp"
    port        = "49152-65535"
    source_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }

  rule {
    description = "Allow all ephemeral port UDP"
    direction   = "in"
    protocol    = "udp"
    port        = "49152-65535"
    source_ips = [
      "0.0.0.0/0",
      "::/0",
    ]
  }
}

resource "hcloud_server" "nat" {
  name               = "nat.${var.web_domain}"
  image              = "ubuntu-22.04"
  server_type        = var.nat_server_type
  location           = var.location
  ssh_keys           = data.hcloud_ssh_keys.tootcat.ssh_keys.*.name
  firewall_ids       = [hcloud_firewall.private.id]
  delete_protection  = var.nat_protect
  rebuild_protection = var.nat_protect

  user_data = templatefile(
    "${path.module}/nat-cloud-init.yaml.tmpl",
    { network = var.network },
  )

  public_net {
    ipv4_enabled = true
    ipv6_enabled = true
  }

  labels = {
    "env"      = "meta"
    "role.nat" = "ok"
  }

  lifecycle {
    ignore_changes = [ssh_keys]
  }
}

resource "hcloud_server_network" "nat" {
  server_id = hcloud_server.nat.id
  subnet_id = hcloud_network_subnet.public.id
  ip        = cidrhost(hcloud_network_subnet.public.ip_range, 7)
}

resource "hcloud_floating_ip" "nat_ipv4" {
  description       = "Primary NAT IP for all things ${var.web_domain}"
  delete_protection = true
  server_id         = hcloud_server.nat.id
  type              = "ipv4"

  labels = {
    "env"      = "meta"
    "role.nat" = "ok"
  }
}

resource "hcloud_floating_ip" "nat_ipv6" {
  description       = "Primary NAT ipv6 IP for all things ${var.web_domain}"
  delete_protection = true
  server_id         = hcloud_server.nat.id
  type              = "ipv6"

  labels = {
    "env"      = "meta"
    "role.nat" = "ok"
  }
}

resource "hcloud_rdns" "nat_ipv4" {
  floating_ip_id = hcloud_floating_ip.nat_ipv4.id
  ip_address     = hcloud_floating_ip.nat_ipv4.ip_address
  dns_ptr        = "nat.${var.web_domain}"
}

resource "hcloud_rdns" "nat_ipv6" {
  floating_ip_id = hcloud_floating_ip.nat_ipv6.id
  ip_address     = hcloud_floating_ip.nat_ipv6.ip_address
  dns_ptr        = "nat.${var.web_domain}"
}

resource "digitalocean_record" "nat_a" {
  domain = data.digitalocean_domain.main.id
  type   = "A"
  name   = "nat"
  value  = hcloud_floating_ip.nat_ipv4.ip_address
  ttl    = 1800
}

resource "digitalocean_record" "nat_aaaa" {
  domain = data.digitalocean_domain.main.id
  type   = "AAAA"
  name   = "nat"
  value  = hcloud_floating_ip.nat_ipv6.ip_address
  ttl    = 1800
}

resource "digitalocean_record" "nat_spf" {
  domain = data.digitalocean_domain.main.id
  type   = "TXT"
  name   = "nat"
  value = join(" ", [
    "v=spf1",
    "mx",
    "ip4:${hcloud_floating_ip.nat_ipv4.ip_address}",
    "ip6:${hcloud_floating_ip.nat_ipv6.ip_address}",
    "-all",
  ])
  ttl = 1800
}

resource "hcloud_network_route" "private" {
  network_id  = hcloud_network.main.id
  destination = "0.0.0.0/0"
  gateway     = hcloud_server_network.nat.ip
}

resource "hcloud_server" "maintenance" {
  name         = "maintenance.${var.web_domain}"
  image        = "ubuntu-22.04"
  server_type  = var.maintenance_server_type
  location     = var.location
  ssh_keys     = data.hcloud_ssh_keys.tootcat.ssh_keys.*.name
  firewall_ids = [hcloud_firewall.maintenance.id]

  public_net {
    ipv4_enabled = true
    ipv6_enabled = true
  }

  labels = {
    "env"              = "meta"
    "role.grafana"     = "ok"
    "role.maintenance" = "ok"
    "role.nat_client"  = "ok"
    "role.prometheus"  = "ok"
  }

  lifecycle {
    ignore_changes = [ssh_keys]
  }
}

resource "hcloud_server_network" "maintenance" {
  server_id = hcloud_server.maintenance.id
  subnet_id = hcloud_network_subnet.public.id
  ip        = cidrhost(hcloud_network_subnet.public.ip_range, 2)
}

resource "hcloud_rdns" "maintenance_ipv4" {
  server_id  = hcloud_server.maintenance.id
  ip_address = hcloud_server.maintenance.ipv4_address
  dns_ptr    = "maintenance.${var.web_domain}"
}

resource "hcloud_rdns" "maintenance_ipv6" {
  server_id  = hcloud_server.maintenance.id
  ip_address = hcloud_server.maintenance.ipv6_address
  dns_ptr    = "maintenance.${var.web_domain}"
}

resource "digitalocean_record" "maintenance_a" {
  domain = data.digitalocean_domain.main.id
  type   = "A"
  name   = "maintenance"
  value  = hcloud_server.maintenance.ipv4_address
}

resource "digitalocean_record" "maintenance_aaaa" {
  domain = data.digitalocean_domain.main.id
  type   = "AAAA"
  name   = "maintenance"
  value  = hcloud_server.maintenance.ipv6_address
}

resource "hcloud_volume" "lilstore" {
  name              = "tootcat-lilstore-meta"
  size              = var.lilstore_volume_size
  format            = "ext4"
  server_id         = hcloud_server.lb.id
  automount         = true
  delete_protection = true

  labels = {
    env          = "meta"
    "role.store" = "ok"
  }
}

resource "hcloud_server" "lb" {
  name               = "lb.${var.web_domain}"
  image              = "ubuntu-22.04"
  server_type        = var.lb_server_type
  location           = var.location
  ssh_keys           = data.hcloud_ssh_keys.tootcat.ssh_keys.*.name
  firewall_ids       = [hcloud_firewall.lb.id]
  delete_protection  = var.lb_protect
  rebuild_protection = var.lb_protect

  public_net {
    ipv4_enabled = true
    ipv6_enabled = true
  }

  labels = {
    "env"             = "meta"
    "role.store"      = "ok"
    "role.lb"         = "ok"
    "role.nat_client" = "ok"
    "role.traefik"    = "ok"
  }

  lifecycle {
    ignore_changes = [ssh_keys]
  }
}

resource "hcloud_server_network" "lb" {
  server_id = hcloud_server.lb.id
  subnet_id = hcloud_network_subnet.public.id
  ip        = cidrhost(hcloud_network_subnet.public.ip_range, -2)
}

resource "hcloud_floating_ip" "lb_ipv4" {
  description       = "Primary load balancer IP for all things ${var.web_domain}"
  delete_protection = true
  server_id         = hcloud_server.lb.id
  type              = "ipv4"

  labels = {
    "env"     = "meta"
    "role.lb" = "ok"
  }
}

resource "hcloud_floating_ip" "lb_ipv6" {
  description       = "Primary load balancer ipv6 IP for all things ${var.web_domain}"
  delete_protection = true
  server_id         = hcloud_server.lb.id
  type              = "ipv6"

  labels = {
    "env"     = "meta"
    "role.lb" = "ok"
  }
}

resource "hcloud_rdns" "lb_ipv4" {
  floating_ip_id = hcloud_floating_ip.lb_ipv4.id
  ip_address     = hcloud_floating_ip.lb_ipv4.ip_address
  dns_ptr        = "lb.${var.web_domain}"
}

resource "hcloud_rdns" "lb_ipv6" {
  floating_ip_id = hcloud_floating_ip.lb_ipv6.id
  ip_address     = hcloud_floating_ip.lb_ipv6.ip_address
  dns_ptr        = "lb.${var.web_domain}"
}

resource "digitalocean_record" "lb_a" {
  domain = data.digitalocean_domain.main.id
  type   = "A"
  name   = "lb"
  value  = hcloud_floating_ip.lb_ipv4.ip_address
}

resource "digitalocean_record" "lb_aaaa" {
  domain = data.digitalocean_domain.main.id
  type   = "AAAA"
  name   = "lb"
  value  = hcloud_floating_ip.lb_ipv6.ip_address
}

resource "digitalocean_record" "staging" {
  domain = data.digitalocean_domain.main.id
  type   = "A"
  name   = "staging"
  value  = local.staging_ip
  ttl    = 60
}

resource "digitalocean_record" "grafana" {
  domain = data.digitalocean_domain.main.id
  type   = "CNAME"
  name   = "grafana"
  value  = "lb.${var.web_domain}."
  ttl    = 60
}

resource "digitalocean_record" "production_a" {
  domain = data.digitalocean_domain.main.id
  type   = "A"
  name   = "@"
  value  = local.production_ip
  ttl    = 60
}

resource "digitalocean_record" "production_aaaa" {
  domain = data.digitalocean_domain.main.id
  type   = "AAAA"
  name   = "@"
  value  = local.production_ipv6
  ttl    = 3600
}

data "digitalocean_records" "mail_a" {
  domain = data.digitalocean_domain.mail.id
  filter {
    key    = "name"
    values = ["mail"]
  }
  filter {
    key    = "type"
    values = ["A"]
  }
}

data "digitalocean_records" "mail_aaaa" {
  domain = data.digitalocean_domain.mail.id
  filter {
    key    = "name"
    values = ["mail"]
  }
  filter {
    key    = "type"
    values = ["AAAA"]
  }
}

resource "digitalocean_record" "production_spf" {
  domain = data.digitalocean_domain.main.id
  type   = "TXT"
  name   = "@"
  value = join(" ", concat([
    "v=spf1",
    "mx",
    ], [
    for rec in data.digitalocean_records.mail_a.records : "ip4:${rec.value}"
    ], [
    for rec in data.digitalocean_records.mail_aaaa.records : "ip6:${rec.value}"
    ], [
    "include:spf.${var.web_domain}",
    "include:nat.${var.web_domain}",
    "-all",
  ]))
  ttl = 1800
}

resource "digitalocean_record" "spf" {
  domain = data.digitalocean_domain.main.id
  type   = "TXT"
  name   = "spf"
  value = join(" ", [
    "v=spf1",
    "mx",
    "ip4:${hcloud_floating_ip.lb_ipv4.ip_address}",
    "ip6:${hcloud_floating_ip.lb_ipv6.ip_address}",
    "-all",
  ])
  ttl = 1800
}

resource "digitalocean_record" "production_mx" {
  domain   = data.digitalocean_domain.main.id
  type     = "MX"
  name     = "@"
  value    = "@"
  priority = 10
  ttl      = 14400
}

resource "digitalocean_record" "production_dkim" {
  domain = data.digitalocean_domain.main.id
  type   = "TXT"
  name   = "default._domainkey"
  ttl    = 14400

  # NOTE: The value here is *almost* like the value that is written
  # to /etc/dkimkeys/default.txt but with some minor modifications
  # so that the record is valid.
  value = join("", [
    "v=DKIM1; ",
    "h=sha256; ",
    "k=rsa; ",
    "t=s; ",
    "s=email; ",
    "p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5vklwMXvRD0FTQSVgjRT6JBkt7r2e4VL5ZU",
    "4Lty5wsM2YAy1bPCp9q7J9SEAdMoxNedIO/oon2w3i8om+EqI946oL7vIPme4MUoK9ZDXCvSo/sKazulQ",
    "QK9EFna7DMKtD42K3/KFIN9ik57a8+cvQh0MhEJYk8xWqdeiXXt2IXmid7R9PSSucMwhcYnInNhTn0cFt",
    "2Xv/vPqxZSOlAtAjmyyLnWL7K91H63xqtH+Oi/LfkomiO9jDlyk4MU4LkRvMMWT3g/GkZO1FfPBRO9kmf",
    "8Ig7uhhI0ZArA4hONt7o812dpbLyp6zm5DCKfHnbNuONWdCS3pRjXdo/Y0WCRwZQIDAQAB",
  ])
}

resource "digitalocean_record" "production_dmarc" {
  domain = data.digitalocean_domain.main.id
  type   = "TXT"
  name   = "_dmarc"
  ttl    = 14400
  value  = "v=DMARC1; p=reject; adkim=s; aspf=s; rua=mailto:tootmaster2021@wooz.dev"
}

resource "local_file" "sshconfig" {
  filename             = "${path.root}/ssh/net.sshconfig"
  file_permission      = "0644"
  directory_permission = "0755"
  content              = <<-EOF
    # vim:filetype=sshconfig

    Host ${hcloud_server.maintenance.name}
      Hostname ${hcloud_server.maintenance.ipv4_address}
      User root
      ForwardAgent yes
      StrictHostKeyChecking yes

    Host ${hcloud_server.lb.name}
      Hostname ${hcloud_server_network.lb.ip}
      User root
      UserKnownHostsFile /dev/null
      StrictHostKeyChecking no
      ProxyJump maintenance.${var.web_domain}

    Host ${hcloud_server.nat.name}
      Hostname ${hcloud_server_network.nat.ip}
      User root
      UserKnownHostsFile /dev/null
      StrictHostKeyChecking no
      ProxyJump maintenance.${var.web_domain}
  EOF
}

resource "local_file" "topology" {
  filename             = "${path.root}/topology.json"
  file_permission      = "0644"
  directory_permission = "0755"
  content = join(
    "\n", [
      jsonencode(local.topology),
      ""
  ])
}

output "topology" {
  value = local.topology
}
