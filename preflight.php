#!/usr/bin/env php
<?php
declare(strict_types=1);
$essResults = [];

foreach (
    [
        'age' => '--version',
        'composer' => '--version',
        'doctl' => 'version',
        'hcloud' => 'version',
        'make' => '--version',
        'node' => '--version',
        'php' => '--version',
        'sops' => '--version',
        'tofu' => '--version',
    ]
    as $tool => $cmd
) {
    $ret = -1;
    system("{$tool} {$cmd} >/dev/null 2>&1", $ret);
    if ($ret != 0) {
        echo "ERROR: '{$tool}' is unavailable or sad (check README.md for a link 💚)" .
            PHP_EOL;
        array_push($essResults, false);
        continue;
    }

    array_push($essResults, true);
    echo "INFO: '{$tool}' ✔️ " . PHP_EOL;
}

if (in_array(false, $essResults)) {
    exit(86);
}

echo 'HUZZAH: essential tooling looking good 🌟' . PHP_EOL;

$extResults = [];

foreach (
    [
        'direnv' => 'version',
        'vagrant' => '--version',
        'virtualbox' => '--help',
    ]
    as $tool => $cmd
) {
    $ret = -1;
    system("{$tool} {$cmd} >/dev/null 2>&1", $ret);
    if ($ret != 0) {
        echo "WARNING: '{$tool}' is unavailable or sad (check README.md for a link " .
            '(if you want to!) 💚)' .
            PHP_EOL;
        array_push($extResults, false);
        continue;
    }

    array_push($extResults, true);
    echo "INFO: '{$tool}' ✔️ " . PHP_EOL;
}

if (in_array(false, $extResults)) {
    echo 'WARNING: extended tooling is not fully available.' . PHP_EOL;
    exit(0);
}

echo 'HUZZAH: extended tooling looking good 🌟' . PHP_EOL;

