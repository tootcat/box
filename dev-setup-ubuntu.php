#!/usr/bin/env php
<?php
declare(strict_types=1);
function usage() {
    echo <<<USAGE
    Usage: dev-setup-ubuntu.php [-h]

    Run all the necessary steps to ensure essential development and operations tooling is
    available and working as expected on an Ubuntu machine.

    USAGE;
}

function err(string $msg) {
    fprintf(STDERR, 'ERROR: ' . rtrim($msg) . PHP_EOL);
}

function info(string $msg) {
    fprintf(STDOUT, 'INFO: ' . rtrim($msg) . PHP_EOL);
}

function main() {
    $options = getopt('h');

    if (!($options['h'] ?? true)) {
        usage();
        exit(0);
    }

    $PATH = getenv('PATH');
    $vendorBin = __DIR__ . '/vendor/bin';
    putenv("PATH={$vendorBin}:{$PATH}");

    ensureComposer() || exit(86);
    ensureComposerDependencies() || exit(86);
    execLocalProvisioning();
}

function ensureComposer() {
    $ret = -1;
    system('composer list >/dev/null 2>&1', $ret);
    if ($ret == 0) {
        info("found 'composer' yay");
        return true;
    }

    err(
        "No 'composer' found. Please run: " .
            "'apt-get install composer php-xml php-curl' 💚",
    );
    return false;
}

function ensureComposerDependencies() {
    $ret = -1;
    system('composer install', $ret);
    if ($ret == 0) {
        info("installed 'composer' dependencies yay");
        return true;
    }

    err('OH NO composer install failed');
    return false;
}

function execLocalProvisioning() {
    pcntl_exec('vendor/bin/dep', ['provision:local', 'localhost', '-v']);
}

if ($argv && $argv[0] && realpath($argv[0]) === __FILE__) {
    main();
}

