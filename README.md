# Boxes!

Things to manage toot.cat via [deployer](https://deployer.org/) on Ubuntu 22.04 LTS.

## dev setup

Step zero is cloning down this repository into a suitable working directory, e.g.:

```
git clone git@gitlab.com:tootcat/boxes.git ~/code/tootcat/boxes
```

### dev setup on ubuntu

With a minimum of PHP available on a machine running Ubuntu, the `dev-setup-ubuntu.php`
script may be used to get all of the essential tooling ready:

```bash
./dev-setup-ubuntu.php
```

### dev setup anywhere else

On all other platforms, or if you prefer to do these things some other way, one _MUST_
have:

- [age](https://age-encryption.org/)
- [composer](https://getcomposer.org/)
- [doctl](https://docs.digitalocean.com/reference/doctl/how-to/install/)
- [hcloud](https://github.com/hetznercloud/cli?tab=readme-ov-file#installation)
- [make](https://www.gnu.org/software/make/)
- [node](https://nodejs.org/en/download)
- [php](https://www.php.net/manual/en/install.php)
- [sops](https://getsops.io/)
- [tofu](https://opentofu.org/docs/intro/install/)

and one _SHOULD_ (probably) have:

- [direnv](https://direnv.net/)
- [vagrant](https://www.vagrantup.com/)
- [virtualbox](https://www.virtualbox.org/)

### dev setup preflight check

One may check if the necessary tools are available and working as expected by running this
"preflight check" script:

```bash
./preflight.php
```

### dev setup secrets management

All secrets are managed by [`sops`](https://getsops.io/) in [dotenv
format](https://12factor.net/config) files located in the [`envs`](./envs) directory using
the [`age`](https://age-encryption.org) encryption backend.

The recipients targeted when encrypting secrets should be provided via the
`SOPS_AGE_RECIPIENTS` environment variable, as shown in
[`.envrc.recommended`](./.envrc.recommended).

In order to do **pretty much anything meaningful** with the code in this repository, one
must have one of the recipient secret keys available at `~/.config/sops/age/keys.txt`.
This may be generated with `age-keygen` like so:

```bash
mkdir -p ~/.config/sops/age/
age-keygen -o ~/.config/sops/age/keys.txt
```

after which the public key must be added to the `SOPS_AGE_RECIPIENTS` environment variable
in `.envrc.recommended`, and then someone who already has access must re-encrypt all
secrets by running `dep sops:envs:reencrypt`.

### dev setup hcloud context

The `hcloud` tool requires a valid "context" to be configured. This may be done once
secrets are being decrypted correctly via the `HCLOUD_TOKEN` environment variable defined
in [`./envs/all.enc.env`](./envs/all.enc.env). If `.envrc.recommended` is being sourced
into one's `.envrc`, then `HCLOUD_TOKEN` may already be defined.

```bash
if printenv HCLOUD_TOKEN >/dev/null; then
  echo 'Yay HCLOUD_TOKEN is available'
else
  echo 'OH NO (ask for halp)'
fi
```

With `HCLOUD_TOKEN`'s existence confirmed, then the `hcloud` tool auth context may be
configured like so:

```bash
hcloud context create tootcat
```

## repository structure

- [`assets`](./assets) contains "asset" files which are meant to be uploaded via
  [`dep`](https://deployer.org), including executable scripts.
- [`composer.json`](./composer.json) is used by [composer](https://getcomposer.org) to
  install dependencies into [`vendor`](./vendor). It is an extremely normal-looking
  `composer.json` and _should_ work well with `composer install`.
- [`composer.lock`](./composer.lock) is the locky thing related to `composer.json`.
- [`deploy.php`](./deploy.php) is the main entrypoint for all the things, and is
  understood by [dep](https://deployer.org/). A list of available commands may be viewed
  by running `dep list`.
- [`docs`](./docs) contains yet more documentation, including the
  [runbook](./docs/runbook.md) which _may_ be very useful.
- [`envs`](./envs) contains files in [dotenv format](https://12factor.net/config).
- [`LICENSE`](./LICENSE) includes legalese and SHOUTING.
- [`Makefile`](./Makefile) contains mostly `.PHONY` targets for running tiny shell
  scripts, with some dependencies for convenience. Running `make help` will show more
  information.
- [`package.json`](./package.json) is used by `npm` (installed with `node`) mostly to
  install [`prettier`](https://prettier.io/) which is used by the `make fmt` target.
- [`package-lock.json`](./package-lock.json) is the locky thing related to `package.json`.
- [`phpunit.xml`](./phpunit.xml) is the configuration file required by `phpunit`, which is
  used internally by `pest`.
- [`README.md`](./README.md) is this document.
- [`recipe`](./recipe) contains yet more code that is used by [`deploy.php`](./deploy.php)
  and is named like this (as opposed to "recipes") because it is the
  convention used within [the Deployer
  codebase](https://github.com/deployphp/deployer/tree/master/recipe).
- [`src`](./src) contains yet even more code that is imported by
  [`deploy.php`](./deploy.php) and used in various places within `./recipe` code.
- [`templates`](./templates) contains template files which are "rendered" via
  [`dep`](https://deployer.org)'s [`parse`
  function](https://deployer.org/docs/7.x/api#parse) and are stored in separate files
  instead of string literals primarily so that syntax highlighting can be a teensy bit
  helpful.
- [`tests`](./tests) contains PHP tests meant to be run via `make test`.
- [`tf`](./tf) contains code understood by [tofu](https://opentofu.org) for provisioning
  base infrastructure resources for the `staging` and `production` environments.
- [`Vagrantfile`](./Vagrantfile) is used by [vagrant](https://www.vagrantup.com/) (with
  [virtualbox](https://www.virtualbox.org/)) for locally developing with a system
  running SSH and systemd as is wanted by [dep](https://deployer.org/) since doing this
  with a Docker container or some such containery thing is fraught and full of papercuts.
- [`vagrant.sshconfig`](./vagrant.sshconfig) is used via [`deploy.php`](./deploy.php) when
  accessing the local VM run via [vagrant](https://www.vagrantup.com/).
- [`vendor`](./vendor) is not tracked in `git` and will only appear once
  [composer](https://getcomposer.org) _does stuff_.

> NOTE: Each of the directories adjacent to this file _may_ include a `README.md` which
> explains what is going on in there.

## infrastructure

### staging and production infrastructure overview

In which the boxes shown here are _mostly_ "servers":

- `localhost` means the local environment / host from which you (a human?) uses tools like
  `dep` and `tofu`.
- `role.maintenance` means the server (in Hetzner Cloud) that is configured to allow
  SSH connections on its public IPv4 address _and also_ has a private IPv4 address
  within the private network also used by `role.web`, `role.db`, and `role.lb`.
- `role.lb` means the load balancer that allows HTTP and HTTPS connections on its
  public IPv4 address.
- `role.web` means the server(s) that run the Mastodon Glitch Edition :tm: code, and are
  configured to only allow HTTP and HTTPS connections on the private IPv4 address so
  that the `lb` may "target" them and handle all public connections.
- `role.db` means the server that is primarily responsible for running PostgreSQL, and
  also Valkey and ElasticSearch. It only allows incoming traffic on its private IPv4
  address.
- `role.nat` means the server that allows the servers within the private subnets to access
  the internet, such as when installing package updates.
- `browser` means the public internet client whatevers that connect to the public IPv4
  address of the `lb`.

```
                   ┌───────hetzner──cloud───────────┐
                   │                                │
                   │                            ┌───┴────┐        ┌────────┐
                   │       ┌───────────────┬────► role.lb│◄───────┤ browser│
                   │       │               │    └───┬────┘        └────────┘
                   │       ▼               ▼        │  ▲
                   │     ┌───────┐      ┌───────┐   │  │     ┌────────┐
                   │     │ role. │      │ role. │   │  └─────┤ browser│
                   │     │  web  │      │  web  │   │  ▲     └────────┘
┌──────────┐       │     └────▲──┘      └───▲───┘   │  │           ┌────────┐
│ localhost│       │          │             │       │  └───────────┤ browser│
└────────┬─┘       │          │   ┌───────┐ │       │              └────────┘
         │    ┌────┴────────┐ └───► role. ◄─┘       │
         └───►│role.maintenance   │  db   │      ┌──┴──────┐
              └────┬────────┘     └───────┘ ─ ─ ─► role.nat│─ ─ ─►
                   │                             └──┬──────┘
                   └────────────────────────────────┘
```

The staging setup is _mostly_ meant to work like the production setup, but is _not_
expected to be running at all times. This is mostly a cost savings measure. Unlike "local
mode", the staging setup runs on _multiple_ servers with different roles, and in Hetzner
Cloud instead of locally with Vagrant and Virtualbox.

### local infrastructure

```
┌──────────localhost──────────┐
│                             │
│              ┌───vagrant──┐ │
│              │            │ │
│  human ─────►│ stage=local│ │
│              │            │ │
│              └────────────┘ │
│                             │
└─────────────────────────────┘
```

<!--
vim:tw=90
-->
