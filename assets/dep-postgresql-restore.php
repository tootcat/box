#!/usr/bin/env php
<?php
declare(strict_types=1);
function usage() {
    echo <<<USAGE
    Usage: dep-postgresql-restore.php [-h] [-l] [-d <database>] [-b <backup-timestamp>]

    Restores a backup of the named <database> from remote storage.

    In addition to the command-line options shown above, these environment variables will
    affect how backups are created and copied to remote storage:

      TMPDIR - temporary directory to which the database backup will be copied from remote
               storage prior to restore

      STORE_DEST_HOST - [username@]hostname destination for remote storage
      STORE_DEST_PREFIX - directory/path prefix for remote storage

    USAGE;
}

function err(string $msg) {
    fprintf(STDERR, 'ERROR: ' . rtrim($msg) . PHP_EOL);
}

function info(string $msg) {
    fprintf(STDOUT, 'INFO: ' . rtrim($msg) . PHP_EOL);
}

function main() {
    $options = getopt('hd:b:l');

    if (!($options['h'] ?? true)) {
        usage();
        exit(0);
    }

    $database = $options['d'] ?? null;
    $backupTimestamp = $options['b'] ?? null;

    if (empty($database)) {
        err('missing required -d <database>');
        exit(86);
    }

    $tmpdir = getenv('TMPDIR');
    $tmpdir = $tmpdir ? $tmpdir : '/tmp';

    if (empty(getenv('STORE_DEST_HOST') ?? '')) {
        err('missing required environment variable STORE_DEST_HOST');
        exit(86);
    }

    if (empty(getenv('STORE_DEST_PREFIX') ?? '')) {
        err('missing required environment variable STORE_DEST_PREFIX');
        exit(86);
    }

    [$allBackups, $ok] = listBackups($database);
    if (!$ok) {
        exit(86);
    }

    if (!($options['l'] ?? true)) {
        foreach ($allBackups as $bak) {
            echo $bak . PHP_EOL;
        }

        exit(0);
    }

    if (empty($backupTimestamp)) {
        $latestBackup = $allBackups[array_key_last($allBackups)];

        info("setting backup timestamp from latest {$latestBackup}");

        $backupTimestamp = basename(
            str_replace("{$database}-", '', $latestBackup),
            '.tar',
        );
    }

    exit(restorePostgresql($database, $tmpdir, $backupTimestamp));
}

function listBackups(string $database): array {
    $remote = '"${STORE_DEST_HOST}"';
    $hostname = gethostname();
    $backupsPrefix = "\"\${STORE_DEST_PREFIX:-.}\"/boxes/db/{$hostname}/backups";

    $ret = -1;
    $allBackups = [];
    exec(
        implode(' ', [
            'ssh',
            '-i',
            '~/.ssh/id_store',
            '-o',
            'StrictHostKeyChecking=no',
            '-o',
            'UserKnownHostsFile=/dev/null',
            $remote,
            'ls',
            '-1',
            $backupsPrefix,
        ]),
        $allBackups,
        $ret,
    );

    if ($ret != 0) {
        err('listing remote database backups failed');
        return [null, false];
    }

    $allBackups = array_filter(
        $allBackups,
        fn(string $entry) => str_starts_with($entry, $database . '-'),
    );

    sort($allBackups);

    return [$allBackups, true];
}

function restorePostgresql(
    string $database,
    string $tmpdir,
    string $backupTimestamp,
) {
    echo "restoring {$database} backup {$backupTimestamp}";

    $remote = '"${STORE_DEST_HOST}"';
    $hostname = gethostname();
    $backupsPrefix = "\"\${STORE_DEST_PREFIX:-.}\"/boxes/db/{$hostname}/backups";

    [$localTar, $ok] = rsyncTarPgDump(
        $database,
        $remote,
        $backupTimestamp,
        $backupsPrefix,
    );
    if (!$ok) {
        return 86;
    }

    if (!untarPgDump(dirname($localTar), $localTar)) {
        return 86;
    }

    if (!runPgRestore($database, dirname($localTar))) {
        return 86;
    }

    if (!cleanupRestoreFiles($database, dirname($localTar), $localTar)) {
        return 86;
    }

    return 0;
}

function rsyncTarPgDump(
    string $database,
    string $remote,
    string $backupTimestamp,
    string $backupsPrefix,
): array {
    info("rsyncing {$database} database backup {$backupTimestamp} to local");

    $remoteTar = "{$backupsPrefix}/{$database}-{$backupTimestamp}.tar";
    $localTar = "/tmp/{$database}-{$backupTimestamp}.tar";

    $ret = -1;
    system(
        implode(' ', [
            'rsync',
            '-avzP',
            '-e',
            '"',
            'ssh',
            '-i',
            '~/.ssh/id_store',
            '-o',
            'StrictHostKeyChecking=no',
            '-o',
            'UserKnownHostsFile=/dev/null',
            '"',
            "{$remote}:{$remoteTar}",
            "{$localTar}",
        ]),
        $ret,
    );

    if ($ret != 0) {
        err('rsync of database backup failed');
        return ['', false];
    }

    return [$localTar, true];
}

function untarPgDump(string $workingDir, string $localTar): bool {
    info("untarring {$localTar}");

    $ret = -1;
    system("tar -C {$workingDir} -xf {$localTar}", $ret);
    if ($ret != 0) {
        err('untar of database backup failed');
        return false;
    }

    return true;
}

function runPgRestore(string $database, string $workingDir): bool {
    info("running pg_restore of {$database} database");

    $ret = -1;
    system(
        implode(' ', [
            "cd {$workingDir} &&",
            implode(' ', [
                "(dropdb {$database} || true) &&",
                "createdb -T template0 {$database} &&",
                "pg_restore --dbname {$database} --exit-on-error",
                "--format=d --jobs=4 ./{$database}",
            ]),
        ]),
        $ret,
    );
    if ($ret != 0) {
        err('restore of database backup failed');
        return false;
    }

    return true;
}

function cleanupRestoreFiles(
    string $database,
    string $workingDir,
    string $localTar,
): bool {
    info("cleaning up restore files for {$database} database");

    $ret = -1;
    system(
        implode(' ', [
            "cd {$workingDir} &&",
            "rm -rf ./{$database} {$localTar}",
        ]),
        $ret,
    );
    if ($ret != 0) {
        err('cleanup of database backup failed');
        return false;
    }

    return true;
}

if ($argv && $argv[0] && realpath($argv[0]) === __FILE__) {
    main();
}

