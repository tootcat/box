#!/usr/bin/env php
<?php
declare(strict_types=1);
function usage() {
    echo <<<USAGE
    Usage: dep-postgresql-backup.php [-h] [-d <database>] [-k <n_keepers>]

    Create a backup of the named <database>, copy it to remote storage, and remove any
    previous backups beyond the number of <n_keepers>.

    In addition to the command-line options shown above, these environment variables will
    affect how backups are created and copied to remote storage:

      TMPDIR - temporary directory to which the database backup will be stored prior to copy
          to remote storage

      STORE_DEST_HOST - [username@]hostname destination for remote storage
      STORE_DEST_PREFIX - directory/path prefix for remote storage

    USAGE;
}

define('LEASE_EXPIRY_SECONDS', 7200);

function err(string $msg) {
    fprintf(STDERR, 'ERROR: ' . rtrim($msg) . PHP_EOL);
}

function info(string $msg) {
    fprintf(STDOUT, 'INFO: ' . rtrim($msg) . PHP_EOL);
}

function main() {
    $options = getopt('hd:k:');

    if (!($options['h'] ?? true)) {
        usage();
        exit(0);
    }

    $database = $options['d'] ?? null;
    $nKeepers = intval($options['k'] ?? 5);

    if (empty($database)) {
        err('missing required -d <database>');
        exit(86);
    }

    $tmpdir = getTmpDir();

    if (empty(getenv('STORE_DEST_HOST') ?? '')) {
        err('missing required environment variable STORE_DEST_HOST');
        exit(86);
    }

    if (empty(getenv('STORE_DEST_PREFIX') ?? '')) {
        err('missing required environment variable STORE_DEST_PREFIX');
        exit(86);
    }

    if (!acquireLease($tmpdir)) {
        err('failed to acquire lease');
        exit(86);
    }

    register_shutdown_function('releaseLease');

    exit(backupPostgresql($database, $tmpdir, $nKeepers));
}

function getTmpDir(): string {
    $tmpdir = getenv('TMPDIR');
    $tmpdir = $tmpdir ? $tmpdir : '/tmp';
    return $tmpdir;
}

function getLeaseFile(): string {
    $tmpdir = getTmpDir();
    return "{$tmpdir}/.dep-postgresql-backup.lease";
}

function acquireLease(): bool {
    $leaseFile = getLeaseFile();
    if (file_exists($leaseFile)) {
        $content = json_decode(file_get_contents($leaseFile), true);
        if (time() - $content['timestamp'] < LEASE_EXPIRY_SECONDS) {
            return false;
        }
    }

    file_put_contents(
        $leaseFile,
        json_encode(['timestamp' => time(), 'pid' => posix_getpid()]),
    );

    return true;
}

function releaseLease() {
    unlink(getLeaseFile());
}

function backupPostgresql(
    string $database,
    string $tmpdir,
    int $nKeepers,
): int {
    [$destDir, $ok] = mkDestDir($database, $tmpdir);
    if (!$ok) {
        return 86;
    }

    [$destDump, $ok] = pgDump($database, $destDir);
    if (!$ok) {
        return 86;
    }

    [$destTar, $ok] = tarPgDump($database, $destDump, $destDir);
    if (!$ok) {
        return 86;
    }

    $remote = '"${STORE_DEST_HOST}"';
    $hostname = gethostname();
    $backupsPrefix = "\"\${STORE_DEST_PREFIX:-.}\"/boxes/db/{$hostname}/backups";

    [$remoteDestTar, $ok] = rsyncTarPgDump(
        $database,
        $destTar,
        $remote,
        $backupsPrefix,
    );
    if (!$ok) {
        return 86;
    }

    [$nCleaned, $ok] = cleanUpBackups(
        $database,
        $remote,
        $backupsPrefix,
        $nKeepers,
    );
    if (!$ok) {
        return 86;
    }

    info("cleaned {$nCleaned} expired backups for {$database} database");

    system("rm -rf {$destDir}");

    return 0;
}

function mkDestDir(string $database, string $tmpdir): array {
    info("creating temp {$database} database backup dir");

    $destDir = tempnam($tmpdir, "postgresql-backup-{$database}-");

    if (false == unlink($destDir)) {
        err('failed to unlink temporary backup dir name');
        return ['', false];
    }

    if (false == mkdir($destDir, 0750, true)) {
        err('failed to make temporary backup dir');
        return ['', false];
    }

    return [$destDir, true];
}

function pgDump(string $database, string $destDir): array {
    info("creating {$database} database dump");

    $destDump = "{$destDir}/{$database}";
    $ret = -1;
    system("pg_dump --format=d --file={$destDump} {$database}", $ret);

    if ($ret != 0) {
        err('pg_dump failed');
        return ['', false];
    }

    return [$destDump, true];
}

function tarPgDump(string $database, string $destDump, string $destDir): array {
    info("creating tar of {$database} database dump");

    $destTar = "{$destDump}.tar";
    $ret = -1;
    system("/usr/bin/tar -C {$destDir} -cf {$destTar} {$database}", $ret);

    if ($ret != 0) {
        err('tar of database backup failed');
        return ['', false];
    }

    return [$destTar, true];
}

function rsyncTarPgDump(
    string $database,
    string $destTar,
    string $remote,
    string $backupsPrefix,
): array {
    info("creating remote {$database} tar prefix");

    $ret = -1;
    system(
        implode(' ', [
            'ssh',
            '-i',
            '~/.ssh/id_store',
            '-o',
            'StrictHostKeyChecking=no',
            '-o',
            'UserKnownHostsFile=/dev/null',
            $remote,
            'mkdir',
            '-p',
            $backupsPrefix,
        ]),
        $ret,
    );

    if ($ret != 0) {
        err('mkdir of database backup prefix failed');
        return ['', false];
    }

    info("rsyncing {$database} database backup tar to remote");

    $backupTimestamp = date('Ymd\THis', filemtime($destTar));
    $remoteDestTar = "{$backupsPrefix}/{$database}-{$backupTimestamp}.tar";

    $ret = -1;
    system(
        implode(' ', [
            'rsync',
            '-avzP',
            '-e',
            '"',
            'ssh',
            '-i',
            '~/.ssh/id_store',
            '-o',
            'StrictHostKeyChecking=no',
            '-o',
            'UserKnownHostsFile=/dev/null',
            '"',
            "{$destTar}",
            "{$remote}:{$remoteDestTar}",
        ]),
        $ret,
    );

    if ($ret != 0) {
        err('rsync of database backup failed');
        return ['', false];
    }

    return [$remoteDestTar, true];
}

function cleanUpBackups(
    string $database,
    string $remote,
    string $backupsPrefix,
    int $nKeepers,
): array {
    info("cleaning up expired backups for {$database} database");

    $ret = -1;
    $allBackups = [];
    exec(
        implode(' ', [
            'ssh',
            '-i',
            '~/.ssh/id_store',
            '-o',
            'StrictHostKeyChecking=no',
            '-o',
            'UserKnownHostsFile=/dev/null',
            $remote,
            'ls',
            '-1',
            $backupsPrefix,
        ]),
        $allBackups,
        $ret,
    );

    if ($ret != 0) {
        err('listing remote database backups failed');
        return [0, false];
    }

    rsort($allBackups);

    $expiredBackups = array_slice($allBackups, $nKeepers);

    foreach ($expiredBackups as $expBak) {
        $ret = -1;
        system(
            implode(' ', [
                'ssh',
                '-i',
                '~/.ssh/id_store',
                '-o',
                'StrictHostKeyChecking=no',
                '-o',
                'UserKnownHostsFile=/dev/null',
                $remote,
                'rm',
                '-vf',
                "{$backupsPrefix}/{$expBak}",
            ]),
            $ret,
        );

        if ($ret != 0) {
            err('delete of expired database backup(s) failed');
            return [0, false];
        }
    }

    return [count($expiredBackups), true];
}

if ($argv && $argv[0] && realpath($argv[0]) === __FILE__) {
    main();
}

