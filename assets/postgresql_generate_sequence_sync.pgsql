-- vim:filetype=pgsql
-- NOTE: This query is intended to be run by a *superuser* with command
-- line flags of '-XAtq' to ignore ~/.psqlrc, output in unalinged mode,
-- print rows only, and print query output only.
SELECT
  $$select setval('$$ ||
  quote_ident(schemaname) || $$.$$ || quote_ident(sequencename) ||
  $$', $$ ||
  last_value ||
  $$); $$
AS sql
FROM pg_sequences
;
