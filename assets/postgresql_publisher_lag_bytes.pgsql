-- vim:filetype=pgsql
SELECT
  slot_name,
  confirmed_flush_lsn,
  pg_current_wal_lsn(),
  (
    pg_current_wal_lsn() - confirmed_flush_lsn
  ) AS lsn_distance
FROM pg_replication_slots;
