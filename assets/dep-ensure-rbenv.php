#!/usr/bin/env php
<?php
declare(strict_types=1);
$home = getenv('HOME');
$rbenvGitURL = getenv('RBENV_GIT_URL') ?: 'https://github.com/rbenv/rbenv.git';
$rubyBuildGitURL =
    getenv('RUBY_BUILD_GIT_URL') ?: 'https://github.com/rbenv/ruby-build.git';
file_put_contents(
    "$home/.bashrc.rbenv",
    <<<'BASH'
    export PATH="$HOME/.rbenv/bin:$PATH"
    export RUBY_CONFIGURE_OPTS=--with-jemalloc
    eval "$(rbenv init -)"

    BASH
    ,
);

if (!str_contains(file_get_contents("$home/.bashrc"), '## init rbenv')) {
    file_put_contents(
        "$home/.bashrc",
        <<<'BASH'
        ## init rbenv
        source ~/.bashrc.rbenv

        BASH
        ,
        FILE_APPEND | LOCK_EX,
    );
}

$rbenvTmp = sprintf('/tmp/%s', uniqid('rbenv_'));
system("git clone $rbenvGitURL $rbenvTmp");

$curSHA1 = system("git -C $home/.rbenv rev-parse HEAD || true");
$tmpSHA1 = system("git -C $rbenvTmp rev-parse HEAD || true");

if ($curSHA1 != $tmpSHA1) {
    $bak = "$home/.rbenv.bak." . time();

    rename("$home/.rbenv", $bak);
    rename($rbenvTmp, "$home/.rbenv");
    system("rsync -avP $bak/versions $home/.rbenv/ || true");
}

if (is_dir("$home/.rbenv/plugins/ruby-build") !== true) {
    system("git clone $rubyBuildGitURL $home/.rbenv/plugins/ruby-build");
}

echo "Cloned rbenv from $rbenvGitURL at revision:";
system("git -C $home/.rbenv rev-parse HEAD");

echo "Cloned ruby-build from $rubyBuildGitURL at revision:";
system("git -C $home/.rbenv/plugins/ruby-build rev-parse HEAD");

$previousClone = glob("$home/.rbenv.bak.*");

if (count($previousClone) > 1) {
    foreach (array_slice($previousClone, 1) as &$d) {
        echo "Removing previous clone at $d";
        system("rm -rf $d");
    }
}

