<?php declare(strict_types=1);
namespace Deployer;

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/src/store.php';
require_once __DIR__ . '/src/install.php';
require_once __DIR__ . '/src/roles.php';

import(__DIR__ . '/recipe/provision.php');
import(__DIR__ . '/recipe/rails.php');
import(__DIR__ . '/recipe/hetzlet.php');
import(__DIR__ . '/recipe/lb.php');
import(__DIR__ . '/recipe/maintenance.php');
import(__DIR__ . '/recipe/sops.php');

define('PRODUCTION_HOSTNAME', 'toot.cat');

set('target', 're-live');

set('alertmanager_smtp_from', 'nobody+alertmanager@{{mail_domain}}');
set('assets_dir', __DIR__ . '/assets');
set('bundler_user', 'mastodon');
set('db_type', 'postgresql');
set('deploy_path', '/var/www/mastodon');
set('dkim_selector', 'default');
set('doctl_sld', PRODUCTION_HOSTNAME);
set('elasticsearch_http_port', 9200);
set('elasticsearch_transport_port', 9300);
set('grafana_hostname', 'localhost');
set('grafana_http_port', 3000);
set('hcloud_metadata', null);
set('hcloud_network_name', 'tootcat');
set('logrotate_rsyslog_maxsize', '1G');
set('logrotate_rsyslog_rotate', '4');
set('logs_systemd_unit', 'mastodon-web');
set('mail_domain', PRODUCTION_HOSTNAME);
set('mail_outbound_hostname', 'nat.toot.cat');
set('mastodon_env_append', '');
set('mastodon_http_port', 3000);
set('mastodon_sidekiq_args', '-c 25');
set('mastodon_sidekiq_db_pool', '25');
set('mastodon_streaming_port', 4000);
set('nginx_site_template', 'nginx-site.conf.tmpl');
set('nodejs_version', 'node_18.x');
set('otlp_port', 4318);
set('php_version', '8.3');
set('postgresql_port', 5432);
set('private_ipv4', '127.0.0.1');
set('prometheus_blackbox_exporter_port', 9115);
set('prometheus_elasticsearch_exporter_port', 9114);
set('prometheus_node_exporter_port', 9100);
set('prometheus_port', 9090);
set('prometheus_postfix_exporter_port', 9154);
set('prometheus_postgres_exporter_port', 9187);
set('prometheus_remote_write_port', 9411);
set('prometheus_statsd_exporter_port', 9102);
set('prometheus_traefik_exporter_port', 8083);
set('prometheus_valkey_exporter_port', 9121);
set('public_path', 'public');
set('rbenv_ruby_versions', ['3.3.5', '3.3.6', '3.4.1', '3.4.2']);
set('rbenv_user', 'mastodon');
set('repository', 'https://gitlab.com/tootcat/glitch.git');
set('smoke_test_base_url', 'https://toot.cat');
set('ssh_log_level', 'INFO');
set('stage', 'UNDEFINED');
set('store_db_prefix', '{{store_dest_prefix}}/boxes/db');
set('store_dest_host', '{{store_user}}@{{lb_private_ipv4}}');
set('store_dest_prefix', 'store/tootcat');
set('store_home_prefix', '{{store_dest_prefix}}/boxes/home');
set('store_shared_prefix', '{{store_dest_prefix}}/glitch');
set('swap_multiplier', 0);
set('templates_dir', __DIR__ . '/templates');
set('tootcat_sld', PRODUCTION_HOSTNAME);
set('tootctl_command', '--help');
set('traefik_ping_port', 8082);
set('use_certbot', true);
set('use_local_cert', false);
set('valkey_port', 6379);
set('working_path', '/var/tmp');

add('shared_files', []);
add('shared_dirs', [
    'public/assets',
    'public/packs',
    'public/system',
    'tmp/cache',
]);
add('writable_dirs', []);

set(
    'ssh_kex_algorithms',
    join(',', [
        'curve25519-sha256',
        'curve25519-sha256@libssh.org',
        'ecdh-sha2-nistp256',
        'ecdh-sha2-nistp384',
        'ecdh-sha2-nistp521',
        'sntrup761x25519-sha512@openssh.com',
        'diffie-hellman-group-exchange-sha256',
        'diffie-hellman-group16-sha512',
        'diffie-hellman-group18-sha512',
        'diffie-hellman-group14-sha256',
    ]),
);

set('store_client_ssh_id', function () {
    return gzdecode(
        base64_decode(get('stage_env')['STORE_CLIENT_SSH_PRIVATE_KEY_GZB64']),
    );
});

set('store_client_ssh_id_pub', function () {
    return gzdecode(
        base64_decode(get('stage_env')['STORE_CLIENT_SSH_PUBLIC_KEY_GZB64']),
    );
});

set('network_metadata', function () {
    return json_decode(
        runLocally('hcloud network describe tootcat --output json'),
        true,
    );
});

set('network_topology', function () {
    return json_decode(
        file_get_contents(__DIR__ . '/tf/net/topology.json'),
        true,
    );
});

set('network_cidr', function () {
    return get('network_topology')['cidr'];
});

set('nproc', function () {
    return run('nproc');
});

set('private_subnet_cidr', function () {
    return get('network_topology')['private'][get('stage')]['cidr'];
});

set('db_private_ipv4', function () {
    return getHostPrivateIP('db.{{stage}}.toot.cat');
});

set('lb_private_ipv4', function () {
    return getHostPrivateIP('lb.toot.cat');
});

set('nat_private_ipv4', function () {
    return getHostPrivateIP('nat.toot.cat');
});

set('maintenance_private_ipv4', function () {
    return getHostPrivateIP('maintenance.toot.cat');
});

set('mastodon_services', [
    'mastodon-web.service',
    'mastodon-sidekiq.service',
    'mastodon-streaming.service',
]);

set('rails_env', function () {
    return get('stage_env')['RAILS_ENV'] ?? 'NOTSET';
});

set('stage_env_clear_raw', function () {
    return file_get_contents(__DIR__ . '/envs/' . get('stage') . '.env');
});

set('stage_env_enc_raw', function () {
    return runLocally('sops -d ' . __DIR__ . '/envs/{{stage}}.enc.env');
});

set('stage_env_raw', function () {
    return implode("\n", [
        get('stage_env_clear_raw'),
        get('stage_env_enc_raw'),
        get('mastodon_env_append'),
    ]) . "\n";
});

set('stage_env', function () {
    return \Dotenv\Dotenv::parse(get('stage_env_raw'));
});

set('all_env_enc_raw', function () {
    return runLocally('sops -d ' . __DIR__ . '/envs/all.enc.env');
});

set('all_env_raw', function () {
    return implode("\n", [get('all_env_enc_raw')]) . "\n";
});

set('all_env', function () {
    return \Dotenv\Dotenv::parse(get('all_env_raw'));
});

set('db_name', function () {
    return ltrim(
        parse_url(
            parse(
                get('stage_env')['DATABASE_URL'] ??
                    'postgres://undefined:undefined@localhost:{{postgresql_port}}/undefined',
            ),
            PHP_URL_PATH,
        ),
        '/',
    );
});

set('db_user', function () {
    return parse_url(
        parse(
            get('stage_env')['DATABASE_URL'] ??
                'postgres://undefined:undefined@localhost:{{postgresql_port}}/undefined',
        ),
        PHP_URL_USER,
    );
});

set('db_password', function () {
    return parse_url(
        parse(
            get('stage_env')['DATABASE_URL'] ??
                'postgres://undefined:undefined@localhost:{{postgresql_port}}/undefined',
        ),
        PHP_URL_PASS,
    );
});

set('mail_relay_host', function () {
    return get('stage_env')['SMTP_RELAY_HOST'] ?? 'NOTSET';
});

set('rails_dotenv', function () {
    return get('stage_env');
});

set('valkey_user', function () {
    return parse_url(
        parse(
            get('stage_env')['VALKEY_URL'] ??
                'valkey://undefined:undefined@localhost:{{valkey_port}}/0',
        ),
        PHP_URL_USER,
    );
});

set('valkey_password', function () {
    return parse_url(
        parse(
            get('stage_env')['VALKEY_URL'] ??
                'valkey://undefined:undefined@localhost:{{valkey_port}}/0',
        ),
        PHP_URL_PASS,
    );
});

set('valkey_default_password', function () {
    return get('stage_env')['VALKEY_DEFAULT_PASSWORD'];
});

set('valkey_prometheus_password', function () {
    return get('stage_env')['VALKEY_PROMETHEUS_PASSWORD'];
});

set('sudo_password', function () {
    return get('stage_env')['DEPLOYER_PASS'];
});

set('grafana_db_password', function () {
    return get('stage_env')['GRAFANA_DB_PASS'];
});

set('shared_env', ['SOURCE_BASE_URL=https://gitlab.com/tootcat/glitch']);

set('top', function () {
    return trim(`git rev-parse --show-toplevel`);
});

// NOTE: The 'localhost' host is mostly a sentinel value for tasks that are meant
// to be run locally.
localhost()
    ->set('stage', 'local')
    ->set('deploy_path', __DIR__);

host('local.toot.cat')
    ->set('config_file', function () {
        return parse('{{top}}/vagrant.sshconfig');
    })
    ->set('domain', 'local.toot.cat')
    ->set('keep_releases', 3)
    ->set('mastodon_env_append', function () {
        return parse(
            implode(
                "\n",
                array_merge(get('shared_env'), [
                    'LOCAL_DOMAIN={{domain}}',
                    'ALTERNATE_DOMAINS=localhost,local.toot.cat',
                ]),
            ) . "\n",
        );
    })
    ->set('remote_user', 'vagrant')
    ->set('become', 'root')
    ->set('stage', 'local')
    ->set('use_local_cert', true)
    ->setLabels([
        'role.db' => 'ok',
        'role.opendkim' => 'ok',
        'role.postfix' => 'ok',
        'role.valkey' => 'ok',
        'role.web' => 'ok',
        'stage' => 'local',
    ]);

host('lb.toot.cat')
    ->set('hcloud_metadata', function () {
        return json_decode(
            runLocally('hcloud server describe lb.toot.cat --output json'),
            true,
        );
    })
    ->set('config_file', '{{top}}/tf.sshconfig')
    ->set('deploy_path', '/tmp')
    ->set('domain', 'lb.toot.cat')
    ->set('store_ssh_authorized_keys', function () {
        return gzdecode(
            base64_decode(get('stage_env')['STORE_SSH_AUTHORIZED_KEYS_GZB64']),
        );
    })
    ->set('store_ssh_id', function () {
        return gzdecode(
            base64_decode(get('stage_env')['STORE_SSH_PRIVATE_KEY_GZB64']),
        );
    })
    ->set('store_ssh_id_pub', function () {
        return gzdecode(
            base64_decode(get('stage_env')['STORE_SSH_PUBLIC_KEY_GZB64']),
        );
    })
    ->set('store_volume_mount', function () {
        $desc = json_decode(
            runLocally(
                'hcloud volume describe tootcat-lilstore-meta --output json',
            ),
            true,
        );
        return '/mnt/HC_Volume_' . strval($desc['id']);
    })
    ->set('floating_ips', function () {
        return describeFloatingIPs(
            get('hcloud_metadata')['public_net']['floating_ips'],
        );
    })
    ->set('private_ipv4', function () {
        return getHostPrivateIP('lb.toot.cat');
    })
    ->set('production_server_urls', function () {
        return json_encode(
            getWebServerURLs('production'),
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        );
    })
    ->set('remote_user', 'root')
    ->set('stage', 'meta')
    ->set('staging_server_urls', function () {
        return json_encode(
            getWebServerURLs('staging'),
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        );
    })
    ->set('swap_multiplier', 2)
    ->set('traefik_acme_email', function () {
        return get('all_env')['ACME_EMAIL'];
    })
    ->set('traefik_digitalocean_token', function () {
        return get('all_env')['DIGITALOCEAN_ACCESS_TOKEN'];
    })
    ->set('traefik_dynamic_configs', [
        '{{templates_dir}}/grafana.yml.tmpl',
        '{{templates_dir}}/production-traefik.yml.tmpl',
        '{{templates_dir}}/staging-traefik.yml.tmpl',
    ])
    ->set('traefik_log_level', 'DEBUG')
    ->set('ufw_rules', [
        'default deny incoming',
        'default allow outgoing',
        'allow from {{network_cidr}} proto tcp to any port 22',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_node_exporter_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_traefik_exporter_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{traefik_ping_port}}',
        'allow 80',
        'allow 443',
    ])
    ->set('web_domain', 'toot.cat')
    ->setLabels([
        'role.store' => 'ok',
        'role.lb' => 'ok',
        'role.nat_client' => 'ok',
        'role.traefik' => 'ok',
        'stage' => 'meta',
    ]);

host('maintenance.toot.cat')
    ->set('alertsmanager_alerting_targets', function () {
        return json_encode(
            ['localhost:9093'],
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        );
    })
    ->set('alertsmanager_receivers_email_configs', function () {
        return json_encode(
            [['to' => 'dan+tootmails@meatballhat.com']],
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        );
    })
    ->set(
        'blackbox_exporter_http_2xx_scrape_configs_static_configs',
        function () {
            return json_encode(
                [
                    ['targets' => ['https://toot.cat/health']],
                    ['targets' => ['https://grafana.toot.cat/api/health']],
                ],
                JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
            );
        },
    )
    ->set('config_file', '{{top}}/tf.sshconfig')
    ->set('deploy_path', '/tmp')
    ->set('domain', 'maintenance.toot.cat')
    ->set('elasticsearch_exporter_scrape_config_static_configs', function () {
        $staticConfigs = [];

        foreach (['staging', 'production'] as $env) {
            foreach (
                getPromScrapeStaticConfigs(
                    ['--selector', "env={$env},role.elasticsearch=ok"],
                    ['role' => 'elasticsearch', 'env' => $env],
                    '{{prometheus_elasticsearch_exporter_port}}',
                )
                as $cfg
            ) {
                array_push($staticConfigs, $cfg);
            }
        }

        return json_encode(
            $staticConfigs,
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        );
    })
    ->set('grafana_hostname', 'grafana.toot.cat')
    ->set('hcloud_metadata', function () {
        return json_decode(
            runLocally(
                'hcloud server describe maintenance.toot.cat --output json',
            ),
            true,
        );
    })
    ->set('hcloud_token_ro', function () {
        return get('stage_env')['HCLOUD_TOKEN_RO'];
    })
    ->set('node_exporter_scrape_config_meta', function () {
        $scrapeConfig = ['job_name' => 'node-meta', 'static_configs' => []];

        foreach (['lb', 'maintenance', 'nat'] as $role) {
            foreach (
                getPromScrapeStaticConfigs(
                    ['--selector', "role.{$role}=ok"],
                    ['role' => $role, 'env' => 'meta'],
                    '{{prometheus_node_exporter_port}}',
                )
                as $cfg
            ) {
                array_push($scrapeConfig['static_configs'], $cfg);
            }
        }

        return json_encode(
            $scrapeConfig,
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        );
    })
    ->set('node_exporter_scrape_config_staging', function () {
        $scrapeConfig = ['job_name' => 'node-staging', 'static_configs' => []];

        foreach (['web', 'db'] as $role) {
            foreach (
                getPromScrapeStaticConfigs(
                    ['--selector', "env=staging,role.{$role}=ok"],
                    ['role' => $role, 'env' => 'staging'],
                    '{{prometheus_node_exporter_port}}',
                )
                as $cfg
            ) {
                array_push($scrapeConfig['static_configs'], $cfg);
            }
        }

        return json_encode(
            $scrapeConfig,
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        );
    })
    ->set('node_exporter_scrape_config_production', function () {
        $scrapeConfig = [
            'job_name' => 'node-production',
            'static_configs' => [],
        ];

        foreach (['web', 'db'] as $role) {
            foreach (
                getPromScrapeStaticConfigs(
                    ['--selector', "env=production,role.{$role}=ok"],
                    ['role' => $role, 'env' => 'production'],
                    '{{prometheus_node_exporter_port}}',
                )
                as $cfg
            ) {
                array_push($scrapeConfig['static_configs'], $cfg);
            }
        }

        return json_encode(
            $scrapeConfig,
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        );
    })
    ->set('postfix_exporter_scrape_config_static_configs', function () {
        $staticConfigs = [];

        foreach (['staging', 'production'] as $env) {
            foreach (
                getPromScrapeStaticConfigs(
                    ['--selector', "env={$env},role.postfix=ok"],
                    ['role' => 'postfix', 'env' => $env],
                    '{{prometheus_postfix_exporter_port}}',
                )
                as $cfg
            ) {
                array_push($staticConfigs, $cfg);
            }
        }

        return json_encode(
            $staticConfigs,
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        );
    })
    ->set('postgres_exporter_scrape_config_static_configs', function () {
        $staticConfigs = [];

        foreach (['staging', 'production'] as $env) {
            foreach (
                getPromScrapeStaticConfigs(
                    ['--selector', "env={$env},role.db=ok"],
                    ['role' => 'db', 'env' => $env],
                    '{{prometheus_postgres_exporter_port}}',
                )
                as $cfg
            ) {
                array_push($staticConfigs, $cfg);
            }
        }

        return json_encode(
            $staticConfigs,
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        );
    })
    ->set('private_ipv4', function () {
        return getHostPrivateIP('maintenance.toot.cat');
    })
    ->set('prometheus_rules_files', [
        '{{assets_dir}}/prometheus-rules-tootcat.yml' => 'tootcat.yml',
        '{{assets_dir}}/prometheus-rules-postgresql.yml' => 'postgresql.yml',
    ])
    ->set('prometheus_scrape_config_targets', function () {
        return json_encode(
            ['localhost:{{prometheus_port}}'],
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        );
    })
    ->set('remote_user', 'root')
    ->set('stage', 'meta')
    ->set('statsd_exporter_scrape_config_static_configs', function () {
        $staticConfigs = [];

        foreach (['staging', 'production'] as $env) {
            foreach (
                getPromScrapeStaticConfigs(
                    ['--selector', "env={$env},role.web=ok"],
                    ['role' => 'web', 'env' => $env],
                    '{{prometheus_statsd_exporter_port}}',
                )
                as $cfg
            ) {
                array_push($staticConfigs, $cfg);
            }
        }

        return json_encode(
            $staticConfigs,
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        );
    })
    ->set('ufw_rules', [
        'default deny incoming',
        'default allow outgoing',
        'allow 22',
        'allow from {{lb_private_ipv4}} proto tcp to any port {{grafana_http_port}}',
        'allow from {{network_cidr}} proto tcp to any port {{otlp_port}}',
        'allow from {{network_cidr}} proto tcp to any port {{prometheus_port}}',
        'allow from {{network_cidr}} proto tcp to any port {{prometheus_remote_write_port}}',
    ])
    ->set('valkey_exporter_scrape_config_static_configs', function () {
        $staticConfigs = [];

        foreach (['staging', 'production'] as $env) {
            foreach (
                getPromScrapeStaticConfigs(
                    ['--selector', "env={$env},role.valkey=ok"],
                    ['role' => 'valkey', 'env' => $env],
                    '{{prometheus_valkey_exporter_port}}',
                )
                as $cfg
            ) {
                array_push($staticConfigs, $cfg);
            }
        }

        return json_encode(
            $staticConfigs,
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        );
    })
    ->setLabels([
        'role.grafana' => 'ok',
        'role.maintenance' => 'ok',
        'role.nat_client' => 'ok',
        'role.opendkim' => 'ok',
        'role.postfix' => 'ok',
        'role.prometheus' => 'ok',
        'role.otelcol' => 'ok',
        'stage' => 'meta',
    ]);

host('nat.toot.cat')
    ->set('hcloud_metadata', function () {
        return json_decode(
            runLocally('hcloud server describe nat.toot.cat --output json'),
            true,
        );
    })
    ->set('config_file', '{{top}}/tf.sshconfig')
    ->set('deploy_path', '/tmp')
    ->set('domain', 'nat.toot.cat')
    ->set('floating_ips', function () {
        return describeFloatingIPs(
            get('hcloud_metadata')['public_net']['floating_ips'],
        );
    })
    ->set('private_ipv4', function () {
        return getHostPrivateIP('nat.toot.cat');
    })
    ->set('remote_user', 'root')
    ->set('stage', 'meta')
    ->set('ufw_rules', [
        'default deny incoming',
        'default allow outgoing',
        'allow in on enp7s0 from {{maintenance_private_ipv4}}',
        'deny 22/tcp',
    ])
    ->set('web_domain', 'toot.cat')
    ->setLabels([
        'role.nat' => 'ok',
        'stage' => 'meta',
    ]);

host('db.staging.toot.cat')
    ->set('config_file', '{{top}}/tf.sshconfig')
    ->set('db_configuration', [
        'max_replication_slots = 10',
        'max_logical_replication_workers = 4',
        'max_worker_processes = 8',
    ])
    ->set('db_volume_mount', function () {
        $desc = json_decode(
            runLocally(
                'hcloud volume describe tootcat-db-{{stage}} --output json',
            ),
            true,
        );
        return '/mnt/HC_Volume_' . strval($desc['id']);
    })
    ->set('deploy_path', '/tmp')
    ->set('domain', 'db.staging.toot.cat')
    ->set('elasticsearch_security_on', true)
    ->set('elasticsearch_auth_uri', function () {
        return implode('', [
            'https://mastodon:',
            get('stage_env')['ES_PASS'],
            '@{{db_private_ipv4}}:{{elasticsearch_http_port}}',
        ]);
    })
    ->set(
        'elasticsearch_uri',
        'https://{{db_private_ipv4}}:{{elasticsearch_http_port}}',
    )
    ->set('hcloud_metadata', function () {
        return json_decode(
            runLocally(
                'hcloud server describe db.staging.toot.cat --output json',
            ),
            true,
        );
    })
    ->set('private_ipv4', function () {
        return getHostPrivateIP('db.staging.toot.cat');
    })
    ->set('remote_user', 'root')
    ->set('stage', 'staging')
    ->set('swap_multiplier', 1)
    ->set('ufw_rules', [
        'default deny incoming',
        'default allow outgoing',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port 22',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{postgresql_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_elasticsearch_exporter_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_node_exporter_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_postfix_exporter_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_postgres_exporter_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_valkey_exporter_port}}',
        'allow from {{private_subnet_cidr}} proto tcp to any port 25',
        'allow from {{private_subnet_cidr}} proto tcp to any port {{postgresql_port}}',
        'allow from {{private_subnet_cidr}} proto tcp to any port {{valkey_port}}',
        'allow from {{private_subnet_cidr}} proto tcp to any port {{elasticsearch_http_port}}',
        'allow from {{private_subnet_cidr}} proto tcp to any port {{elasticsearch_transport_port}}',
        // NOTE: This allows forwarding via the podman bridge network
        'route allow in on enp7s0 out on cni-podman0 to any port {{valkey_port}}',
    ])
    ->setLabels([
        'role.db' => 'ok',
        'role.elasticsearch' => 'ok',
        'role.nat_client' => 'ok',
        'role.opendkim' => 'ok',
        'role.postfix' => 'ok',
        'role.valkey' => 'ok',
        'stage' => 'staging',
    ]);

host('web0[1:2].staging.toot.cat')
    ->set('bundler_version', '~>2.6')
    ->set('config_file', '{{top}}/tf.sshconfig')
    ->set('domain', 'staging.toot.cat')
    ->set('db_hostname', 'db.staging.toot.cat')
    ->set('hcloud_metadata', function () {
        return json_decode(
            runLocally('hcloud server describe {{hostname}} --output json'),
            true,
        );
    })
    ->set('keep_releases', 5)
    ->set('mastodon_env_append', function () {
        return parse(
            implode(
                "\n",
                array_merge(get('shared_env'), [
                    'ES_CA_FILE=/etc/elasticsearch/certs/http_ca.crt',
                    'ES_ENABLED=true',
                    'ES_HOST=https://{{db_private_ipv4}}',
                    'ES_PRESET=single_node_cluster',
                    'ES_USER=mastodon',
                    'LOCAL_DOMAIN={{domain}}',
                    'OTEL_EXPORTER_OTLP_ENDPOINT=http://{{maintenance_private_ipv4}}:{{otlp_port}}',
                    'SMTP_AUTH_METHOD=none',
                    'SMTP_FROM_ADDRESS=nobody+staging@{{mail_domain}}',
                    'SMTP_OPENSSL_VERIFY_MODE=none',
                    'SMTP_PORT=25',
                    'SMTP_SERVER={{db_private_ipv4}}',
                ]),
            ) . "\n",
        );
    })
    ->set('nginx_site_template', 'nginx-site-no-ssl.conf.tmpl')
    ->set('private_ipv4', function () {
        return getHostPrivateIP(parse('{{hostname}}'));
    })
    ->set('remote_user', 'root')
    ->set('stage', 'staging')
    ->set('swap_multiplier', 2)
    ->set('ufw_rules', [
        'default deny incoming',
        'default allow outgoing',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port 22',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_node_exporter_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_statsd_exporter_port}}',
        'allow from {{lb_private_ipv4}} proto tcp to any port 80',
    ])
    ->set('use_certbot', false)
    ->setLabels([
        'role.nat_client' => 'ok',
        'role.web' => 'ok',
        'stage' => 'staging',
    ]);

host('db.production.toot.cat')
    ->set('config_file', '{{top}}/tf.sshconfig')
    ->set('db_volume_mount', function () {
        $desc = json_decode(
            runLocally(
                'hcloud volume describe tootcat-db-{{stage}} --output json',
            ),
            true,
        );
        return '/mnt/HC_Volume_' . strval($desc['id']);
    })
    ->set('db_configuration', [
        'max_replication_slots = 10',
        'max_logical_replication_workers = 4',
        'max_worker_processes = 8',
    ])
    ->set('deploy_path', '/tmp')
    ->set('domain', 'db.production.toot.cat')
    ->set('elasticsearch_security_on', true)
    ->set('elasticsearch_auth_uri', function () {
        return implode('', [
            'https://mastodon:',
            get('stage_env')['ES_PASS'],
            '@{{db_private_ipv4}}:{{elasticsearch_http_port}}',
        ]);
    })
    ->set(
        'elasticsearch_uri',
        'https://{{db_private_ipv4}}:{{elasticsearch_http_port}}',
    )
    ->set('hcloud_metadata', function () {
        return json_decode(
            runLocally(
                'hcloud server describe db.production.toot.cat --output json',
            ),
            true,
        );
    })
    ->set('private_ipv4', function () {
        return getHostPrivateIP('db.production.toot.cat');
    })
    ->set('remote_user', 'root')
    ->set('stage', 'production')
    ->set('swap_multiplier', 2)
    ->set('ufw_rules', [
        'default deny incoming',
        'default allow outgoing',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port 22',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{postgresql_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_elasticsearch_exporter_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_node_exporter_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_postfix_exporter_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_postgres_exporter_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_valkey_exporter_port}}',
        'allow from {{private_subnet_cidr}} proto tcp to any port 25',
        'allow from {{private_subnet_cidr}} proto tcp to any port {{postgresql_port}}',
        'allow from {{private_subnet_cidr}} proto tcp to any port {{valkey_port}}',
        'allow from {{private_subnet_cidr}} proto tcp to any port {{elasticsearch_http_port}}',
        'allow from {{private_subnet_cidr}} proto tcp to any port {{elasticsearch_transport_port}}',
        // NOTE: This allows forwarding via the podman bridge network
        'route allow in on enp7s0 out on cni-podman0 to any port {{valkey_port}}',
    ])
    ->setLabels([
        'role.db' => 'ok',
        'role.elasticsearch' => 'ok',
        'role.nat_client' => 'ok',
        'role.opendkim' => 'ok',
        'role.postfix' => 'ok',
        'role.valkey' => 'ok',
        'stage' => 'production',
    ]);

host('web0[1:2].production.toot.cat')
    ->set('bundler_version', '~>2.6')
    ->set('config_file', '{{top}}/tf.sshconfig')
    ->set('domain', 'toot.cat')
    ->set('db_hostname', 'db.production.toot.cat')
    ->set('hcloud_metadata', function () {
        return json_decode(
            runLocally('hcloud server describe {{hostname}} --output json'),
            true,
        );
    })
    ->set('keep_releases', 5)
    ->set('mastodon_env_append', function () {
        return parse(
            implode(
                "\n",
                array_merge(get('shared_env'), [
                    'ES_CA_FILE=/etc/elasticsearch/certs/http_ca.crt',
                    'ES_ENABLED=true',
                    'ES_HOST=https://{{db_private_ipv4}}',
                    'ES_PRESET=single_node_cluster',
                    'ES_USER=mastodon',
                    'LOCAL_DOMAIN={{domain}}',
                    'OTEL_EXPORTER_OTLP_ENDPOINT=http://{{maintenance_private_ipv4}}:{{otlp_port}}',
                    'SMTP_AUTH_METHOD=none',
                    'SMTP_FROM_ADDRESS=nobody@{{mail_domain}}',
                    'SMTP_OPENSSL_VERIFY_MODE=none',
                    'SMTP_PORT=25',
                    'SMTP_SERVER={{db_private_ipv4}}',
                ]),
            ) . "\n",
        );
    })
    ->set('nginx_site_template', 'nginx-site-no-ssl.conf.tmpl')
    ->set('private_ipv4', function () {
        return getHostPrivateIP(parse('{{hostname}}'));
    })
    ->set('remote_user', 'root')
    ->set('stage', 'production')
    ->set('swap_multiplier', 2)
    ->set('ufw_rules', [
        'default deny incoming',
        'default allow outgoing',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port 22',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_node_exporter_port}}',
        'allow from {{maintenance_private_ipv4}} proto tcp to any port {{prometheus_statsd_exporter_port}}',
        'allow from {{lb_private_ipv4}} proto tcp to any port 80',
    ])
    ->set('use_certbot', false)
    ->setLabels([
        'role.nat_client' => 'ok',
        'role.web' => 'ok',
        'stage' => 'production',
    ]);

host('cloud1.vbz.ovh', 'cloud5.vbz.ovh')
    ->set('config_file', 'legacy.sshconfig')
    ->set('deploy_path', '/tmp')
    ->setLabels([
        'stage' => 'legacy',
    ]);

function getHostPrivateIP(string $hostname): string {
    foreach (
        host(parse($hostname))->get('hcloud_metadata', [])['private_net'] ?? []
        as $net
    ) {
        if (str_starts_with($net['ip'], '10.')) {
            return $net['ip'];
        }
    }

    return '127.0.0.1';
}

function describeFloatingIPs(array $floatingIPIDs): array {
    $ips = [];

    foreach ($floatingIPIDs as $floatingIPID) {
        $flipStr = strval($floatingIPID);

        $desc = json_decode(
            runLocally("hcloud floating-ip describe {$flipStr} --output json"),
            true,
        );

        $suffix = '';

        if ($desc['type'] == 'ipv4') {
            $suffix = '/32';
        } elseif ($desc['type'] == 'ipv6') {
            $suffix = '/64';
        }

        array_push($ips, str_replace($suffix, '', $desc['ip']) . $suffix);
    }

    sort($ips);
    return $ips;
}

function getSelectServers(array $args): array {
    $matchingServers = [];

    foreach (
        json_decode(
            runLocally(
                implode(
                    ' ',
                    array_merge(['hcloud', 'server', 'list'], $args, [
                        '--output',
                        'json',
                    ]),
                ),
            ),
            true,
        )
        as $srv
    ) {
        array_push($matchingServers, $srv);
    }

    return $matchingServers;
}

function getPromScrapeStaticConfigs(
    array $args,
    array $labels,
    string $port,
): array {
    $scrapeConfigs = [];

    foreach (getSelectServers($args) as $srv) {
        $scrapeConfig = [
            'targets' => [parse($srv['private_net'][0]['ip'] . ':' . $port)],
            'labels' => $labels,
        ];

        if (!array_key_exists('instance', $scrapeConfig['labels'])) {
            $scrapeConfig['labels']['instance'] = $srv['name'];
        }

        array_push($scrapeConfigs, $scrapeConfig);
    }

    return $scrapeConfigs;
}

function getWebServerURLs(string $stage): array {
    $webServers = [];

    foreach (
        getSelectServers(['--selector', "env={$stage},role.web=ok"])
        as $srv
    ) {
        array_push($webServers, [
            'url' => 'http://' . $srv['private_net'][0]['ip'],
        ]);
    }

    return $webServers;
}

desc('Dump some info about the selected host(s?)');
task('infodump', function () {
    info('db_name={{db_name}}');
    info('private_ipv4={{private_ipv4}}');
    info(
        'dep_provisioned="' .
            run('cat /etc/dep.provisioned 2>/dev/null || echo unknown') .
            '"',
    );
});

desc('Write sshconfig for hosts defined in tf and chmod 0600 all of them');
task('tf:sshconfig', function () {
    file_put_contents(
        __DIR__ . '/tf.sshconfig',
        parse(file_get_contents(parse('{{templates_dir}}/tf.sshconfig.tmpl'))),
    );

    foreach (
        [
            'legacy.sshconfig',
            'tf.sshconfig',
            'tf/net/ssh/net.sshconfig',
            'tf/production/ssh/production.sshconfig',
            'tf/staging/ssh/staging.sshconfig',
            'vagrant.sshconfig',
        ]
        as $sshConfigFile
    ) {
        if (file_exists($sshConfigFile)) {
            chmod(__DIR__ . '/' . $sshConfigFile, 0600);
        }
    }
});

desc('Provision swap');
task('provision:swap', function () {
    if (0.0 == floatval(get('swap_multiplier'))) {
        return;
    }

    $totalMemMB = intval(run("free --mega | awk '/^Mem:/ { print \$2 }'"));
    $totalSwapMB = intval($totalMemMB * floatval(get('swap_multiplier')));

    if (test("grep -q '^/var/cache/swap' /etc/fstab")) {
        info('existing swap entry found in /etc/fstab');
        return;
    }

    $count10MB = intval($totalSwapMB / 10);

    run(
        implode(' ', [
            'free -h &&',
            'swapoff -a -v &&',
            "dd if=/dev/zero of=/var/cache/swap bs=10MB count=$count10MB &&",
            'chmod -v 0600 /var/cache/swap &&',
            'mkswap /var/cache/swap &&',
            "printf '/var/cache/swap none swap defaults 0 0\\n' | tee -a /etc/fstab &&",
            'systemctl daemon-reload &&',
            'swapon -a -v &&',
            'free -h',
        ]),
    );
});

desc('Provision Mastodon nginx site');
task('provision:mastodon:nginx', function () {
    if (!has_role('web')) {
        return;
    }

    install(
        '{{templates_dir}}/{{nginx_site_template}}',
        '/etc/nginx/sites-available/mastodon',
        'www-data',
        'www-data',
        '0640',
    );

    if (get('use_local_cert') === true) {
        runLocally('cd /tmp && mkcert {{domain}} localhost 127.0.0.1 ::1');
        runLocally(
            'cat /tmp/{{domain}}+3.pem | tee /tmp/{{domain}}-chain.pem &>/dev/null',
        );
        runLocally(
            'cat "$(mkcert -CAROOT)/rootCA.pem" | tee -a /tmp/{{domain}}-chain.pem &>/dev/null',
        );
        runLocally(
            'cat /tmp/{{domain}}+3.pem | tee /tmp/{{domain}}-fullchain.pem &>/dev/null',
        );
        runLocally(
            'cat /tmp/{{domain}}-chain.pem | tee -a /tmp/{{domain}}-fullchain.pem &>/dev/null',
        );

        run('mkdir -p /etc/letsencrypt/live/{{domain}}');
        upload(
            '/tmp/{{domain}}-fullchain.pem',
            '/etc/letsencrypt/live/{{domain}}/fullchain.pem',
        );
        upload(
            '/tmp/{{domain}}+3-key.pem',
            '/etc/letsencrypt/live/{{domain}}/privkey.pem',
        );
        run('chown -R root:root /etc/letsencrypt/live/{{domain}}');
    }

    run("sed -i '/# server_tokens off;/s/# se/se/' /etc/nginx/nginx.conf");
    run('systemctl reload nginx');
});

task('provision:ssh:private_ipv4', function () {
    if (has_role('maintenance')) {
        return;
    }

    run(
        "printf 'ListenAddress {{private_ipv4}}\\n' | tee /tmp/99-deployer.conf",
    );
    run(
        'install -v -m 0644 /tmp/99-deployer.conf /etc/ssh/sshd_config.d/99-deployer.conf',
    );
    run('rm -vf /tmp/99-deployer.conf');
});

task('provision:mastodon:services', function () {
    if (!has_role('web')) {
        return;
    }

    run('mkdir -p {{deploy_path}}');
    run('touch {{deploy_path}}/.env');

    foreach (
        [
            'mastodon-sidekiq.service',
            'mastodon-streaming.service',
            'mastodon-streaming@.service',
            'mastodon-web.service',
        ]
        as $svc
    ) {
        install(
            '{{templates_dir}}/' . $svc . '.tmpl',
            '/usr/lib/systemd/system/' . $svc,
            'root',
            'root',
            '0644',
        );
    }

    run('systemctl daemon-reload');

    foreach (['php{{php_version}}-fpm.service', 'rsyslog.service'] as $svc) {
        run('systemctl disable --now ' . $svc . ' || true');
    }

    foreach (
        array_filter([
            get('use_certbot') ? 'certbot.timer' : null,
            'fail2ban.service',
            'logrotate.timer',
            'mastodon-sidekiq.service',
            'mastodon-streaming.service',
            'mastodon-streaming@{{mastodon_streaming_port}}.service',
            'mastodon-web.service',
            'ssh.service',
            'ufw.service',
        ])
        as $svc
    ) {
        run('systemctl enable --now ' . $svc);
    }
});

desc('Set the hostname');
task('provision:hostname', function () {
    run('hostnamectl hostname {{hostname}}');
});

desc('Configure elasticsearch for mastodon');
task('provision:mastodon:elasticsearch', function () {
    if (!has_role('elasticsearch')) {
        return;
    }

    install(
        '{{templates_dir}}/elasticsearch.yml.tmpl',
        '/etc/elasticsearch/elasticsearch.yml',
        'root',
        'elasticsearch',
        '0660',
    );

    run('mkdir -p /var/lib/elasticsearch /var/log/elasticsearch');
    run(
        'chown -v elasticsearch:elasticsearch /var/lib/elasticsearch /var/log/elasticsearch',
    );
    run(
        'chmod -v 2750 /var/lib/elasticsearch /var/log/elasticsearch /etc/elasticsearch',
    );

    if (input()->getOption('softly')) {
        run('systemctl start elasticsearch.service');
    } else {
        run('systemctl restart elasticsearch.service');
    }

    $elasticPass = run(
        implode(' ', [
            '/usr/share/elasticsearch/bin/elasticsearch-reset-password',
            '--auto',
            '--batch',
            '--silent',
            '--username',
            'elastic',
        ]),
    );

    $elasticPass = trim($elasticPass);

    run(
        implode(' ', [
            'curl',
            '-f',
            '-s',
            '-k',
            '-X',
            'POST',
            '-u',
            '"elastic:%secret%"',
            '{{elasticsearch_uri}}/_security/role/mastodon_full_access?pretty',
            '-H',
            '"Content-Type: application/json"',
            '-d',
            "'" .
            json_encode(
                [
                    'cluster' => ['monitor'],
                    'indices' => [
                        [
                            'names' => ['*'],
                            'privileges' => [
                                'read',
                                'monitor',
                                'write',
                                'manage',
                            ],
                        ],
                    ],
                ],
                JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
            ) .
            "'",
        ]),
        secret: $elasticPass,
    );

    run(
        implode(' ', [
            'curl',
            '-f',
            '-s',
            '-k',
            '-X',
            'POST',
            '-u',
            '"elastic:%secret%"',
            '{{elasticsearch_uri}}/_security/user/mastodon?pretty',
            '-H',
            '"Content-Type: application/json"',
            '-d',
            "'" .
            json_encode(
                [
                    'password' => get('stage_env')['ES_PASS'],
                    'roles' => ['mastodon_full_access'],
                ],
                JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
            ) .
            "'",
        ]),
        secret: $elasticPass,
    );
})->oncePerNode();

task('provision:mastodon:user', function () {
    if (!has_role('web')) {
        return;
    }

    run('adduser --quiet mastodon || true');
});

after('provision:install', 'provision:hostname');
after('provision:install', 'provision:mastodon:elasticsearch');
after('provision:install', 'provision:mastodon:nginx');
after('provision:install', 'provision:mastodon:services');
after('provision:install', 'provision:mastodon:user');

before('provision:install', 'provision:swap');
before('provision:mastodon:services', 'provision:ssh:private_ipv4');

desc('Write stage env to shared dir');
task('deploy:mastodon:env', function () {
    if (!has_role('web')) {
        return;
    }

    install_contents(
        get('stage_env_raw'),
        '{{deploy_path}}/.env',
        'deployer',
        'deployer',
        '0640',
    );

    run(
        implode(' ', [
            'if ! grep -q "## init mastodon env" ~mastodon/.bashrc; then',
            '  echo -n "${INIT_MASTODON_ENV_BASE64}" | ',
            '    base64 --decode | ',
            '    tee -a ~mastodon/.bashrc &>/dev/null;',
            'fi',
        ]),
        env: [
            'INIT_MASTODON_ENV_BASE64' => base64_encode(
                parse(
                    file_get_contents(
                        parse('{{templates_dir}}/init_mastodon.bash.tmpl'),
                    ),
                ),
            ),
        ],
    );
});

desc('Include git short sha1 via environment variable');
task('deploy:mastodon:version_metadata', function () {
    if (!has_role('web')) {
        return;
    }

    run('touch {{release_path}}/.env');
    run('sed -i \'/^MASTODON_VERSION_METADATA/d\' {{release_path}}/.env');
    $versionMetadata = run('cut -b1-9 <{{release_path}}/REVISION');
    run(
        implode(' ', [
            'printf \'MASTODON_VERSION_METADATA=%s\\n\' "${V}" |',
            'tee -a {{release_path}}/.env &>/dev/null',
        ]),
        env: ['V' => $versionMetadata],
    );
    run('grep MASTODON_VERSION_METADATA {{release_path}}/.env');
});

desc('Ensure mastodon permissions are good');
task('deploy:mastodon:permissions', function () {
    if (!has_role('web')) {
        return;
    }

    run('mkdir -p {{deploy_path}}');
    run('chown -R deployer:deployer {{deploy_path}}');
    run('chmod -R g+r {{deploy_path}}');
    run('find {{deploy_path}} -type d -print0 | xargs -0 chmod g+rx');

    if (test('git config --global safe.directory') === false) {
        run(
            implode(' ', [
                'git',
                'config',
                '--global',
                '--add',
                'safe.directory',
                '{{deploy_path}}/.dep/repo',
            ]),
        );
    }
});

desc('Turn on the mastodon nginx site');
task('deploy:mastodon:site', function () {
    if (!has_role('web')) {
        return;
    }

    run(
        'ln -svf /etc/nginx/sites-available/mastodon /etc/nginx/sites-enabled/mastodon',
    );
});

desc('Stop all mastodon services');
task('deploy:mastodon:stop', function () {
    if (!has_role('web')) {
        return;
    }

    foreach (get('mastodon_services') as $svc) {
        run('systemctl stop ' . $svc);
    }
});

desc('Start all mastodon services');
task('deploy:mastodon:start', function () {
    if (!has_role('web')) {
        return;
    }

    foreach (get('mastodon_services') as $svc) {
        run('systemctl start ' . $svc);
    }
});

desc('Reload all mastodon-related services');
task('deploy:mastodon:reload', function () {
    if (!has_role('web')) {
        return;
    }

    run('systemctl enable --now nginx.service');
    run('systemctl reload nginx.service');

    foreach (
        [
            'mastodon-sidekiq.service',
            'mastodon-streaming.service',
            'mastodon-web.service',
        ]
        as $svc
    ) {
        run("systemctl enable --now {$svc}");

        if (get('rolling')) {
            info(
                "fully restarting <fg=green;options=bold>{$svc}</> due to rolling deploy",
            );

            run("systemctl stop {$svc}");
            run("systemctl start {$svc}");

            continue;
        }

        if (input()->getOption('softly')) {
            run("systemctl start {$svc}");
        } else {
            run("systemctl restart {$svc}");
        }
    }
});

desc('Wait for mastodon availability');
task('deploy:mastodon:wait', function () {
    if (!has_role('web')) {
        return;
    }

    if (input()->getOption('hastily')) {
        info('not waiting for mastodon!');
        return;
    }

    run('systemctl is-active --wait mastodon-web');
    run('wait-for-it 127.0.0.1:{{mastodon_http_port}} -t 120');
});

desc('Run a tootctl command');
task('deploy:mastodon:tootctl', function () {
    if (!has_role('web')) {
        return;
    }

    tootctl('{{tootctl_command}}');
});

function tootctl(string $command) {
    run(
        implode(' ', [
            " NOPROMPT=1 sudo -u mastodon -H bash -lec \"",
            implode(' ', [
                'cd {{current_path}} &&',
                'source ~/.bashrc.rbenv &&',
                'set -o allexport &&',
                'source {{deploy_path}}/.env &&',
                'set +o allexport &&',
                "bundle exec bin/tootctl {$command}",
            ]),
            "\"",
        ]),
    );
}

// NOTE: this is a fully-exploded version of 'deploy' since it seems that invoke() does
// not run hooks :-///
desc('Run a deploy mastodon-style via invoke() up until publish');
task('deploy:mastodon:invokable:prepublish', [
    // deploy:prepare {
    'deploy:info',
    'deploy:mastodon:env',
    'deploy:setup',
    'deploy:lock',
    'deploy:release',
    // --- just a smidge of provisioning {
    'provision:rbenv:cache:pull',
    'provision:rbenv:versions',
    'provision:rbenv:cache:push',
    'provision:bundler',
    // --- }
    'deploy:update_code',
    'deploy:mastodon:version_metadata',
    'deploy:shared',
    'deploy:writable',
    'deploy:mastodon:permissions',
    // }
    'deploy:cache:pull',
    'deploy:vendors',
    'deploy:precompile',
]);

desc('Run a deploy mastodon-style via invoke() from publish to end');
task('deploy:mastodon:invokable:publish', [
    // deploy:publish {
    'deploy:symlink',
    'deploy:unlock',
    'deploy:cleanup',
    'deploy:mastodon:site',
    // -- interlude! {
    'rails:db:migrate',
    // }
    'deploy:mastodon:reload',
    'deploy:mastodon:wait',
    'deploy:success',
    // }
    'deploy:cache:push',
]);

desc('Deploy mastodon in a rolling zero-downtime kinda way');
task('deploy:mastodon:rolling', function () {
    set('rolling', true);

    $webStage = get('stage');
    $webHostname = get('hostname');
    $allWebServerURLs = getWebServerURLs($webStage);

    if (empty($allWebServerURLs)) {
        throw error('missing server URLs for {{stage}}');
    }

    $diffWebServerURLs = array_filter(
        $allWebServerURLs,
        function ($v, $k) {
            return $v['url'] != 'http://' . get('private_ipv4');
        },
        ARRAY_FILTER_USE_BOTH,
    );

    invoke('deploy:mastodon:invokable:prepublish');

    $downtimeStart = time();

    on(select('role.traefik=ok'), function () use (
        $diffWebServerURLs,
        $webStage,
        $webHostname,
    ) {
        info("cordoning <fg=yellow;options=bold>{$webHostname}</>");

        set(
            "{$webStage}_server_urls",
            json_encode(
                $diffWebServerURLs,
                JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
            ),
        );
        set('filename', "{{templates_dir}}/{$webStage}-traefik.yml.tmpl");
        invoke('provision:traefik:sync_one_config');
    });

    invoke('deploy:mastodon:invokable:publish');

    on(select('role.traefik=ok'), function () use (
        $allWebServerURLs,
        $webStage,
        $webHostname,
    ) {
        info("uncordoning <fg=green;options=bold>{$webHostname}</>");

        set(
            "{$webStage}_server_urls",
            json_encode(
                $allWebServerURLs,
                JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
            ),
        );
        set('filename', "{{templates_dir}}/{$webStage}-traefik.yml.tmpl");
        invoke('provision:traefik:sync_one_config');
    });

    $totalDowntime = time() - $downtimeStart;
    info(
        "finished deploying <fg=green;options=bold>{$webHostname}</> " .
            "(downtime={$totalDowntime}s)",
    );
})->limit(1);

use Symfony\Component\HttpClient\HttpClient;
use Masterminds\HTML5;

desc('Smoke test a tootcat-flavored Mastodon Glitch instance');
task('deploy:mastodon:smoke', function () {
    $baseURL = get('smoke_test_base_url');

    $client = HttpClient::create();

    $reqURL = $baseURL . '/health';
    $resp = $client->request('GET', $reqURL);
    $status = $resp->getStatusCode();

    if ($status >= 400) {
        throw error("HTTP GET {$reqURL} status {$status}");
    }

    info("GET <fg=white;options=bold>{$reqURL}</> OK");

    $reqURL = $baseURL . '/getting-started';
    $resp = $client->request('GET', $reqURL);
    $status = $resp->getStatusCode();

    if ($status >= 400) {
        throw error("HTTP GET {$reqURL} status {$status}");
    }

    info("GET <fg=white;options=bold>{$reqURL}</> OK");

    $body = $resp->getContent();
    $h5 = new HTML5();
    $dom = $h5->loadHTML($body);
    $initialStateEl = $dom->getElementById('initial-state');

    $initialState = json_decode($initialStateEl->textContent, true);

    if (
        $initialState['meta']['source_url'] !=
        'https://gitlab.com/tootcat/glitch'
    ) {
        throw error(
            "Unexpected source_url {$initialState['meta']['source_url']}",
        );
    }

    info('    <fg=white;options=bold>source_url</> OK');

    if (preg_match('/glitch/', $initialState['meta']['version']) == false) {
        throw error("Unexpected version {$initialState['meta']['version']}");
    }

    info('    <fg=white;options=bold>version</> OK');

    # TODO: more rigorous tests(?)
});

task('logs:app', function () {
    run('journalctl -f -u {{logs_systemd_unit}}');
});

after('deploy:info', 'deploy:mastodon:env');
after('deploy:update_code', 'deploy:mastodon:version_metadata');
after('deploy:writable', 'deploy:mastodon:permissions');
after('deploy:cleanup', 'deploy:mastodon:site');
after('deploy:cleanup', 'deploy:mastodon:reload');
after('deploy:mastodon:reload', 'deploy:mastodon:wait');
after('deploy:failed', 'deploy:unlock');
