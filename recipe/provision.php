<?php declare(strict_types=1);
namespace Deployer;

use Symfony\Component\Console\Input\InputOption;

add('recipes', ['provision']);

option(
    'softly',
    null,
    InputOption::VALUE_NONE,
    'Provision softly, minimizing disruption',
);

import('contrib/yarn.php');
import('recipe/common.php');

require_once __DIR__ . '/provision/base.php';
require_once __DIR__ . '/provision/bundler.php';
require_once __DIR__ . '/provision/deployer.php';
require_once __DIR__ . '/provision/elasticsearch.php';
require_once __DIR__ . '/provision/store.php';
require_once __DIR__ . '/provision/firewall.php';
require_once __DIR__ . '/provision/floating_ip.php';
require_once __DIR__ . '/provision/grafana.php';
require_once __DIR__ . '/provision/hcloud_volumes.php';
require_once __DIR__ . '/provision/local.php';
require_once __DIR__ . '/provision/nat.php';
require_once __DIR__ . '/provision/opendkim.php';
require_once __DIR__ . '/provision/otelcol.php';
require_once __DIR__ . '/provision/postfix.php';
require_once __DIR__ . '/provision/postgresql.php';
require_once __DIR__ . '/provision/prometheus.php';
require_once __DIR__ . '/provision/rbenv.php';
require_once __DIR__ . '/provision/traefik.php';
require_once __DIR__ . '/provision/valkey.php';
require_once __DIR__ . '/provision/yarn.php';

desc('Provision the server');
task('provision', [
    'provision:configure',
    'provision:update',
    'provision:upgrade',
    'provision:install',
    'provision:ssh',
    'provision:firewall',
    'provision:deployer',
    'provision:yarn',
    'provision:php:maybe',
    'provision:rbenv',
    'provision:bundler',
    'provision:valkey',
    'provision:postgresql',
    'provision:record',
]);

desc('Wait for SSH availability');
task('provision:wait_ssh', function () {
    $nowTime = time();
    $elapsedTime = 0;
    $maxTime = intval(get('max_time', '300'));
    $sleepSeconds = intval(get('sleep_seconds', '10'));

    while ($elapsedTime < $maxTime) {
        try {
            run('curl -fsSL http://archive.ubuntu.com');
            return;
        } catch (\Throwable $exc) {
            info(
                "sleeping {$sleepSeconds}s until retry (elapsed={$elapsedTime})",
            );
            sleep($sleepSeconds);
        }

        $elapsedTime = time() - $nowTime;
    }

    throw error('failed to connect to SSH');
});

desc('Record the provisioning');
task('provision:record', function () {
    run('date -u | tee /etc/dep.provisioned');
});
