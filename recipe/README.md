# recipe(s)

This directory tree is kinda sorta meant to look and feel like the same-named directory in
the [Deployer source tree](https://github.com/deployphp/deployer/tree/master/recipe).

## Rails

[`rails.php`](./rails.php) is the primary recipe used by the top-level
[`deploy.php`](../deploy.php). It "takes over" the high-level `dep provision` and `dep
deploy` tasks, reusing some tasks defined via the [Deployer "Common
Recipe"](https://deployer.org/docs/7.x/recipe/common) or redefining those tasks that
require key overrides.

## Hetzlet

[`hetzlet.php`](./hetzlet.php) is something of an abuse of Deployer for the purpose of
defining tasks that are primarily useful for managing semi-ephemeral staging environments.
It knows how to interact with the Hetzner Cloud API via the
[hcloud](https://github.com/hetznercloud/cli?tab=readme-ov-file#installation) tool, and
(confusingly!?) assumes DNS records are managed in Digital Ocean. YMMV :warning:
