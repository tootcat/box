<?php declare(strict_types=1);
namespace Deployer;

desc('Installs packages for lb role');
task('provision:install:lb', function () {
    if (!has_role('lb')) {
        return;
    }

    $packages = [
        'acl',
        'apt-transport-https',
        'curl',
        'debian-archive-keyring',
        'debian-keyring',
        'fail2ban',
        'file',
        'jq',
        'pkg-config',
        'python-is-python3',
        'rsync',
        'sshpass',
        'ufw',
        'unzip',
        'uuid-runtime',
        'whois',
        'zlib1g-dev',
    ];

    run(
        'apt-get install -y ' . implode(' ', $packages),
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
        timeout: 900,
    );
})->oncePerNode();

after('provision:install', 'provision:install:lb');
