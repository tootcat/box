<?php declare(strict_types=1);
namespace Deployer;

desc('Installs packages for maintenance role');
task('provision:install:maintenance', function () {
    if (!has_role('maintenance')) {
        return;
    }

    $packages = [
        'acl',
        'apt-transport-https',
        'curl',
        'debian-archive-keyring',
        'debian-keyring',
        'fail2ban',
        'file',
        'jq',
        'pkg-config',
        'python-is-python3',
        'python3-yaml',
        'rsync',
        'sshpass',
        'ufw',
        'unzip',
        'uuid-runtime',
        'whois',
        'zlib1g-dev',
    ];

    run(
        'apt-get install -y ' . implode(' ', $packages),
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
        timeout: 900,
    );
})->oncePerNode();

desc('Ensure maintenance services are up');
task('provision:maintenance:services', function () {
    if (!has_role('maintenance')) {
        return;
    }

    run('systemctl enable --now fail2ban');
    run('fail2ban-client reload --all || true');
});

after('provision:install', 'provision:install:maintenance');
before('provision:record', 'provision:maintenance:services');
