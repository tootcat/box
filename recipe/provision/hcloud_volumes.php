<?php declare(strict_types=1);
namespace Deployer;

desc('Set up all hcloud volumes');
task('provision:hcloud_volumes', function () {
    $lsblk = json_decode(run('lsblk --json --output-all'), true);
    $fsTabMounts = json_decode(run('findmnt --fstab --json'), true);
    $fsTabChanged = false;

    foreach ($lsblk['blockdevices'] as $blockDevice) {
        if (!str_starts_with($blockDevice['vendor'], 'HC ')) {
            continue;
        }

        if ($blockDevice['model'] != 'Volume') {
            continue;
        }

        $expectedMount = "/mnt/HC_Volume_{$blockDevice['serial']}";
        $fsType = $blockDevice['fstype'];

        if (
            empty(
                array_filter(
                    $fsTabMounts['filesystems'],
                    fn($entry) => $entry['target'] == $expectedMount &&
                        $entry['fstype'] == $fsType,
                )
            )
        ) {
            run(
                'echo "${ENTRY_B64}" | base64 --decode | tee -a /etc/fstab',
                env: [
                    'ENTRY_B64' => base64_encode(
                        "\n{$blockDevice['path']} {$expectedMount} {$fsType} discard,nofail,defaults 0 0\n",
                    ),
                ],
            );

            $fsTabChanged = true;
        }

        if (!empty($blockDevice['mountpoint'])) {
            continue;
        }

        run("mkdir -p {$expectedMount}");
        run("mount {$expectedMount}");
    }

    if ($fsTabChanged) {
        run('systemctl daemon-reload');
    }
});

before('provision:install', 'provision:hcloud_volumes');
