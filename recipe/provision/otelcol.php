<?php declare(strict_types=1);
namespace Deployer;

set(
    'otelcol_download_url',
    'https://github.com/open-telemetry/opentelemetry-collector-releases/releases' .
        '/download/v{{otelcol_version}}/otelcol_{{otelcol_version}}_linux_amd64.deb',
);
set('otelcol_version', '0.106.0');

desc('Installs the OpenTelemetry Collector');
task('provision:otelcol', function () {
    if (!has_role('otelcol')) {
        return;
    }

    run(
        'apt-get install -y curl gdebi-core',
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
        timeout: 900,
    );

    $curVersion = trim(run('otelcol --version || echo not installed'));

    if ($curVersion != parse('otelcol version {{otelcol_version}}')) {
        info(
            "current otelcol version <fg=yellow;options=bold>{$curVersion}</> " .
                'does not match desired <fg=green;options=bold>{{otelcol_version}}</>' .
                '; installing...',
        );
        run('curl -fsSL -o /tmp/otelcol.deb {{otelcol_download_url}}');
        run(
            'gdebi -n /tmp/otelcol.deb',
            env: ['DEBIAN_FRONTEND' => 'noninteractive'],
            timeout: 900,
        );
        run('systemctl enable --now otelcol');
    } else {
        info(
            'otelcol <fg=green;options=bold>{{otelcol_version}}</> already installed',
        );
    }

    install(
        '{{templates_dir}}/otelcol-config.yaml.tmpl',
        '/etc/otelcol/config.yaml',
        'root',
        'root',
        '0644',
    );

    if (input()->getOption('softly')) {
        run('systemctl start otelcol');
    } else {
        run('systemctl restart otelcol');
    }
});

after('provision:install', 'provision:otelcol');
