<?php declare(strict_types=1);
namespace Deployer;

set('storage_box_env', '/etc/default/storage-box');

set('store_client_ssh_id', '');
set('store_client_ssh_id_pub', '');
set('store_client_env', '/etc/default/store');
set('store_dest_host', '');
set('store_dest_prefix', '/dev/null');
set('store_gid', '2269');
set('store_home', '/home/{{store_user}}');
set('store_root_dir', '/var/cache/dep');
set('store_ssh_authorized_keys', '');
set('store_ssh_id', '');
set('store_ssh_id_pub', '');
set('store_uid', '2269');
set('store_user', 'store');
set('store_volume_mount', null);

desc('Sets up store server');
task('provision:store', function () {
    if (!has_role('store')) {
        return;
    }

    if (empty(get('store_volume_mount'))) {
        return;
    }

    run(
        implode(' ', [
            'getent group {{store_user}} ||',
            'groupadd',
            '--gid {{store_gid}}',
            '{{store_user}}',
        ]),
    );

    run(
        implode(' ', [
            'getent passwd {{store_user}} ||',
            'useradd',
            '--uid {{store_uid}}',
            '--gid {{store_gid}}',
            '--home-dir {{store_home}}',
            '--no-create-home',
            '--shell /usr/bin/bash',
            '{{store_user}}',
        ]),
    );

    run(
        implode(' ', [
            'install -d -m 0700',
            '-o {{store_user}}',
            '-g {{store_user}}',
            '{{store_home}}/.ssh',
        ]),
    );

    install_contents(
        get('store_ssh_id'),
        '{{store_home}}/.ssh/id_ed25519',
        '{{store_user}}',
        '{{store_user}}',
        '0600',
    );

    install_contents(
        get('store_ssh_id_pub'),
        '{{store_home}}/.ssh/id_ed25519.pub',
        '{{store_user}}',
        '{{store_user}}',
        '0600',
    );

    install_contents(
        get('store_ssh_authorized_keys'),
        '{{store_home}}/.ssh/authorized_keys',
        '{{store_user}}',
        '{{store_user}}',
        '0600',
    );

    run(
        join(' ', [
            'if ! test -h {{store_home}}/store; then',
            'ln -svf {{store_root_dir}} {{store_home}}/store;',
            'fi',
        ]),
    );

    run(
        'install -d -o {{store_user}} -g {{store_user}} -m 0755 {{store_root_dir}}',
    );

    run('ls -la {{store_volume_mount}}');
    run('mkdir -p {{store_volume_mount}}/dep');

    if (!input()->getOption('softly')) {
        run(
            'chown -R {{store_user}}:{{store_user}} {{store_volume_mount}}/dep',
        );
    }

    run(
        implode(' ', [
            "if ! grep -q '^{{store_volume_mount}}/dep {{store_root_dir}}' /etc/fstab; then",
            implode(' ', [
                "printf '{{store_volume_mount}}/dep {{store_root_dir}} none defaults,bind 0 0\\n' |",
                'tee -a /etc/fstab;',
            ]),
            'fi',
        ]),
    );

    install(
        '{{templates_dir}}/store-rdfind.service.tmpl',
        '/etc/systemd/system/store-rdfind.service',
        'root',
        'root',
        '0644',
    );

    install(
        '{{assets_dir}}/store-rdfind.timer',
        '/etc/systemd/system/store-rdfind.timer',
        'root',
        'root',
        '0644',
    );

    run('systemctl daemon-reload');
    run('systemctl enable --now store-rdfind.timer');

    $lsblk = json_decode(run('lsblk --fs --paths --json'), true);

    foreach ($lsblk['blockdevices'] as $blockDevice) {
        if (in_array(get('store_volume_mount'), $blockDevice['mountpoints'])) {
            if (in_array(get('store_root_dir'), $blockDevice['mountpoints'])) {
                info('{{store_root_dir}} is already bind-mounted');

                return;
            }
        }
    }

    run('mount {{store_root_dir}}');
});

desc('Sets up storage box bits');
task('provision:store:storage_box', function () {
    if (!has_role('store')) {
        return;
    }

    $allEnv = get('all_env');

    install_contents(
        join("\n", [
            'STORAGE_BOX_HOST="' . $allEnv['STORAGE_BOX_HOST'] . '"',
            'STORAGE_BOX_PASS="' . $allEnv['STORAGE_BOX_PASS'] . '"',
            'STORAGE_BOX_PORT="' . $allEnv['STORAGE_BOX_PORT'] . '"',
            'STORAGE_BOX_PREFIX="tootcat/boxes"',
            'STORAGE_BOX_USER="' . $allEnv['STORAGE_BOX_USER'] . '"',
            'SSHPASS="' . $allEnv['STORAGE_BOX_PASS'] . '"',
        ]) . "\n",
        '{{storage_box_env}}',
        'root',
        '{{store_user}}',
        '0640',
    );

    install(
        '{{templates_dir}}/storage-box-backup.service.tmpl',
        '/etc/systemd/system/storage-box-backup.service',
        'root',
        'root',
        '0644',
    );

    install(
        '{{assets_dir}}/storage-box-backup.timer',
        '/etc/systemd/system/storage-box-backup.timer',
        'root',
        'root',
        '0644',
    );

    run('systemctl daemon-reload');
    run('systemctl enable --now storage-box-backup.timer');
});

desc('Installs packages for store server');
task('provision:store:install', function () {
    if (!has_role('store')) {
        return;
    }

    run(
        'apt-get install -y rdfind',
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
        timeout: 900,
    );
})->oncePerNode();

desc('Set up store client bits');
task('provision:store:client', function () {
    store_client_init('root', 'root', '/root/.ssh');
});

before('provision:install', 'provision:store:client');
before('provision:store', 'provision:store:install');
after('provision:install', 'provision:store');
after('provision:store', 'provision:store:storage_box');
