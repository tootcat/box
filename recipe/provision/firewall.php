<?php declare(strict_types=1);
namespace Deployer;

set('ufw_rules', [
    'default deny incoming',
    'default allow outgoing',
    'allow 22',
    'allow 80',
    'allow 443',
]);

desc('Setups a firewall');
task('provision:firewall', function () {
    run('ufw --force disable');
    run('ufw --force reset');

    foreach (get('ufw_rules', []) as $rule) {
        run("ufw {$rule}");
    }

    run('ufw --force enable');
})->oncePerNode();
