<?php declare(strict_types=1);
namespace Deployer;

desc('Adds repositories and update for grafana');
task('provision:grafana:update', function () {
    if (!has_role('grafana')) {
        return;
    }

    $grafanaKeyring = '/usr/share/keyrings/grafana.asc';

    run(
        implode(' ', [
            'curl',
            '-fsSL',
            '-o',
            $grafanaKeyring,
            'https://packages.grafana.com/gpg.key',
        ]),
    );

    $aptEntry = implode(' ', [
        'deb',
        "[signed-by={$grafanaKeyring}]",
        'https://packages.grafana.com/oss/deb',
        'stable',
        'main',
    ]);
    run("echo '{$aptEntry}' | tee /etc/apt/sources.list.d/grafana.list");
});

desc('Installs packages for grafana');
task('provision:grafana:install', function () {
    if (!has_role('grafana')) {
        return;
    }

    run(
        'apt-get install -y grafana',
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
        timeout: 900,
    );

    run('systemctl enable --now grafana-server');
});

desc('Configure grafana');
task('provision:grafana:configure', function () {
    if (!has_role('grafana')) {
        return;
    }

    install(
        '{{templates_dir}}/grafana.ini.tmpl',
        '/etc/grafana/grafana.ini',
        'root',
        'grafana',
        '0640',
    );

    run('mkdir -p /etc/grafana/provisioning/datasources');
    upload(
        '{{assets_dir}}/grafana-datasources/',
        '/etc/grafana/provisioning/datasources/',
    );

    if (input()->getOption('softly')) {
        run('systemctl start grafana-server');
    } else {
        run('systemctl restart grafana-server');
    }
});

before('provision:update', 'provision:grafana:update');
after('provision:install', 'provision:grafana:install');
after('provision:grafana:install', 'provision:grafana:configure');
