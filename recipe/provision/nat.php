<?php declare(strict_types=1);
namespace Deployer;

desc('Provision the NAT server');
task('provision:nat', function () {
    if (!has_role('nat')) {
        return;
    }

    run('apt-get update -yq', env: ['DEBIAN_FRONTEND' => 'noninteractive']);
    run(
        implode(' ', [
            'apt-get install -yq',
            'bind9-dnsutils',
            'bind9-host',
            'conntrack',
            'conntrackd',
            'ethtool',
            'iproute2',
            'iptables',
            'iptables-persistent',
            'iputils-ping',
            'iputils-tracepath',
            'lsof',
            'mtr-tiny',
            'net-tools',
            'netcat-openbsd',
            'networkd-dispatcher',
            'nftables',
            'ngrep',
            'socat',
            'tcpdump',
            'telnet',
            'traceroute',
        ]),
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
    );

    run("sed -i '/^#net.ipv4.ip_forward=1/s/^#//' /etc/sysctl.conf");
    run(
        "sed -i '" .
            implode('', [
                '/DEFAULT_FORWARD_POLICY=/',
                "s/DEFAULT_FORWARD_POLICY=.+/DEFAULT_FORWARD_POLICY=\"ACCEPT\"/",
            ]) .
            "' /etc/default/ufw",
    );

    if (input()->getOption('softly')) {
        run('systemctl start ufw.service');
    } else {
        run('systemctl restart ufw.service');
    }
});

desc('Send internet traffic through NAT server');
task('provision:nat:client', function () {
    if (!has_role('nat_client')) {
        return;
    }
    run('apt-get update -yq', env: ['DEBIAN_FRONTEND' => 'noninteractive']);
    run(
        implode(' ', [
            'apt-get install -yq',
            'bind9-dnsutils',
            'ethtool',
            'iproute2',
            'lsof',
            'mtr-tiny',
            'ngrep',
            'tcpdump',
        ]),
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
    );
});

after('provision:install', 'provision:nat');
after('provision:install', 'provision:nat:client');
