<?php declare(strict_types=1);
namespace Deployer;

set('db_configuration', []);
set('db_name', '');
set('db_password', '');
set('db_volume_mount', null);
set('db_backup_n_keepers', '3');
set('db_backup_timestamp', null);
set('db_replication_local_port', '35432');
set('db_replication_remote_port', '5432');
set('db_replication_remote', 'nobody@localhost');
set('db_replication_role', null);
set('db_replication_pass', null);
set('db_replication_name', '');
set('db_replication_subscription', '');
set('db_replication_publication', '');
set('db_replication_tools_dir', '/var/lib/postgresql/.local/share/deployer');
set('db_restore_clean', null);
set('grafana_db_password', '');
set('postgresql_version', '16');
set('private_ipv4', '127.0.0.1');
set('private_subnet_cidr', '127.0.0.1/32');

desc('Adds repositories and update for postgresql');
task('provision:postgresql:update', function () {
    if (!has_role('db')) {
        return;
    }

    $pgdgKeyring = '/usr/share/keyrings/apt.postgresql.org.asc';

    run(
        implode(' ', [
            'curl',
            '-fsSL',
            '-o',
            $pgdgKeyring,
            'https://www.postgresql.org/media/keys/ACCC4CF8.asc',
        ]),
    );

    $aptEntry = implode(' ', [
        'deb',
        "[signed-by={$pgdgKeyring}]",
        'https://apt.postgresql.org/pub/repos/apt',
        '{{lsb_release}}-pgdg',
        'main',
    ]);
    run("echo '{$aptEntry}' | tee /etc/apt/sources.list.d/pgdg.list");
});

desc('Installs packages for postgresql');
task('provision:postgresql:install', function () {
    if (!has_role('db')) {
        return;
    }

    run(
        'apt-get install -y postgresql-{{postgresql_version}}',
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
        timeout: 900,
    );

    run(
        'apt-get install -y --no-install-suggests php-cli',
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
        timeout: 900,
    );
})->oncePerNode();

desc('Provision the postgresql database');
task('provision:postgresql', function () {
    if (!has_role('db')) {
        return;
    }

    $destPath = '/tmp/deployer-rails-provision-postgresql.psql';

    install(
        '{{templates_dir}}/provision-postgresql.psql.tmpl',
        $destPath,
        'postgres',
        'postgres',
        '0644',
    );

    run(" NOPROMPT=1 sudo -u postgres psql -v ON_ERROR_STOP=1 <{$destPath}");
    run("rm -vf {$destPath}");

    install_contents(
        parse(
            implode(
                "\n",
                array_merge(
                    ["listen_addresses = '{{private_ipv4}}'"],
                    get('db_configuration'),
                ),
            ) . "\n",
        ),
        '/etc/postgresql/{{postgresql_version}}/main/conf.d/99-deployer.conf',
        'postgres',
        'postgres',
        '0644',
    );

    $hbaConf = '/etc/postgresql/{{postgresql_version}}/main/pg_hba.conf';
    $hbaEntry =
        'host {{db_name}} {{db_user}} {{private_subnet_cidr}} scram-sha-256';

    run(
        implode(' ', [
            "if ! grep -q '^host {{db_name}} {{db_user}}' {$hbaConf}; then",
            implode(' ', [
                "printf '%s\\n' \"\${HBA_ENTRY}\" | tee -a \${HBA_CONF};",
            ]),
            'fi',
        ]),
        env: [
            'HBA_ENTRY' => parse($hbaEntry),
            'HBA_CONF' => parse($hbaConf),
        ],
    );

    $grafanaHbaEntry =
        'host {{db_name}} grafana {{maintenance_private_ipv4}}/32 scram-sha-256';

    run(
        implode(' ', [
            "if ! grep -q '^host {{db_name}} grafana' {$hbaConf}; then",
            implode(' ', [
                "printf '%s\\n' \"\${HBA_ENTRY}\" | tee -a \${HBA_CONF};",
            ]),
            'fi',
        ]),
        env: [
            'HBA_ENTRY' => parse($grafanaHbaEntry),
            'HBA_CONF' => parse($hbaConf),
        ],
    );
});

desc('Set up postgres user store access');
task('provision:postgresql:store_access', function () {
    if (!has_role('db')) {
        return;
    }

    store_client_init('postgres', 'postgres', '/var/lib/postgresql/.ssh');
});

desc('Set up postgresql backup');
task('provision:postgresql:backup', function () {
    if (!has_role('db')) {
        return;
    }

    if (input()->getOption('softly')) {
        run('systemctl start postgresql@{{postgresql_version}}-main');
    } else {
        run('systemctl restart postgresql@{{postgresql_version}}-main');
    }

    install(
        '{{assets_dir}}/dep-postgresql-backup.php',
        '/usr/local/bin/dep-postgresql-backup.php',
        'root',
        'root',
        '0755',
    );

    install(
        '{{templates_dir}}/postgresql-backup.service.tmpl',
        '/etc/systemd/system/postgresql-backup.service',
        'root',
        'root',
        '0644',
    );

    install(
        '{{assets_dir}}/postgresql-backup.timer',
        '/etc/systemd/system/postgresql-backup.timer',
        'root',
        'root',
        '0644',
    );

    run('systemctl daemon-reload');
    run('systemctl enable --now postgresql-backup.timer');
})->oncePerNode();

desc('Add postgresql restore script');
task('provision:postgresql:restore_script', function () {
    if (!has_role('db')) {
        return;
    }

    install(
        '{{assets_dir}}/dep-postgresql-restore.php',
        '/usr/local/bin/dep-postgresql-restore.php',
        'root',
        'root',
        '0755',
    );
})->oncePerNode();

desc('Restore postgresql from storage');
task('provision:postgresql:restore', function () {
    if (!has_role('db')) {
        return;
    }

    run(
        implode(' ', [
            ' NOPROMPT=1 sudo -u postgres bash -ec',
            '"',
            implode(';', [
                'set -o allexport',
                'source /etc/default/store',
                'dep-postgresql-restore.php -d {{db_name}}' .
                (empty(get('db_backup_timestamp'))
                    ? ''
                    : ' -b {{db_backup_timestamp}}') .
                (empty(get('db_restore_clean')) ? '' : ' -C'),
            ]),
            '"',
        ]),
    );
});

task('provision:postgresql:mount', function () {
    if (!has_role('db')) {
        return;
    }

    if (empty(get('db_volume_mount'))) {
        return;
    }

    run('ls -la {{db_volume_mount}}');
    run("mkdir -p '{{db_volume_mount}}/postgresql'");
    run("chown -R postgres:postgres '{{db_volume_mount}}/postgresql'");

    $lsblk = json_decode(run('lsblk --fs --paths --json'), true);

    run(
        implode(' ', [
            "if ! grep -q '^{{db_volume_mount}}/postgresql /var/lib/postgresql' /etc/fstab; then",
            implode(' ', [
                "printf '{{db_volume_mount}}/postgresql /var/lib/postgresql none defaults,bind 0 0\\n' |",
                'tee -a /etc/fstab;',
            ]),
            'fi',
        ]),
    );

    run('systemctl daemon-reload');

    foreach ($lsblk['blockdevices'] as $blockDevice) {
        if (in_array(get('db_volume_mount'), $blockDevice['mountpoints'])) {
            if (in_array('/var/lib/postgresql', $blockDevice['mountpoints'])) {
                info('/var/lib/postgresql is already bind-mounted');
                return;
            }
        }
    }

    run('systemctl stop postgresql@{{postgresql_version}}-main');

    run(
        implode(' ', [
            "if ! test -d '{{db_volume_mount}}/postgresql/{{postgresql_version}}/main'; then",
            implode(' ', [
                "rsync -avP /var/lib/postgresql/ '{{db_volume_mount}}/postgresql/';",
            ]),
            'fi',
        ]),
    );

    run('mount /var/lib/postgresql');
    run('chown -R postgres:postgres /var/lib/postgresql');

    run('systemctl start postgresql@{{postgresql_version}}-main');
})->hidden();

desc('Set up an SSH tunnel for PostgreSQL replication');
task('provision:postgresql:replication_tunnel', function () {
    if (!(has_role('db') && has_role('db_replica'))) {
        return;
    }

    $idEd25519 = get('stage_env')['POSTGRES_SSH_ID_ED25519'] ?? '';

    if (empty($idEd25519)) {
        throw error('missing POSTGRES_SSH_ID_ED25519');
    }

    $idEd25519Dest = '/var/lib/postgresql/.ssh/id_ed25519';
    $tmpDest = '/tmp/id_ed25519';

    run("install -d -v -m 0700 -o postgres \$(dirname {$idEd25519Dest})");
    run(
        implode(' ', [
            'set -o errexit;',
            'set -o pipefail;',
            "trap 'rm -f {$tmpDest}' EXIT QUIT TERM;",
            'printenv POSTGRES_SSH_ID_ED25519 |',
            'base64 --decode |',
            "tee {$tmpDest} >/dev/null;",
            "install -v -m 0600 -o postgres {$tmpDest} {$idEd25519Dest};",
        ]),
        env: ['POSTGRES_SSH_ID_ED25519' => $idEd25519],
    );

    install(
        '{{templates_dir}}/postgresql-replication-tunnel.service.tmpl',
        '/etc/systemd/system/postgresql-replication-tunnel.service',
        'root',
        'root',
        '0644',
    );

    run('systemctl daemon-reload');
    run('systemctl enable --now postgresql-replication-tunnel.service');
});

desc('Add tools for postgresql replication');
task('provision:postgresql:replication_tools', function () {
    if (!(has_role('db') && has_role('db_replica'))) {
        return;
    }

    run('mkdir -p {{db_replication_tools_dir}}');

    foreach (
        [
            'postgresql_generate_sequence_sync.pgsql',
            'postgresql_publisher_lag_bytes.pgsql',
        ]
        as $f
    ) {
        install(
            "{{assets_dir}}/{$f}",
            "{{db_replication_tools_dir}}/{$f}",
            'postgres',
            'postgres',
            '0644',
        );
    }

    run(
        'chown -R postgres:postgres ' .
            "\$(dirname \$(dirname {{db_replication_tools_dir}}))",
    );
});

desc('Sync sequences values from replication source to local');
task('provision:postgresql:replication_sync_sequences', function () {
    if (!(has_role('db') && has_role('db_replica'))) {
        return;
    }

    if (empty(get('db_replication_name'))) {
        throw error('missing db_replication_name');
    }

    $genSeqPgsql =
        '{{db_replication_tools_dir}}' .
        '/postgresql_generate_sequence_sync.pgsql';

    run(
        implode(' ', [
            "NOPROMPT=1 sudo -u postgres -H bash -c '",
            implode(' ', [
                'set -o errexit;',
                'set -o pipefail;',
                'ssh {{db_replication_remote}}',
                "psql -d {{db_replication_name}} -XAtq < {$genSeqPgsql} |",
                'psql -d {{db_name}} -v ON_ERROR_STOP -1',
            ]),
            "'",
        ]),
    );
});

desc("Control replication (-o verb='create|start|stop|drop')");
task('provision:postgresql:replication:ctl', function () {
    if (!(has_role('db') && has_role('db_replica'))) {
        return;
    }

    $verb = get('verb');
    if (empty($verb)) {
        throw error('missing verb');
    }

    if (empty(get('db_replication_publication'))) {
        throw error('missing db_replication_publication');
    }

    if (empty(get('db_replication_subscription'))) {
        throw error('missing db_replication_subscription');
    }

    $tmpDest = "/tmp/postgresql_replication_{$verb}.pgsql";
    install(
        "{{templates_dir}}/postgresql_replication_{$verb}.pgsql.tmpl",
        $tmpDest,
        'root',
        'root',
        '0640',
    );

    run(
        "NOPROMPT=1 sudo -u postgres -H psql -d {{db_name}} -v ON_ERROR_STOP=1 <{$tmpDest}",
    );

    run("rm -f {$tmpDest}");
});

desc('Show replication lag in kilobytes (-o once=1 to not loop)');
task('provision:postgresql:replication:lag_kb', function () {
    if (!(has_role('db') && has_role('db_replica'))) {
        return;
    }

    $lagQuery =
        '{{db_replication_tools_dir}}/postgresql_publisher_lag_bytes.pgsql';

    doquery:
    $res = run(
        'NOPROMPT=1 sudo -u postgres -H ssh {{db_replication_remote}} ' .
            "psql -d {{db_replication_name}} -tA <{$lagQuery}",
    );

    $resParts = explode('|', $res);
    $lagBytes = intval($resParts[count($resParts) - 1]);
    $color = 'white';

    if ($lagBytes == 0) {
        $color = 'green';
    } elseif ($lagBytes > 1000 * 1000 * 1000) {
        $color = 'red';
    } elseif ($lagBytes > 1000 * 1000) {
        $color = 'yellow';
    }

    $lagKB = $lagBytes / 1000.0;

    info("lag: <fg={$color};options=bold>{$lagKB}</> KB");

    if (empty(get('once'))) {
        usleep(intval(floatval(get('lag_sleep_seconds', '1')) * 1000000));
        goto doquery;
    }
});

define('MIN_REPLICATION_LAG_SECONDS', 15);

desc('Wait until replication lag is consistently zero');
task('provision:postgresql:replication:lag_kb_wait', function () {
    if (!(has_role('db') && has_role('db_replica'))) {
        return;
    }

    $lagQuery =
        '{{db_replication_tools_dir}}/postgresql_publisher_lag_bytes.pgsql';

    $sampleStart = time();
    $timeoutStart = time();
    $lagWaitTimeout = intval(get('lag_wait_timeout', 300));

    doquery:
    $res = run(
        'NOPROMPT=1 sudo -u postgres -H ssh {{db_replication_remote}} ' .
            "psql -d {{db_replication_name}} -tA <{$lagQuery}",
    );

    $resParts = explode('|', $res);
    $lagBytes = intval($resParts[count($resParts) - 1]);

    $color = 'white';

    if ($lagBytes == 0) {
        $color = 'green';
    } elseif ($lagBytes > 1000 * 1000 * 1000) {
        $color = 'red';
    } elseif ($lagBytes > 1000 * 1000) {
        $color = 'yellow';
    }

    $lagKB = $lagBytes / 1000.0;

    info("lag: <fg={$color};options=bold>{$lagKB}</> KB");

    if ($lagBytes != 0) {
        info('lag: resetting');
        $sampleStart = time();
    }

    $waited = time() - $sampleStart;
    $waitRemaining = MIN_REPLICATION_LAG_SECONDS - $waited;

    if ($waited < MIN_REPLICATION_LAG_SECONDS) {
        if (time() - $timeoutStart > $lagWaitTimeout) {
            throw error('lag: timeout!');
        }

        info("lag: waiting... ({$waitRemaining}s remaining)");

        usleep(intval(floatval(get('lag_sleep_seconds', '1')) * 1000000));
        goto doquery;
    }

    info('lag: <fg=green;options=bold>stable</>!');
});

desc('Run one-time replication init commands (DESTRUCTIVE)');
task('provision:postgresql:replication:init', function () {
    if (!(has_role('db') && has_role('db_replica'))) {
        return;
    }

    if (get('no_really_i_mean_it') != 'reallyreally') {
        throw error('nah');
    }

    $asPostgres = 'NOPROMPT=1 sudo -u postgres -H';

    run(
        "{$asPostgres} pg_dropcluster --stop {{postgresql_version}} main || true",
    );
    run("{$asPostgres} pg_createcluster {{postgresql_version}} main");
    run('systemctl daemon-reload');
    run('systemctl start postgresql@{{postgresql_version}}-main');
    run("{$asPostgres} createuser -e {{db_name}}");

    run(
        "{$asPostgres} psql -c \"ALTER ROLE {{db_name}} ENCRYPTED PASSWORD '%secret%'\"",
        secret: get('db_password'),
    );

    run("{$asPostgres} createdb -O {{db_name}} -T template0 -e {{db_name}}");

    $schemaDest = '/var/tmp/{{db_replication_name}}.schema.sql';
    run("rm -f {$schemaDest}");

    run(
        "{$asPostgres} ssh {{db_replication_remote}} " .
            'pg_dump -d {{db_replication_name}} ' .
            '--no-comments --no-publications --no-privileges ' .
            '--format=p --schema-only | ' .
            "tee {$schemaDest} >/dev/null",
    );

    $schemaAdapted = '/var/tmp/{{db_name}}.adapted-schema.sql';

    run(
        'install -v -m 0640 -o postgres -g postgres ' .
            "{$schemaDest} {$schemaAdapted}",
    );
    run(
        "sed -i '" .
            "s/ OWNER TO {{db_replication_name}};\$/ OWNER TO {{db_name}};/g" .
            "' {$schemaAdapted}",
    );

    run(
        "{$asPostgres} psql -d {{db_name}} -v ON_ERROR_STOP=1 <{$schemaAdapted}",
    );
});

before('provision:postgresql', 'provision:postgresql:mount');
after('provision:update', 'provision:postgresql:update');
after('provision:postgresql', 'provision:postgresql:backup');
after('provision:postgresql', 'provision:postgresql:store_access');
after('provision:postgresql', 'provision:postgresql:restore_script');
after('provision:postgresql', 'provision:postgresql:replication_tunnel');
after('provision:postgresql', 'provision:postgresql:replication_tools');
after('provision:install', 'provision:postgresql:install');
