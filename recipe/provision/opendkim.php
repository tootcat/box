<?php declare(strict_types=1);
namespace Deployer;

desc('Provision OpenDKIM');
task('provision:opendkim', function () {
    if (!has_role('opendkim')) {
        return;
    }

    run('apt-get update -yq', env: ['DEBIAN_FRONTEND' => 'noninteractive']);

    run(
        'apt-get install -yq opendkim opendkim-tools',
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
    );

    run('install -d -o opendkim -g opendkim -m 0700 /etc/dkimkeys');
    run('echo "# this space intentionally left blank">/etc/default/opendkim');

    install(
        '{{templates_dir}}/opendkim.conf.tmpl',
        '/etc/opendkim.conf',
        'root',
        'root',
        '0644',
    );

    install_contents(
        gzdecode(base64_decode(get('stage_env')['DKIM_PRIVATE_KEY_GZB64'])),
        '/etc/dkimkeys/default.private',
        'opendkim',
        'opendkim',
        '0600',
    );

    install_contents(
        gzdecode(base64_decode(get('stage_env')['DKIM_PUBLIC_KEY_GZB64'])),
        '/etc/dkimkeys/default.txt',
        'opendkim',
        'opendkim',
        '0600',
    );

    run('systemctl enable --now opendkim');
    run('systemctl reload opendkim');
});

after('provision:install', 'provision:opendkim');
