<?php declare(strict_types=1);
namespace Deployer;

desc('Assigns floating ip');
task('provision:floating_ip', function () {
    if (empty(get('floating_ips'))) {
        return;
    }

    set(
        'floating_ips_json_array_string',
        json_encode(
            get('floating_ips'),
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
        ),
    );

    $rendered = parse(
        file_get_contents(
            parse('{{templates_dir}}/netplan-floating-ip.yaml.tmpl'),
        ),
    );

    install_contents(
        $rendered,
        '/etc/netplan/60-floating-ip.yaml',
        'root',
        'root',
        '0600',
    );

    run('chmod -v 0600 /etc/netplan/*.*');
    run('netplan apply');
});

before('provision:update', 'provision:floating_ip');
