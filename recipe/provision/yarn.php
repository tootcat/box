<?php declare(strict_types=1);
namespace Deployer;

task('provision:yarn', function () {
    if (!has_role('web')) {
        return;
    }

    run('corepack enable');
    run('yarn set version stable');
});
