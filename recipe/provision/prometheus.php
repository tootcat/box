<?php declare(strict_types=1);
namespace Deployer;

set('alertsmanager_alerting_targets', '[]');
set('alertsmanager_default_route_group_interval', '5m');
set('alertsmanager_default_route_group_wait', '30s');
set('alertsmanager_default_route_repeat_interval', '5m');
set('alertsmanager_receivers_email_configs', '[]');
set('elasticsearch_exporter_scrape_config_static_configs', '[]');
set('node_exporter_scrape_config_static_configs', '[]');
set('postfix_exporter_scrape_config_static_configs', '[]');
set('postgres_exporter_scrape_config_static_configs', '[]');
set('blackbox_exporter_http_2xx_scrape_configs_static_configs', '[]');
set('prometheus_config', '{{prometheus_config_dir}}/prometheus.yml');
set('prometheus_config_dir', '/etc/prometheus');
set('prometheus_db_dir', '/var/lib/prometheus');
set(
    'prometheus_download_url',
    'https://github.com/prometheus/prometheus/releases/download' .
        '/v{{prometheus_version}}/prometheus-{{prometheus_version}}.linux-amd64.tar.gz',
);
set(
    'prometheus_download_dest',
    '/tmp/prometheus-{{prometheus_version}}.linux-amd64',
);
set('prometheus_rules_files', []);
set('prometheus_scrape_config_targets', '[]');
set('prometheus_user', 'prometheus');
set('prometheus_version', '2.53.1');
set('statsd_exporter_scrape_config_static_configs', '[]');
set(
    'statsd_exporter_download_url',
    'https://github.com/prometheus/statsd_exporter/releases/download' .
        '/v{{statsd_exporter_version}}/statsd_exporter-{{statsd_exporter_version}}.linux-amd64.tar.gz',
);
set('statsd_exporter_version', '0.26.1');
set('valkey_exporter_scrape_config_static_configs', '[]');

desc('Installs prometheus');
task('provision:prometheus', function () {
    $prometheusPkgSvcs = ['prometheus-node-exporter'];

    if (has_role('prometheus')) {
        $prometheusPkgSvcs[] = 'prometheus-alertmanager';
        $prometheusPkgSvcs[] = 'prometheus-blackbox-exporter';
    }

    if (has_role('db')) {
        $prometheusPkgSvcs[] = 'prometheus-postgres-exporter';
    }

    if (has_role('elasticsearch')) {
        $prometheusPkgSvcs[] = 'prometheus-elasticsearch-exporter';
    }

    if (has_role('postfix')) {
        $prometheusPkgSvcs[] = 'prometheus-postfix-exporter';
    }

    if (has_role('valkey')) {
        $prometheusPkgSvcs[] = 'prometheus-redis-exporter';
    }

    run(
        'apt-get install -y ' . implode(' ', $prometheusPkgSvcs),
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
        timeout: 900,
    );

    if (has_role('elasticsearch')) {
        // TODO: install prometheus-elasticsearch-exporter v1.7.0 from
        // https://github.com/prometheus-community/elasticsearch_exporter/releases/tag/v1.7.0
        // instead of using the ubuntu 22.04 package which does not support scraping from
        // the version of elasticsearch we're using.
        install_contents(
            parse(
                implode("\n", [
                    'ARGS="--es.ssl-skip-verify --es.uri={{elasticsearch_auth_uri}}"',
                    '',
                ]),
            ),
            '/etc/default/prometheus-elasticsearch-exporter',
            'root',
            'root',
            '0644',
        );
    }

    if (has_role('prometheus')) {
        run(
            'getent group {{prometheus_user}} 2>/dev/null || ' .
                'groupadd --system {{prometheus_user}}',
        );
        run(
            'getent passwd {{prometheus_user}} 2>/dev/null || ' .
                join(' ', [
                    'useradd',
                    '--system',
                    '--home-dir {{prometheus_db_dir}}',
                    '--gid {{prometheus_user}}',
                    '--no-create-home',
                    '--shell /usr/sbin/nologin',
                    '{{prometheus_user}}',
                ]),
        );

        run(
            'install -d -o {{prometheus_user}} -g {{prometheus_user}} -m 0755 {{prometheus_db_dir}}',
        );

        run(
            'install -d -o root -g {{prometheus_user}} -m 0755 {{prometheus_config_dir}}',
        );
        run(
            'install -d -o root -g {{prometheus_user}} -m 0755 {{prometheus_config_dir}}/rules',
        );
        run(
            'install -d -o root -g {{prometheus_user}} -m 0755 {{prometheus_config_dir}}/files_sd',
        );
        run(
            'install -d -o root -g {{prometheus_user}} -m 0755 {{prometheus_config_dir}}/scrapes',
        );
        run(
            'install -d -o root -g {{prometheus_user}} -m 0755 {{prometheus_config_dir}}/console_libraries',
        );
        run(
            'install -d -o root -g {{prometheus_user}} -m 0755 {{prometheus_config_dir}}/consoles',
        );

        if (
            preg_match(
                parse('/.+version {{prometheus_version}}.+/'),
                run('prometheus --version || echo not installed'),
            ) === 0
        ) {
            run(
                'curl -fsSL -o {{prometheus_download_dest}}.tar.gz {{prometheus_download_url}}',
            );
            run('tar -C /tmp -xf {{prometheus_download_dest}}.tar.gz');

            run(
                'install -m 0755 -o root -g root {{prometheus_download_dest}}/prometheus /usr/local/bin/',
            );
            run(
                'install -m 0755 -o root -g root {{prometheus_download_dest}}/promtool /usr/local/bin/',
            );

            run(
                'rsync -av {{prometheus_download_dest}}/console_libraries/ {{prometheus_config_dir}}/console_libraries/',
            );
            run(
                'rsync -av {{prometheus_download_dest}}/consoles/ {{prometheus_config_dir}}/consoles/',
            );
        }

        install(
            '{{templates_dir}}/prometheus.service.tmpl',
            '/etc/systemd/system/prometheus.service',
            'root',
            'root',
            '0644',
        );

        run('touch {{prometheus_config}}');
        run('touch /etc/default/prometheus');

        $prometheusPkgSvcs[] = 'prometheus';
    }

    if (has_role('web')) {
        if (
            preg_match(
                parse('/.+version {{statsd_exporter_version}}.+/'),
                run('statsd_exporter --version || echo not installed'),
            ) === 0
        ) {
            run(
                'curl -fsSL -o /tmp/statsd_exporter.tar.gz {{statsd_exporter_download_url}}',
            );
            run(
                join(' ', [
                    'tar -C /tmp --strip-components=1 --wildcards',
                    '-xf /tmp/statsd_exporter.tar.gz',
                    '"statsd_exporter-*/statsd_exporter"',
                ]),
            );
            run(
                'install -m 0755 -o root -g root /tmp/statsd_exporter /usr/local/bin/',
            );

            run('install -d -m 0755 -o root -g root {{prometheus_config_dir}}');
        }

        install(
            '{{templates_dir}}/statsd-mapping.yaml.tmpl',
            '{{prometheus_config_dir}}/statsd-mapping.yaml',
            'root',
            'root',
            '0644',
        );
        install(
            '{{assets_dir}}/statsd-exporter.service',
            '/etc/systemd/system/statsd-exporter.service',
            'root',
            'root',
            '0644',
        );
        install_contents(
            'ARGS="--statsd.mapping-config={{prometheus_config_dir}}/statsd-mapping.yaml"' .
                "\n",
            '/etc/default/statsd-exporter',
            'root',
            'root',
            '0644',
        );

        $prometheusPkgSvcs[] = 'statsd-exporter';
    }

    if (has_role('db')) {
        install_contents(
            'DATA_SOURCE_NAME="' .
                join(' ', [
                    'user={{prometheus_user}}',
                    'host=/run/postgresql',
                    'dbname=postgres',
                ]) .
                '"' .
                "\n",
            '/etc/default/prometheus-postgres-exporter',
            'prometheus',
            'prometheus',
            '0640',
        );

        run(
            'NOPROMPT=1 sudo -u postgres -H ' .
                'createuser --role pg_monitor {{prometheus_user}} || true',
        );

        install_contents(
            parse(
                join("\n", [
                    "REDIS_ADDR='redis://{{db_private_ipv4}}:6379'",
                    "REDIS_USER='prometheus'",
                    "REDIS_PASSWORD='{{valkey_prometheus_password}}'",
                    "REDIS_EXPORTER_INCL_SYSTEM_METRICS='true'",
                    "REDIS_EXPORTER_LOG_FORMAT='json'",
                    "REDIS_EXPORTER_CONNECTION_TIMEOUT='3s'",
                ]) . "\n",
            ),
            '/etc/default/prometheus-redis-exporter',
            'prometheus',
            'prometheus',
            '0640',
        );
    }

    foreach ($prometheusPkgSvcs as $svc) {
        run("systemctl enable --now {$svc}");

        if (input()->getOption('softly')) {
            run("systemctl start {$svc}");
        } else {
            run("systemctl restart {$svc}");
        }
    }
});

desc('Configure prometheus');
task('provision:prometheus:configure', function () {
    if (!has_role('prometheus')) {
        return;
    }

    install(
        '{{templates_dir}}/prometheus.yml.tmpl',
        '{{prometheus_config}}',
        'root',
        'root',
        '0644',
    );

    install_contents(
        join('', [
            'ARGS="',
            join(' ', ['--enable-feature=otlp-write-receiver']),
            '"',
            "\n",
        ]),
        '/etc/default/prometheus',
        'root',
        'root',
        '0644',
    );

    install(
        '{{templates_dir}}/alertmanager.yml.tmpl',
        '{{prometheus_config_dir}}/alertmanager.yml',
        'root',
        'root',
        '0644',
    );

    foreach (get('prometheus_rules_files') as $src => $dest) {
        install(
            $src,
            "{{prometheus_config_dir}}/rules/{$dest}",
            'root',
            'root',
            '0644',
        );

        run("promtool check rules {{prometheus_config_dir}}/rules/{$dest}");
    }

    run('promtool check config {{prometheus_config}}');

    run('systemctl reload prometheus');
    run('systemctl reload prometheus-alertmanager');
});

after('provision:install', 'provision:prometheus');
after('provision:prometheus', 'provision:prometheus:configure');
