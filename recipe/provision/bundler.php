<?php declare(strict_types=1);
namespace Deployer;

set('bundler_user', function () {
    return ask(
        ' In which user account will bundler be installed? ',
        'deployer',
        [],
    );
});

set('bundler_user_home', function () {
    return explode(':', run('getent passwd {{bundler_user}}'))[5];
});

set('bundler_version', '~>2.5');

desc('Installs bundler');
task('provision:bundler', function () {
    if (!has_role('web')) {
        return;
    }

    info('Installing bundler {{bundler_version}} for user {{bundler_user}}');

    run('adduser --disabled-login {{bundler_user}} || true');
    run('adduser {{bundler_user}} deployer || true');

    run(
        implode(' ', [
            ' NOPROMPT=1',
            'sudo',
            '-H',
            '-u',
            '{{bundler_user}}',
            '{{bundler_user_home}}/.rbenv/shims/gem',
            'install',
            '--no-document',
            'bundler',
            '--version',
            '"{{bundler_version}}"',
        ]),
    );

    run(
        implode(' ', [
            ' NOPROMPT=1',
            'sudo',
            '-H',
            '-u',
            '{{bundler_user}}',
            '{{bundler_user_home}}/.rbenv/bin/rbenv',
            'rehash',
        ]),
    );
});
