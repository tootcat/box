<?php declare(strict_types=1);
namespace Deployer;

set('db_volume_mount', null);
set('elasticsearch_auth_uri', 'http://127.0.0.1:{{elasticsearch_http_port}}');
set('elasticsearch_uri', 'http://127.0.0.1:{{elasticsearch_http_port}}');
set('elasticsearch_version', '8.x');
set('elasticsearch_security_on', false);

set('elasticsearch_security_on_as_bool', function () {
    return json_encode(
        get('elasticsearch_security_on'),
        JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT,
    );
});

desc('Adds repository and update for elasticsearch');
task('provision:elasticsearch:update', function () {
    if (!has_role('elasticsearch')) {
        return;
    }

    run('mkdir -p /usr/share/keyrings');

    $elasticKeyring = '/usr/share/keyrings/elastic.asc';
    run(
        "curl -fsSL -o $elasticKeyring https://artifacts.elastic.co/GPG-KEY-elasticsearch",
    );

    run(
        implode(' ', [
            "echo 'deb [signed-by=$elasticKeyring] https://artifacts.elastic.co/packages/{{elasticsearch_version}}/apt stable main' |",
            'tee /etc/apt/sources.list.d/elastic-{{elasticsearch_version}}.list',
        ]),
    );
});

desc('Installs packages for elasticsearch');
task('provision:elasticsearch:install', function () {
    if (!has_role('elasticsearch')) {
        return;
    }

    run('apt-get update -yq', env: ['DEBIAN_FRONTEND' => 'noninteractive']);

    run(
        'apt-get install -y elasticsearch',
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
        timeout: 900,
    );
});

desc('Copy the elasticsearch HTTP CA file to storage');
task('provision:elasticsearch:ca_stash', function () {
    if (!has_role('elasticsearch')) {
        return;
    }

    store_stash(
        '/etc/elasticsearch/certs/http_ca.crt',
        '{{store_db_prefix}}/{{hostname}}/elasticsearch/certs/http_ca.crt',
    );
});

desc('Copy the elasticsearch HTTP CA file from storage for client use');
task('provision:elasticsearch:ca_unstash', function () {
    if (count(select('role.elasticsearch=ok')) == 0) {
        return;
    }

    if (!has_role('web')) {
        return;
    }

    store_unstash(
        '{{store_db_prefix}}/{{db_hostname}}/elasticsearch/certs/http_ca.crt',
        '/etc/elasticsearch/certs/http_ca.crt',
    );

    run('chown -R root:deployer /etc/elasticsearch');
    run('chmod 0750 /etc/elasticsearch /etc/elasticsearch/certs');
    run('chmod 0640 /etc/elasticsearch/certs/http_ca.crt');
});

task('provision:elasticsearch:mount', function () {
    if (!has_role('elasticsearch')) {
        return;
    }

    if (empty(get('db_volume_mount'))) {
        return;
    }

    run('ls -la {{db_volume_mount}}');
    run("mkdir -p '{{db_volume_mount}}/elasticsearch'");
    run(
        "chown -R elasticsearch:elasticsearch '{{db_volume_mount}}/elasticsearch'",
    );

    run(
        implode(' ', [
            "if ! grep -q '^{{db_volume_mount}}/elasticsearch /var/lib/elasticsearch' /etc/fstab; then",
            implode(' ', [
                "printf '{{db_volume_mount}}/elasticsearch /var/lib/elasticsearch none defaults,bind 0 0\\n' |",
                'tee -a /etc/fstab;',
            ]),
            'fi',
        ]),
    );

    run('systemctl daemon-reload');

    $lsblk = json_decode(run('lsblk --fs --paths --json'), true);

    foreach ($lsblk['blockdevices'] as $blockDevice) {
        if (in_array(get('db_volume_mount'), $blockDevice['mountpoints'])) {
            if (
                in_array('/var/lib/elasticsearch', $blockDevice['mountpoints'])
            ) {
                info('/var/lib/elasticsearch is already bind-mounted');

                return;
            }
        }
    }

    run('systemctl stop elasticsearch.service');
    run('mount /var/lib/elasticsearch');
    run('systemctl start elasticsearch.service');
})->hidden();

after('provision:update', 'provision:elasticsearch:update');
after('provision:install', 'provision:elasticsearch:install');
after('provision:elasticsearch:install', 'provision:elasticsearch:mount');
after('provision:install', 'provision:elasticsearch:ca_stash');
after('provision:deployer', 'provision:elasticsearch:ca_unstash');
