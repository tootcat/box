<?php declare(strict_types=1);
namespace Deployer;

set('traefik_version', '2.11.0');
set('traefik_api_dashboard', 'true');
set('traefik_api_insecure', 'true');
set('traefik_digitalocean_token', null);
set('traefik_dynamic_configs', []);
set('traefik_env', []);

set('traefik_api_dashboard_bool', function () {
    return boolval(get('traefik_api_dashboard'));
});

set('traefik_api_insecure_bool', function () {
    return boolval(get('traefik_api_insecure'));
});

// NOTE: this task adapted from:
// ttps://gist.github.com/ubergesundheit/7c9d875befc2d7bfd0bf43d8b3862d85
desc('Installs traefik');
task('provision:traefik:install', function () {
    if (!has_role('traefik')) {
        return;
    }

    $dest = '/usr/local/bin/traefik';

    if (
        run(
            "({$dest} version 2>/dev/null || true) | awk '/^Version:/ { print \$2 }'",
        ) == get('traefik_version')
    ) {
        info('traefik {{traefik_version}} is installed');
    } else {
        $tmpDest = '/tmp/traefik';
        $tmpDestTgz = "{$tmpDest}.tar.gz";
        $downloadURL = implode('', [
            'https://github.com/traefik/traefik/releases/download',
            '/v{{traefik_version}}/traefik_v{{traefik_version}}_linux_amd64.tar.gz',
        ]);

        run("curl -fsSL -o {$tmpDestTgz} {$downloadURL}");
        run("tar -C $(dirname {$tmpDest}) -xf {$tmpDestTgz} traefik");
        run("install -v -m 0755 -o root {$tmpDest} {$dest}");
        run("rm -f {$tmpDestTgz} {$tmpDest}");
    }

    run("setcap 'cap_net_bind_service=+ep' {$dest}");
    run('getent group traefik || groupadd --gid 321 traefik');
    run(
        implode(' ', [
            'getent passwd traefik ||',
            'useradd -g traefik --no-user-group --home-dir /var/www',
            '--no-create-home --shell /usr/sbin/nologin --system --uid 321',
            'traefik',
        ]),
    );
    run(
        'mkdir -p /etc/traefik/acme /etc/traefik/traefik.yaml.d /var/log/traefik',
    );

    $tmpToml = tempnam('/tmp', 'traefik-yaml-');

    file_put_contents(
        $tmpToml,
        parse(file_get_contents(parse('{{templates_dir}}/traefik.yaml.tmpl'))),
    );

    $tmpDestToml = '/tmp/traefik.yaml';
    $destToml = '/etc/traefik/traefik.yaml';

    upload($tmpToml, $tmpDestToml);
    unlink($tmpToml);

    run("install -v -m 0644 -o root {$tmpDestToml} {$destToml}");
    run("rm -f {$tmpDestToml}");

    $tmpEnv = tempnam('/tmp', 'traefik-env-');

    file_put_contents(
        $tmpEnv,
        parse(
            <<<EOSH
            DO_AUTH_TOKEN={{traefik_digitalocean_token}}
            EOSH
            ,
        ),
    );

    $tmpDestEnv = '/tmp/traefik-env';
    $destEnv = '/etc/default/traefik';

    upload($tmpEnv, $tmpDestEnv);
    unlink($tmpEnv);

    run("install -v -m 0600 -o traefik {$tmpDestEnv} {$destEnv}");
    run("rm -f {$tmpDestEnv}");

    $tmpDestService = '/tmp/traefik.service';
    $destService = '/etc/systemd/system/traefik.service';

    upload('{{assets_dir}}/traefik.service', $tmpDestService);
    run("install -v -m 0644 -o root {$tmpDestService} {$destService}");
    run("rm -f {$tmpDestService}");

    upload(
        '{{assets_dir}}/logrotate-traefik.conf',
        '/etc/logrotate.d/traefik',
        ['options' => ['--chown=root:root']],
    );

    run('chown -v root:root /etc/traefik');
    run('chown -v -R traefik:traefik /etc/traefik/acme');
    run('chown -v -R traefik:traefik /etc/traefik/traefik.yaml.d');
    run('chown -v -R traefik:syslog /var/log/traefik');
    run('systemctl daemon-reload');
    run('systemctl enable --now traefik');
    if (input()->getOption('softly')) {
        run('systemctl start traefik');
    } else {
        run('systemctl restart traefik');
    }
});

desc('Sync traefik dynamic configs');
task('provision:traefik:sync_configs', function () {
    if (!has_role('traefik')) {
        return;
    }

    $tmpOut = tempnam('/tmp', 'traefik-dynamic-config-');
    unlink($tmpOut);
    mkdir($tmpOut);

    foreach (get('traefik_dynamic_configs') as $cfgPath) {
        $cfgPath = parse($cfgPath);

        file_put_contents(
            "{$tmpOut}/" . basename($cfgPath, '.tmpl'),
            str_replace('\\/\\/', '//', parse(file_get_contents($cfgPath))),
        );
    }

    upload("{$tmpOut}/", '/etc/traefik/traefik.yaml.d/', [
        'options' => ['--delete', '--chown=traefik:traefik'],
    ]);

    runLocally("rm -rf {$tmpOut}");
});

desc('Sync one traefik dynamic config');
task('provision:traefik:sync_one_config', function () {
    if (!has_role('traefik')) {
        return;
    }

    $cfgPath = get('filename', null);

    if (empty($cfgPath)) {
        throw error('missing "filename" option');
    }

    install_contents(
        str_replace('\\/\\/', '//', parse(file_get_contents($cfgPath))),
        '/etc/traefik/traefik.yaml.d/' . basename($cfgPath, '.tmpl'),
        'traefik',
        'traefik',
        '0644',
    );
});

task('logs:traefik:access', function () {
    if (!has_role('traefik')) {
        return;
    }

    run('tail -F /var/log/traefik/access.log', timeout: 900);
})->verbose();

after('provision:install', 'provision:traefik:install');
after('provision:traefik:install', 'provision:traefik:sync_configs');
