<?php declare(strict_types=1);
namespace Deployer;

desc('Provision the Postfix as an outbound SMTP server');
task('provision:postfix', function () {
    if (!has_role('postfix')) {
        return;
    }

    run('apt-get update -yq', env: ['DEBIAN_FRONTEND' => 'noninteractive']);

    $dest = '/tmp/dep-postfix-debconf-set-selections';

    install_contents(
        parse(
            implode("\n", [
                'postfix postfix/mailname string {{domain}}',
                'postfix postfix/main_mailer_type select Internet Site',
                'postfix postfix/mynetworks string 127.0.0.0/8 [::]/128 {{private_subnet_cidr}}',
            ]),
        ) . "\n",
        $dest,
        'root',
        'root',
        '0644',
    );

    run(
        "debconf-set-selections {$dest}",
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
    );

    run("rm -f {$dest}");

    run(
        'apt-get install -yq postfix',
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
    );

    install(
        '{{templates_dir}}/postfix-main.cf.tmpl',
        '/etc/postfix/main.cf',
        'root',
        'root',
        '0644',
    );

    $stageEnv = get('stage_env');

    if (!empty($stageEnv['SMTP_RELAY_HOST'])) {
        install_contents(
            implode('', [
                '[',
                $stageEnv['SMTP_RELAY_HOST'],
                ']:submission ',
                $stageEnv['SMTP_RELAY_USERNAME'],
                ':',
                $stageEnv['SMTP_RELAY_PASSWORD'],
                "\n",
            ]),
            '/etc/postfix/sasl_passwd',
            'root',
            'root',
            '0600',
        );

        run('postmap /etc/postfix/sasl_passwd');
    }

    run('systemctl enable --now postfix');
    run('systemctl reload postfix');
});

after('provision:install', 'provision:postfix');
