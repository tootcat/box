<?php declare(strict_types=1);
namespace Deployer;

set('doctl_version', '1.104.0');
set('hcloud_version', '1.43.0');
set('sops_version', '3.9.0');

desc('Provision localhost for doing things');
task('provision:local', function () {
    info(
        "If you can read this, then 'sudo' may be waiting for you to " .
            'type your password and hit ENTER',
    );
    system('sudo whoami >/dev/null');

    run(
        implode(' ', [
            'sudo apt-get install -y',
            implode(' ', [
                'age',
                'apt-transport-https',
                'build-essential',
                'ca-certificates',
                'curl',
                'make',
                'nodejs',
                'npm',
                'php-yaml',
                'rsync',
                'sshpass',
            ]),
        ]),
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
        timeout: 900,
    );
})->limit(1);

task('provision:local:npm', function () {
    run('npm install');
})->limit(1);

task('provision:local:doctl', function () {
    run(
        implode(' ', [
            'curl -fsSL -o /tmp/doctl.tar.gz',
            'https://github.com/digitalocean/doctl/releases/download' .
            '/v{{doctl_version}}/doctl-{{doctl_version}}-linux-amd64.tar.gz',
        ]),
    );
    run('cd /tmp && tar -xf doctl.tar.gz doctl');
    run('sudo install -v -m 0755 -o root /tmp/doctl /usr/local/bin/doctl');
    run('rm -vf /tmp/doctl /tmp/doctl.tar.gz');
})->limit(1);

task('provision:local:hcloud', function () {
    run(
        implode(' ', [
            'curl -fsSL -o /tmp/hcloud.tar.gz',
            'https://github.com/hetznercloud/cli/releases/download' .
            '/v{{hcloud_version}}/hcloud-linux-amd64.tar.gz',
        ]),
    );
    run('cd /tmp && tar -xf hcloud.tar.gz hcloud');
    run('sudo install -v -m 0755 -o root /tmp/hcloud /usr/local/bin/hcloud');
    run('rm -vf /tmp/hcloud /tmp/hcloud.tar.gz');
})->limit(1);

task('provision:local:sops', function () {
    run(
        implode(' ', [
            'curl -fsSL -o /tmp/sops',
            'https://github.com/getsops/sops/releases/download' .
            '/v{{sops_version}}/sops-v{{sops_version}}.linux.amd64',
        ]),
    );
    run('sudo install -v -m 0755 -o root /tmp/sops /usr/local/bin/sops');
    run('rm -f /tmp/sops');
})->limit(1);

task('provision:local:tofu', function () {
    $openTofuKeyring = '/etc/apt/keyrings/opentofu.gpg';
    $openTofuRepoKeyring = '/etc/apt/keyrings/opentofu-repo.gpg';
    $openTofuRepo = 'https://packages.opentofu.org/opentofu/tofu/any/';

    run('sudo install -m 0755 -d /etc/apt/keyrings');
    run(
        'curl -fsSL https://get.opentofu.org/opentofu.gpg | ' .
            "sudo tee {$openTofuKeyring} >/dev/null",
    );
    run(
        "if ! test -f {$openTofuRepoKeyring}; then " .
            'curl -fsSL https://packages.opentofu.org/opentofu/tofu/gpgkey | ' .
            "sudo gpg --no-tty --batch --dearmor -o {$openTofuRepoKeyring} >/dev/null; " .
            'fi',
    );
    run('sudo chmod a+r /etc/apt/keyrings/opentofu.gpg');

    $openTofuList = <<<EOAPTLIST
    deb [signed-by={$openTofuKeyring},{$openTofuRepoKeyring}] {$openTofuRepo} any main
    deb-src [signed-by={$openTofuKeyring},{$openTofuRepoKeyring}] {$openTofuRepo} any main
    EOAPTLIST;

    run(
        implode(' ', [
            "echo \"\${OPENTOFU_LIST_BASE64}\" | base64 --decode |",
            'sudo tee /etc/apt/sources.list.d/opentofu.list >/dev/null',
        ]),
        env: ['OPENTOFU_LIST_BASE64' => base64_encode($openTofuList)],
    );

    run(
        'sudo apt-get update -yqq',
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
    );
    run(
        'sudo apt-get install -yqq tofu',
        env: ['DEBIAN_FRONTEND' => 'noninteractive'],
    );
})->limit(1);

after('provision:local', 'provision:local:npm');
after('provision:local', 'provision:local:doctl');
after('provision:local', 'provision:local:hcloud');
after('provision:local', 'provision:local:sops');
after('provision:local', 'provision:local:tofu');
