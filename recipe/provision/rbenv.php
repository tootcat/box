<?php declare(strict_types=1);
namespace Deployer;

set('store_home_prefix', '');

set('rbenv_user', function () {
    return ask(
        ' In which user account will rbenv be installed? ',
        'deployer',
        [],
    );
});

set('rbenv_user_home', function () {
    return explode(':', run('getent passwd {{rbenv_user}}'))[5];
});

set('bin/rbenv', '{{rbenv_user_home}}/.rbenv/bin/rbenv');

set('rbenv_git_url', 'https://github.com/rbenv/rbenv.git');
set('ruby_build_git_url', 'https://github.com/rbenv/ruby-build.git');
set('rbenv_ruby_versions', ['3.2.3']);
set('rbenv_global_version', function () {
    return max(get('rbenv_ruby_versions'));
});

desc('Installs rbenv');
task('provision:rbenv', function () {
    if (!has_role('web')) {
        return;
    }

    info('Installing rbenv for user {{rbenv_user}} from {{rbenv_git_url}}');

    run('adduser --quiet --disabled-login {{rbenv_user}} || true');
    run('adduser --quiet {{rbenv_user}} deployer || true');

    $ensureRbenv = '/usr/local/bin/dep-ensure-rbenv.php';

    install(
        '{{assets_dir}}/dep-ensure-rbenv.php',
        $ensureRbenv,
        'root',
        'deployer',
        '0755',
    );

    run(
        implode(' ', [
            ' NOPROMPT=1',
            'sudo',
            '-E',
            '-H',
            '-u',
            '{{rbenv_user}}',
            $ensureRbenv,
        ]),
        env: [
            'RBENV_GIT_URL' => get('rbenv_git_url'),
            'RUBY_BUILD_GIT_URL' => get('ruby_build_git_url'),
        ],
    );
})->oncePerNode();

task('provision:rbenv:cache:pull', function () {
    if (!has_role('web')) {
        return;
    }

    if (empty(get('store_dest_host'))) {
        warning('No store configured; skipping cache pull');
        return;
    }

    foreach (['.local/', '.rbenv/'] as $prefix) {
        store_unstash(
            "{{store_home_prefix}}/$prefix",
            "{{rbenv_user_home}}/$prefix",
            1800,
            true,
        );
    }

    run('chown -R {{rbenv_user}}:deployer {{rbenv_user_home}}');
})->hidden();

task('provision:rbenv:versions', function () {
    if (!has_role('web')) {
        return;
    }

    foreach (get('rbenv_ruby_versions') as $v) {
        cd('{{rbenv_user_home}}');
        run(
            implode(' ', [
                ' NOPROMPT=1',
                'sudo',
                '-E',
                '-H',
                '-u',
                '{{rbenv_user}}',
                '{{bin/rbenv}}',
                'install',
                '--skip-existing',
                $v,
            ]),
            env: [
                'RUBY_CONFIGURE_OPTS' => '--with-jemalloc',
                'RUBY_BUILD_CURL_OPTS' => '-s',
            ],
            timeout: 1800,
            idle_timeout: 1800,
        );
    }

    run(
        implode(' ', [
            ' NOPROMPT=1',
            'sudo',
            '-H',
            '-u',
            '{{rbenv_user}}',
            '{{bin/rbenv}}',
            'global',
            '{{rbenv_global_version}}',
        ]),
    );
});

task('provision:rbenv:cache:push', function () {
    if (!has_role('web')) {
        return;
    }

    if (empty(get('store_dest_host'))) {
        warning('No store configured; skipping cache pull');
        return;
    }

    foreach (['.local/', '.rbenv/'] as $prefix) {
        store_stash(
            "{{rbenv_user_home}}/$prefix",
            "{{store_home_prefix}}/$prefix",
            1800,
            true,
        );
    }
})->hidden();

after('provision:rbenv', 'provision:rbenv:versions');
before('provision:rbenv:versions', 'provision:rbenv:cache:pull');
after('provision:rbenv:versions', 'provision:rbenv:cache:push');
