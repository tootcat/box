<?php declare(strict_types=1);
namespace Deployer;

use function Deployer\Support\parse_home_dir;

set('sudo_password', null);
set('ssh_copy_id', '');

desc('Setups a deployer user');
task('provision:deployer', function () {
    if (!has_role('web')) {
        return;
    }

    if (test('id deployer >/dev/null 2>&1')) {
        // TODO: Check what created deployer user configured correctly.
        // TODO: Update sudo_password of deployer user.
        // TODO: Copy ssh_copy_id to deployer ssh dir.
        info('deployer user already exist');
    } else {
        run('useradd deployer');
        run('mkdir -p /home/deployer/.ssh');
        run('mkdir -p /home/deployer/.deployer');
        run('adduser deployer sudo');

        run('chsh -s /bin/bash deployer');
        run('cp /root/.profile /home/deployer/.profile');
        run('cp /root/.bashrc /home/deployer/.bashrc');

        // Make color prompt.
        run(
            "sed -i 's/#force_color_prompt=yes/force_color_prompt=yes/' /home/deployer/.bashrc",
        );

        $password = run("mkpasswd -m sha-512 '%secret%'", [
            'secret' => get('sudo_password'),
        ]);
        run("usermod --password '%secret%' deployer", ['secret' => $password]);

        if (!empty(get('ssh_copy_id'))) {
            $file = parse_home_dir(get('ssh_copy_id'));
            if (!file_exists($file)) {
                info('Configure path to your public key.');
                writeln('');
                writeln(
                    "    set(<info>'ssh_copy_id'</info>, <info>'~/.ssh/id_rsa.pub'</info>);",
                );
                writeln('');
                $file = ask(
                    ' Specify path to your public ssh key: ',
                    '~/.ssh/id_rsa.pub',
                );
            }
            run(
                'echo "$KEY" >> /root/.ssh/authorized_keys',
                env: ['KEY' => file_get_contents(parse_home_dir($file))],
            );
        }

        run(
            'cp /root/.ssh/authorized_keys /home/deployer/.ssh/authorized_keys',
        );
        run('ssh-keygen -f /home/deployer/.ssh/id_rsa -t rsa -N ""');

        run('chown -R deployer:deployer /home/deployer');
        run('chmod 755 /home/deployer');
        run('chmod 700 /home/deployer/.ssh/id_rsa');

        run('usermod -a -G www-data deployer');
        run('groups deployer');
    }
})->oncePerNode();
