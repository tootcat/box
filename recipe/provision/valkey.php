<?php declare(strict_types=1);
namespace Deployer;

set('valkey_user', 'notset');
set('valkey_password', null);
set('valkey_default_password', null);
set('valkey_memory', '100M');
set('valkey_image', 'docker.io/valkey/valkey:7.2.5-alpine');
set('valkey_uid', '1000');
set('valkey_gid', '999');

desc('Provision the valkey server');
task('provision:valkey', function () {
    if (!has_role('valkey')) {
        return;
    }

    run('getent group valkey || groupadd --gid {{valkey_gid}} valkey');

    run(
        implode(' ', [
            'getent passwd valkey ||',
            'useradd',
            '--uid {{valkey_uid}}',
            '--gid {{valkey_gid}}',
            '--home-dir /var/lib/valkey',
            '--no-create-home',
            '--shell /usr/sbin/nologin',
            'valkey',
        ]),
    );

    run(
        implode(' ', [
            'install',
            '-v',
            '-d',
            '-o valkey',
            '-g root',
            '-m 0755',
            '/var/log/valkey',
            '/var/lib/valkey',
            '/etc/valkey',
        ]),
    );

    run('sysctl vm.overcommit_memory=1');
    install_contents(
        "vm.overcommit_memory=1\n",
        '/etc/sysctl.d/90-valkey.conf',
        'root',
        'root',
        '0644',
    );

    install_contents(
        parse(
            implode("\n", [
                'VALKEY_SERVER_IMAGE={{valkey_image}}',
                'VALKEY_SERVER_MEMORY={{valkey_memory}}',
                'VALKEY_SERVER_PRIVATE_IPV4={{private_ipv4}}',
            ]) . "\n",
        ),
        '/etc/default/valkey-server',
        'valkey',
        'root',
        '0644',
    );

    install(
        '{{assets_dir}}/valkey-server.service',
        '/usr/lib/systemd/system/valkey-server.service',
        'valkey',
        'root',
        '0644',
    );

    install(
        '{{templates_dir}}/valkey.conf.tmpl',
        '/etc/valkey/valkey.conf',
        'valkey',
        'root',
        '0640',
    );

    run('systemctl disable --now redis-server || true');
    run('systemctl daemon-reload');
    run('systemctl enable --now valkey-server');

    if (input()->getOption('softly')) {
        run('systemctl start valkey-server');
    } else {
        run('systemctl restart valkey-server');
    }
})->oncePerNode();

desc('Import snapshot to valkey server');
task('provision:valkey:import', function () {
    install(
        get('dump', '/tmp/dump.rdb'),
        '/var/lib/valkey/dump.rdb',
        'valkey',
        'root',
        '0600',
    );
});
