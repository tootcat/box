<?php declare(strict_types=1);
namespace Deployer;

set('apt_env', ['DEBIAN_FRONTEND' => 'noninteractive']);
set('use_certbot', true);

desc('Adds repositories and update');
task('provision:update', function () {
    run('apt-get update', env: get('apt_env'));
    run(
        'apt-get install -y software-properties-common apt-transport-https',
        env: get('apt_env'),
    );

    run('apt-get update', env: get('apt_env'));
})->oncePerNode();

desc('Adds repositories and update for web role');
task('provision:update:web', function () {
    if (!has_role('web')) {
        return;
    }

    run('apt-add-repository ppa:ondrej/php -y', env: get('apt_env'));

    $nodesource_keyring = '/usr/share/keyrings/nodesource.asc';
    run(
        "curl -fsSL -o {$nodesource_keyring} https://deb.nodesource.com/gpgkey/nodesource.gpg.key",
    );
    run(
        "echo 'deb [signed-by={$nodesource_keyring}] https://deb.nodesource.com/{{nodejs_version}} {{lsb_release}} main' | tee /etc/apt/sources.list.d/nodesource.list",
    );
    run(
        "echo 'deb-src [signed-by={$nodesource_keyring}] https://deb.nodesource.com/{{nodejs_version}} {{lsb_release}} main' | tee -a /etc/apt/sources.list.d/nodesource.list",
    );

    run('apt-get update', env: get('apt_env'));
});

desc('Installs packages');
task('provision:install', function () {
    info('delegating install to role-specific tasks');
})->oncePerNode();

desc('Installs packages for web role');
task('provision:install:web', function () {
    if (!has_role('web')) {
        return;
    }

    $packages = [
        'acl',
        'apt-transport-https',
        'autoconf',
        'bison',
        'build-essential',
        'curl',
        'debian-archive-keyring',
        'debian-keyring',
        'fail2ban',
        'ffmpeg',
        'file',
        'g++',
        'gcc',
        'git',
        'git-core',
        'gosu',
        'imagemagick',
        'jq',
        'libffi-dev',
        'libgdbm-dev',
        'libicu-dev',
        'libidn11-dev',
        'libjemalloc-dev',
        'libmcrypt4',
        'libncurses5-dev',
        'libpcre3-dev',
        'libpq-dev',
        'libprotobuf-dev',
        'libreadline6-dev',
        'libsqlite3-dev',
        'libssl-dev',
        'libxml2-dev',
        'libxslt1-dev',
        'libyaml-dev',
        'logrotate',
        'make',
        'ncdu',
        'nginx',
        'nodejs',
        'pkg-config',
        'protobuf-compiler',
        'python-is-python3',
        'redis-tools',
        'rsync',
        'sshpass',
        'sqlite3',
        'ufw',
        'unzip',
        'uuid-runtime',
        'wait-for-it',
        'whois',
        'zlib1g-dev',
    ];

    if (get('use_certbot')) {
        $packages = array_merge($packages, [
            'certbot',
            'python3-certbot-nginx',
        ]);
    }

    run(
        'apt-get install -y ' . implode(' ', $packages),
        env: get('apt_env'),
        timeout: 900,
    );
})->oncePerNode();

desc('Installs packages for db role');
task('provision:install:db', function () {
    if (!has_role('db')) {
        return;
    }

    $packages = array_merge(
        [
            'acl',
            'apt-transport-https',
            'curl',
            'debian-archive-keyring',
            'debian-keyring',
            'fail2ban',
            'file',
            'jq',
            'pkg-config',
            'python-is-python3',
            'redis-tools',
            'rsync',
            'sshpass',
            'ufw',
            'unzip',
            'uuid-runtime',
            'whois',
            'zlib1g-dev',
        ],
        has_role('valkey') ? ['crun', 'podman', 'uidmap'] : [],
    );

    run(
        'apt-get install -y ' . implode(' ', $packages),
        env: get('apt_env'),
        timeout: 900,
    );
})->oncePerNode();

desc('Provision logrotate');
task('provision:base:logrotate', function () {
    run('install -d -m 0755 -o root -g root /etc/logrotate.d');

    install(
        '{{templates_dir}}/logrotate.d-rsyslog.conf.tmpl',
        '/etc/logrotate.d/rsyslog',
        'root',
        'root',
        '0644',
    );

    run('systemctl enable --now logrotate.timer');
});

before('provision:install', 'provision:install:db');
before('provision:install', 'provision:install:web');

after('provision:update', 'provision:update:web');
after('provision:install', 'provision:base:logrotate');
