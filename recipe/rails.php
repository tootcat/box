<?php declare(strict_types=1);
namespace Deployer;

use Symfony\Component\Console\Input\InputOption;

require_once __DIR__ . '/deploy/bundler.php';
require_once __DIR__ . '/deploy/yarn.php';
require_once __DIR__ . '/provision.php';

add('recipes', ['rails']);

option(
    'hastily',
    null,
    InputOption::VALUE_NONE,
    'Deploy hastily, without waiting!',
);

set('bin/rails', '{{bin/bundle}} exec {{release_or_current_path}}/bin/rails');

set('db_user', '');
set('db_password', '');
set('db_name', '');
set('db_volume_mount', null);
set('rails_dotenv', []);

desc('Provision php (maybe)');
task('provision:php:maybe', function () {
    if (!has_role('web')) {
        return;
    }

    invoke('provision:php');
})->oncePerNode();

function getCacheHash() {
    return sha1(
        run(
            implode(' ', [
                'sha1sum {{deploy_path}}/.env',
                '{{release_or_current_path}}/.env*',
                '{{release_or_current_path}}/Gemfile.lock',
                '{{release_or_current_path}}/yarn.lock |',
                'cut -b1-40',
            ]),
        ),
    );
}

desc('Pull artifacts from cache');
task('deploy:cache:pull', function () {
    if (!has_role('web')) {
        return;
    }

    if (empty(get('store_dest_host'))) {
        warning('No store configured; skipping cache pull');
        return;
    }

    $cacheHash = getCacheHash();

    foreach (
        [
            '{{store_shared_prefix}}/shared/{{target}}/',
            '{{store_shared_prefix}}/shared/' . $cacheHash . '/',
        ]
        as $prefix
    ) {
        store_unstash($prefix, '{{deploy_path}}/shared/', 1800, true);
    }

    run('chown -R deployer:deployer {{deploy_path}}/shared', timeout: 300);
});

desc('Push artifacts to cache');
task('deploy:cache:push', function () {
    if (!has_role('web')) {
        return;
    }

    if (empty(get('store_dest_host'))) {
        warning('No store configured; skipping cache push');
        return;
    }

    $cacheHash = getCacheHash();

    foreach (
        [
            '{{store_shared_prefix}}/shared/{{target}}/',
            '{{store_shared_prefix}}/shared/' . $cacheHash . '/',
        ]
        as $prefix
    ) {
        store_stash('{{deploy_path}}/shared/', $prefix, 1800);
    }

    run('mkdir -p {{deploy_path}}/.caches');

    run(
        'date -u | tee "{{deploy_path}}/.caches/${CACHE_HASH}"',
        env: ['CACHE_HASH' => $cacheHash],
    );

    run('chown -R deployer:deployer {{deploy_path}}/.caches');
});

desc('Precompile assets');
task('deploy:precompile', function () {
    if (!has_role('web')) {
        return;
    }

    $cacheHash = getCacheHash();
    if (test('test -f {{deploy_path}}/.caches/' . $cacheHash)) {
        info('cache already present for assets; skipping precompile');
        return;
    }

    run('chown -R {{bundler_user}} {{deploy_path}}');
    run(
        implode(' ', [
            ' NOPROMPT=1',
            'sudo',
            '-E',
            '-H',
            '-u',
            '{{bundler_user}}',
            'bash',
            '-c',
            "'cd {{release_path}} && {{bin/bundle}} exec bin/rails assets:precompile --trace'",
        ]),
        env: [
            'ACTIVE_RECORD_ENCRYPTION_DETERMINISTIC_KEY' =>
                'precompile_placeholder',
            'ACTIVE_RECORD_ENCRYPTION_KEY_DERIVATION_SALT' =>
                'precompile_placeholder',
            'ACTIVE_RECORD_ENCRYPTION_PRIMARY_KEY' => 'precompile_placeholder',
            'NODE_OPTIONS' => '--max-old-space-size=4096',
            'OTP_SECRET' => 'precompile_placeholder',
            'RAILS_ENV' => 'production',
            'SECRET_KEY_BASE' => 'precompile_placeholder',
        ],
        timeout: 1800,
        idle_timeout: 1800,
    );
    run('chown -R deployer:deployer {{deploy_path}}');
});

desc('Install all vendor dependencies');
task('deploy:vendors', function () {
    if (!has_role('web')) {
        return;
    }

    invoke('deploy:vendors:bundler');
    invoke('deploy:vendors:yarn');
});

desc('Run rails database migrations');
task('rails:db:migrate', function () {
    if (!has_role('web')) {
        return;
    }

    run(
        implode(' ', [
            ' NOPROMPT=1',
            'sudo',
            '-E',
            '-H',
            '-u',
            '{{bundler_user}}',
            'bash',
            '-c',
            "'cd {{current_path}} && {{bin/bundle}} exec bin/rails db:migrate --trace'",
        ]),
        env: get('rails_dotenv'),
        timeout: 3600,
    );
})->once();

task('deploy:success', function () {
    if (!has_role('web')) {
        return;
    }

    info('successfully deployed!');
});

desc('Deploys project');
task('deploy', [
    'deploy:prepare',
    'deploy:cache:pull',
    'deploy:vendors',
    'deploy:precompile',
    'deploy:publish',
    'deploy:cache:push',
]);
