<?php declare(strict_types=1);
namespace Deployer;

set('bin/bundle', '{{bundler_user_home}}/.rbenv/shims/bundle');
set('bundler_action', 'install');
set('bundler_deployment', 'true');
set('bundler_without', 'development:test');
set('bundler_frozen', 'true');

desc('Installs bundler dependencies');
task('deploy:vendors:bundler', function () {
    if (!has_role('web')) {
        return;
    }

    run('chown -R {{bundler_user}} {{release_path}}');
    run(
        implode(' ', [
            ' NOPROMPT=1',
            'sudo',
            '-H',
            '-u',
            '{{bundler_user}}',
            'bash',
            '-c',
            "'cd {{release_path}} && {{bin/bundle}} {{bundler_action}}'",
        ]),
        env: [
            'BUNDLE_DEPLOYMENT' => get('bundler_deployment'),
            'BUNDLE_WITHOUT' => get('bundler_without'),
            'BUNDLE_FROZEN' => get('bundler_frozen'),
        ],
        timeout: 900,
        idle_timeout: 900,
    );
    run('chown -R deployer:deployer {{release_path}}');
});
