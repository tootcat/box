<?php declare(strict_types=1);
namespace Deployer;

desc('Installs yarn dependencies');
task('deploy:vendors:yarn', function () {
    if (!has_role('web')) {
        return;
    }

    run(
        'cd {{release_path}} && {{bin/yarn}} install --immutable',
        timeout: 900,
        idle_timeout: 900,
    );
});
