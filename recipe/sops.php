<?php declare(strict_types=1);
namespace Deployer;

add('recipes', ['sops']);

desc('Re-encrypt env files');
task('sops:envs:reencrypt', function () {
    $rcpt = explode(',', getenv('SOPS_AGE_RECIPIENTS'));
    $nRcpt = count($rcpt);

    info("re-encrypting to {$nRcpt} recipient(s)");

    $envDirGlob = parse('{{top}}/envs/*.enc.env');

    info("globbing {$envDirGlob}");

    foreach (glob($envDirGlob) as $envFile) {
        $baseEnvFile = basename($envFile, '.enc.env');
        $unencFile = parse("{{top}}/envs/{$baseEnvFile}.unenc.env");

        runLocally(
            implode(' ', [
                "sops -d {$envFile} >{$unencFile} &&",
                "sops --encrypt -a \"\${SOPS_AGE_RECIPIENTS}\" {$unencFile} >{$envFile}",
            ]),
            env: ['SOPS_AGE_RECIPIENTS' => implode(',', $rcpt)],
        );
        runLocally("rm -f {$unencFile}");
    }
});
