<?php declare(strict_types=1);
namespace Deployer;

set('hetzlet_n_keepers', '5');
set('hetzlet_resolver_region', 'sg');
set('hetzlet_region', 'hel1');
set('hetzlet_type', 'cx31');
set('hetzlet_compatible_host', false);
set('doctl_sld', '');

desc('Ensure the hetzlet is up');
task('hetzlet:up', function () {
    if (!get('hetzlet_compatible_host')) {
        throw error(
            '<fg=red;options=bold>This host is not hetzlet-compatible, egad!</>',
        );
    }

    $env = loadEnv();

    $hetzlet = ensureHetzletUp($env);

    info(
        'ensured hetzlet <fg=yellow;options=bold>' .
            $hetzlet['name'] .
            '</> is up at <fg=green;options=bold>' .
            $hetzlet['public_net']['ipv4']['ip'] .
            '</>',
    );
});

desc('Ensure the hetzlet is down');
task('hetzlet:down', function () {
    if (!get('hetzlet_compatible_host')) {
        throw error(
            '<fg=red;options=bold>This host is not hetzlet-compatible, egad!</>',
        );
    }

    $env = loadEnv();

    ensureHetzletDown($env);

    info('ensured hetzlet <fg=yellow;options=bold>{{domain}}</> is down');
});

function loadEnv() {
    return array_merge(
        \Dotenv\Dotenv::parse(file_get_contents(__DIR__ . '/../envs/all.env')),
        [
            'HETZLET_N_KEEPERS' => get('hetzlet_n_keepers'),
            'HETZLET_RESOLVER_REGION' => get('hetzlet_resolver_region'),
            'HETZLET_REGION' => get('hetzlet_region'),
            'HETZLET_TYPE' => get('hetzlet_type'),
        ],
        \Dotenv\Dotenv::parse(
            runLocally('sops -d ' . __DIR__ . '/../envs/all.enc.env'),
        ),
    );
}

function findImage(array $env) {
    $images = json_decode(
        runLocally(
            implode(' ', [
                'hcloud',
                'image',
                'list',
                '--selector',
                'env={{stage}}',
                '--sort',
                'created:asc',
                '--output',
                'json',
            ]),
            env: $env,
        ),
        true,
    );

    $images = array_filter(
        $images,
        fn($img): bool => $img['description'] == get('domain'),
    );

    return end($images)['id'];
}

function findServer(array $env) {
    $servers = json_decode(
        runLocally(
            implode(' ', [
                'hcloud',
                'server',
                'list',
                '--selector',
                'env={{stage}}',
                '--sort',
                'created:asc',
                '--output',
                'json',
            ]),
            env: $env,
        ),
        true,
    );

    $servers = array_filter(
        $servers,
        fn($srv): bool => $srv['name'] == get('domain'),
    );

    return end($servers);
}

function findDNSRecord(string $name, array $env) {
    $unfilteredDNSRecords = json_decode(
        runLocally(
            'doctl compute domain records list {{doctl_sld}} -o json',
            env: $env,
        ),
        true,
    );

    $aRecords = array_filter(
        $unfilteredDNSRecords,
        fn($entry): bool => in_array($entry['type'], ['A', 'AAAA']),
    );

    $dnsRecords = array_filter(
        $unfilteredDNSRecords,
        fn($entry): bool => $entry['name'] == $name,
    );

    if (empty($dnsRecords)) {
        return null;
    }

    return end($dnsRecords);
}

function ensureHetzletUp(array $env) {
    $hetzlet = ensureHetzlet($env);

    ensureDNSRecords($env);

    return $hetzlet;
}

function ensureHetzlet(array $env) {
    $imageID = findImage($env);
    if (empty($imageID)) {
        throw error(
            'failed to find image matching ' .
                'stage=<fg=yellow;options=bold>{{stage}}</> ' .
                'domain=<fg=yellow;options=bold>{{domain}}</>',
        );
    }

    $server = findServer($env);
    if (!empty($server)) {
        return $server;
    }

    $sshKeys = array_map(
        fn($item): string => $item['name'],
        json_decode(
            runLocally('hcloud ssh-key list --output json', env: $env),
            true,
        ),
    );

    $sshKeysArgs = array_merge(
        ...array_map(fn($key): array => ['--ssh-key', $key], $sshKeys),
    );

    $createArgs = array_merge(
        [
            'hcloud',
            'server',
            'create',
            '--name',
            '{{domain}}',
            '--image',
            $imageID,
            '--type',
            '{{hetzlet_type}}',
            '--location',
            '{{hetzlet_region}}',
            '--label',
            'env={{stage}}',
        ],
        $sshKeysArgs,
    );

    runLocally(
        implode(' ', $createArgs),
        env: $env,
        timeout: 1800,
        idle_timeout: 1800,
    );

    return findServer($env);
}

function ensureHetzletDown(array $env) {
    if (get('domain') == get('doctl_sld')) {
        throw error('<fg=red;options=bold>One must not delete production</>');
    }

    $server = findServer($env);

    if (empty($server)) {
        warning('server unavailable; cannot take snapshot');
    } else {
        info('creating snapshot for <fg=yellow;options=bold>{{domain}}</>');

        runLocally(
            implode(' ', [
                'hcloud',
                'server',
                'create-image',
                '--description',
                '{{domain}}',
                '--type',
                'snapshot',
                '--label',
                'env={{stage}}',
                '{{domain}}',
            ]),
            env: $env,
        );
    }

    info('deleting <fg=yellow;options=bold>{{domain}}</>');
    runLocally('hcloud server delete {{domain}} || true', env: $env);

    ensureDNSRecords($env, true);
}

function ensureDNSRecords(array $env, ?bool $nullify = false) {
    $server = findServer($env);
    if (empty($server) && !$nullify) {
        throw error(
            'failed to find server matching ' .
                'stage=<fg=yellow;options=bold>{{stage}}</> ' .
                'domain=<fg=yellow;options=bold>{{domain}}</>',
        );
    }

    $ipv4Addr = $nullify ? '127.0.0.1' : $server['public_net']['ipv4']['ip'];

    $subdomain = str_replace('.' . get('doctl_sld'), '', get('domain'));
    $grafanaSubdomain = "grafana.$subdomain";

    foreach (
        [
            ['type' => 'A', 'addr' => $ipv4Addr],
            // TODO: ipv6, too?
        ]
        as &$rec
    ) {
        foreach ([$subdomain, $grafanaSubdomain] as $name) {
            info(
                "looking for DNS records matching <fg=yellow;options=bold>$name</>",
            );
            $dnsRecord = findDNSRecord($name, $env);

            if (is_null($dnsRecord)) {
                throw error(
                    'failed to find DNS record matching ' .
                        "name=<fg=yellow;options=bold>$name</>" .
                        ' in domain=<fg=yellow>{{domain}}</>',
                );
            }

            $recArgs = [
                '--record-type',
                $rec['type'],
                '--record-data',
                $rec['addr'],
                '--record-name',
                $name,
                '--record-ttl',
                '60',
            ];

            if (is_null($dnsRecord)) {
                $dnsRecord = end(
                    json_decode(
                        runLocally(
                            'doctl compute domain records create {{doctl_sld}} -o json ' .
                                implode(' ', $recArgs),
                        ),
                    ),
                );
            }

            runLocally(
                'doctl compute domain records update {{doctl_sld}} --record-id ' .
                    $dnsRecord['id'] .
                    ' ' .
                    implode(' ', $recArgs),
            );

            if ($rec['type'] != 'A') {
                continue;
            }

            retryWithBackOff(
                30,
                300,
                fn() => checkDNSPropagation(
                    $name . '.' . get('doctl_sld'),
                    $rec['addr'],
                ),
            );
        }
    }
}

function retryWithBackOff(
    int $maxIntervalSeconds,
    int $maxWaitSeconds,
    callable $callback,
) {
    $startTime = time();
    $n = 0;

    while (true) {
        if (time() - $startTime > $maxWaitSeconds) {
            throw error('oh no timeout');
        }

        $sleepSeconds = max([
            0,
            min([1 * 1.75 ** $n + rand(0, 100) * 0.01, $maxIntervalSeconds]),
        ]);

        try {
            $callback();
            return;
        } catch (\Throwable $exc) {
            warning("failed; waiting <fg=yellow;options=bold>$sleepSeconds</>");
        }

        sleep($sleepSeconds);
        $n++;
    }
}

function checkDNSPropagation(string $name, string $addr) {
    info(
        "<fg=yellow;options=bold>$name</> getaddress " .
            '(via region <fg=green>{{hetzlet_resolver_region}}</>)',
    );

    $rIPs = resolverIPs();
    $digNameservers = array_map(fn($entry): string => '@' . $entry, $rIPs);
    $actualAddr = trim(
        runLocally(
            implode(
                ' ',
                array_merge(['dig', '+short'], $digNameservers, [$name]),
            ),
        ),
    );

    if ($actualAddr == $addr) {
        info(
            "<fg=yellow;options=bold>$name</> resolving to " .
                "<fg=green;options=bold>$addr</> " .
                '(via region <fg=green>{{hetzlet_resolver_region}}</>)',
        );
        return;
    }

    throw error(
        "address not propagated yet for <fg=yellow;options=bold>$name</> " .
            "(<fg=red>$actualAddr</> != <fg=green;options=bold>$addr</>)",
    );
}

function resolverIPs() {
    $resp = json_decode(
        fetch(
            'https://public-dns.info/nameserver/{{hetzlet_resolver_region}}.json',
        ),
        true,
    );

    $resolvers = array_filter(
        $resp,
        fn($entry): bool => $entry['reliability'] >= 0.99999,
    );

    $ips = array_map(fn($entry): string => $entry['ip'], $resolvers);

    return array_intersect_key($ips, array_rand($ips, 5));
}
