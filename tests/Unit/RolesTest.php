<?php declare(strict_types=1);
namespace Deployer;

require_once __DIR__ . '/../../src/roles.php';

function get(string $attr): mixed {
    return [
        'labels' => [
            'role.void' => 'ok',
            'role.flippy' => 'ok',
        ],
    ][$attr] ?? null;
}

describe('has_role', function () {
    it('has one role', function () {
        expect(has_role('void'))->toBeTrue();
    });

    it('has another role', function () {
        expect(has_role('flippy'))->toBeTrue();
    });

    it('does not have role', function () {
        expect(has_role('glomper'))->toBeFalse();
    });
});
